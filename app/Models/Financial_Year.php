<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Financial_Year extends Model
{
    use HasFactory;

    protected $table = 'mst_fy';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'year',
        'start_date',
        'end_date',
        'active',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];
}
