<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationDocument extends Model
{
    use HasFactory;

    protected $table = 'trans_documents';
    public $timestamps = false;
    
   
    protected $fillable = array('application_id', 'document_id', 'document_file', 'created_by', 'updated_by', 'deleted_by', 'deleted_at');


    protected $dates = ['deleted_at'];
}
