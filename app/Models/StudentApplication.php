<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentApplication extends Model
{
    use HasFactory;
    protected $table = 'student_application';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'standard_id',
        'cast_id',
        'distance_id',
        'firstname',
        'middlename',
        'lastname',
        'dob',
        'age',
        'nationality',
        'birth_place',
        'mother_tongue',
        'religion',
        'adhaar_no',
        'father_name',
        'father_qualification',
        'father_profession',
        'father_office_address',
        'father_email',
        'father_mobile_no',
        'father_adhaar_no',
        'father_pan_no',
        'mother_name',
        'mother_qualification',
        'mother_profession',
        'mother_office_address',
        'mother_email',
        'mother_mobile_no',
        'mother_adhaar_no',
        'mother_pan_no',
        'local_address',
        'permanent_address',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function religions()
    {
        return $this->belongsTo(Religion_Master::class, 'religion');
    }
}
