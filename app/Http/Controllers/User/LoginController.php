<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Session;

class LoginController extends Controller
{
    //

    public function landingpage(){
        return view('landingpage');
    }

    public function login(){
        return view('user.login');
    }

    public function dologin(Request $request){

        // dd($request->all());

         $validator = Validator::make($request->all(),
          [
            'email' => 'required|email',
            'password' => 'required'
         ]);

         if($validator->fails())
         {
             return redirect()->back()
                   ->withErrors($validator)
                   ->withInput();
        }

    $credentials = $request->only('email', 'password');

    // $credentials['usertype'] = 1;

    if(Auth::attempt($credentials)){



        if(Auth::user()->usertype==0){

            $user = session()->flash('message', 'Welcome To Admin Panel');

        }elseif(Auth::user()->usertype==1){

            $user = session()->flash('message', 'Welcome To User Panel');

        }elseif(Auth::user()->usertype==2){

            $user = session()->flash('message', 'Welcome To Clerk Panel');

        }elseif(Auth::user()->usertype==3){

            $user = session()->flash('message', 'Welcome To SuperAdmin Panel');
        }

        $user;
        Session::flash('alert-type', 'success');
        return redirect('admin/index');
    }else{
        session()->flash('message', 'Invalid Credential');
        Session::flash('alert-type', 'error');
        return redirect()->back();
    }
}


}
