<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentApplication;
use App\Models\Cast_Master;
use App\Models\Distance_Master;
use App\Models\Standard_Master;
use App\Models\Documents_Master;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use App\Models\TermsCondition_Master;
use App\Models\ApplicationDocument;
use App\Models\Religion_Master;
use App\Models\User;
use Validator;
use Auth;
use Session;
use File;
use DB;
use Illuminate\Validation\Rules\Exists;

class ApplicationController extends Controller
{
    //

    public function schoolList()
    {
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        // return view('user.application.school_button', compact('school'));
        return view('user.application.schoolName', compact('school'));
    }

    public function termsAndCondition($id)
    {
        $terms = TermsCondition_Master::where('id', $id)->first();
        return view('user.application.terms_conditions', compact('terms'));
    }


    public function get_cast(Request $req)
    {
        $religions = $req->input('religions');
        $data = DB::table("mst_cast")
            ->select("id", "cast_name")
            ->whereIn("religion_id", $religions)
            ->orderby('id', 'Desc')
            ->get();
        return response()->json($data);
    }



    public function index()
    {
        $query = DB::table('student_application AS t1')
            ->select('t1.*', 't2.standard_name', 't3.cast_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->whereNull('t1.deleted_at');
        if (Auth::user()->usertype == 1) {
            $query->where('t1.created_by', Auth::user()->id);
        }
        $result =  $query->get();
        return view('user.application.grid', compact('result'));
    }

    public function create(Request $request, $id)
    {
        $school   = SchoolMaster::where('id', $id)->first();
        $cast = Cast_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $distance = Distance_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $standard = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        $financial_y = Financial_Year::where('is_active', 1)->orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $user = User::where('id', Auth::user()->id)->first();
        $document = Documents_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('user.application.add', compact('cast', 'distance', 'standard', 'document', 'school', 'financial_y', 'religion', 'user'));
    }


    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'standard_id' => 'required',
                'cast_id'       => 'required',
                'fy_id'         => 'required',
                'firstname'     => 'required',
                'lastname'      => 'required',
                'dob'           => 'required',
                'nationality'   => 'required',
                'birth_place'   => 'required',
                'mother_tongue' => 'required',
                'religion'      => 'required',
                'adhaar_no'     => 'required|min:10|max:12|unique:student_application,adhaar_no',
                'first_name'    => 'required',
                'middle_name'   => 'required',
                'last_name'     => 'required',
                'mobile_no'     => 'required|min:10|max:12|regex:/[0-9]{10}/',
                'email'         =>  'required|email',
                'father_adhaar_no' => 'required|min:10|max:12',
                'father_profession'=>'required',
                'mother_first_name' => 'required',
                'mother_middle_name' => 'required',
                'mother_last_name' => 'required',
                'mother_email'   =>  'required|email',
                'mother_mobile_no' => 'required|min:10|max:12|regex:/[0-9]{10}/',
                'mother_adhaar_no' => 'required|min:10|max:12',
                'mother_profession'=>'required',
                'document_file' => 'required',

            ],

            [
                'standard_id.required' => 'Standard Name is required',
                'cast_id.required' => 'Cast Name is required',
                'fy_id.required'     => 'Financial Year is required',
                'firstname.required' => 'First Name is required',
                'lastname.required' => 'Last Name is required',
                'dob.required' => 'Date of Birth is required',
                'nationality.required' => 'Nationality is required',
                'birth_place.required' => 'Birth Place is required',
                'mother_tongue.required' => 'Mother Tongue is required',
                'religion.required' => 'Religion is required',
                'adhaar_no.required' => 'Adhaar Number is required',
                'first_name' => 'Father First Name is required',
                'middle_name' => 'Father Middle Name is required',
                'last_name' => 'Father Last Name is required',
                'mobile_no' => 'Father Mobile Number is required',
                'email' => 'Father Email is required',
                'mother_first_name.required' => 'Mother Name is required',
                'mother_middle_name.required' => 'Mother Middle Name is required',
                'mother_last_name.required' => 'Mother Last Name is required',
                'mother_email.required' => 'Mother Email is required',
                'mother_mobile_no.required' => 'Mother Mobile Number is required',
                'mother_adhaar_no.required' => 'Mother Adhaar Number is required',

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }



        if ($request->twins == 2) {

            $twinsApplicationIds = [];
            for ($i = 0; $i < 2; $i++) {
                $unique_id = 'STD_' . rand();
                $user = new StudentApplication;
                $user->standard_id = $request->standard_id;
                $user->cast_id     = $request->cast_id;
                $user->school_id = $request->school_id;
                $user->fy_id = $request->fy_id;
                $user->application_no = $unique_id;

                if ($i == 0) {
                    $user->firstname = $request->firstname;
                    $user->middlename = $request->middlename;
                    $user->lastname = $request->lastname;
                    $user->dob = $request->dob;
                    $user->age = $request->age;
                    $user->gender = $request->gender;
                    $user->nationality = $request->nationality;
                    $user->birth_place = $request->birth_place;
                    $user->mother_tongue = $request->mother_tongue;
                    $user->religion = $request->religion;
                    $user->adhaar_no = $request->adhaar_no;
                    $user->blood_group = $request->blood_group;
                } else {

                    $user->firstname = $request->t_firstname;
                    $user->middlename = $request->t_middlename;
                    $user->lastname = $request->t_lastname;
                    $user->dob = $request->t_dob;
                    $user->age = $request->t_age;
                    $user->gender = $request->t_gender;
                    $user->nationality = $request->t_nationality;
                    $user->birth_place = $request->t_birth_place;
                    $user->mother_tongue = $request->t_mother_tongue;
                    $user->religion = $request->t_religion;
                    $user->adhaar_no = $request->t_adhaar_no;
                    $user->blood_group = $request->t_blood_group;
                    $user->twins = $request->twins;
                }

                $user->father_qualification = $request->father_qualification;
                $user->father_profession = $request->father_profession;
                $user->father_designation = $request->father_designation;
                $user->father_office_address = $request->father_office_address;

                $user->father_alt_contact = $request->father_alt_contact;
                $user->father_adhaar_no = $request->father_adhaar_no;
                $user->father_pan_no = $request->father_pan_no;
                $user->mother_first_name = $request->mother_first_name;
                $user->mother_middle_name = $request->mother_middle_name;
                $user->mother_last_name = $request->mother_last_name;
                $user->mother_qualification = $request->mother_qualification;
                $user->mother_profession = $request->mother_profession;
                $user->mother_designation = $request->mother_designation;
                $user->mother_office_address = $request->mother_office_address;
                $user->mother_email = $request->mother_email;
                $user->mother_mobile_no = $request->mother_mobile_no;
                $user->mother_alt_contact = $request->mother_alt_contact;
                $user->mother_adhaar_no = $request->mother_adhaar_no;
                $user->mother_pan_no = $request->mother_pan_no;
                $user->house_no = $request->house_no;
                $user->area = $request->area;
                $user->pincode = $request->pincode;
                $user->city_name = $request->city_name;
                $user->residential_type = $request->residential_type;
                $user->no_of_months = $request->no_of_months;
                $user->same_address = $request->same_address;
                $user->p_house_no = $request->p_house_no;
                $user->p_area = $request->p_area;
                $user->p_pincode = $request->p_pincode;
                $user->p_city_name = $request->p_city_name;
                $user->landmark = $request->landmark;
                $user->p_landmark = $request->p_landmark;
                $user->created_by = Auth::user()->id;
                $user->save();

                $register = User::findOrFail(Auth::user()->id);
                $register->first_name = $request->first_name;
                $register->middle_name = $request->middle_name;
                $register->last_name = $request->last_name;
                $register->mobile_no = $request->mobile_no;
                $register->email = $request->email;
                $register->save();

                $twinsApplicationIds[] = $user->id;
            }

            // dd($twinsApplicationIds);
            $documentTypeIds = $request->input('document_id');

            // dd($documentTypeIds);
            if ($request->hasFile("document_file")) {
                $files = $request->file("document_file");

                foreach ($files as $key => $file) {
                    $documentTypeId = $documentTypeIds[$key];
                    $imageName = $file->getClientOriginalName();
                    $file->move('documents/', $imageName);

                    // Create a document record for each twin application
                    foreach ($twinsApplicationIds as $applicationId) {
                        ApplicationDocument::create([
                            "document_file" => $imageName,
                            'document_id' => $documentTypeId,
                            "application_id" => $applicationId,
                            "created_by" => Auth::user()->id
                        ]);
                    }
                }
            }

            session()->flash('message', 'Application form completed Successful');
            Session::flash('alert-type', 'success');
            return redirect('student_application');
        }


        $unique_id = 'STD_' . rand();
        $user = new StudentApplication;

        $user->standard_id = $request->standard_id;
        $user->cast_id     = $request->cast_id;
        $user->school_id = $request->school_id;
        $user->fy_id = $request->fy_id;
        $user->application_no = $unique_id;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->dob = $request->dob;
        $user->age = $request->age;
        $user->gender = $request->gender;
        $user->nationality = $request->nationality;
        $user->birth_place = $request->birth_place;
        $user->mother_tongue = $request->mother_tongue;
        $user->religion = $request->religion;
        $user->adhaar_no = $request->adhaar_no;
        $user->father_qualification = $request->father_qualification;
        $user->father_profession = $request->father_profession;
        $user->father_designation = $request->father_designation;
        $user->father_office_address = $request->father_office_address;
        $user->father_alt_contact = $request->father_alt_contact;
        $user->father_adhaar_no = $request->father_adhaar_no;
        $user->father_pan_no = $request->father_pan_no;
        $user->mother_first_name = $request->mother_first_name;
        $user->mother_middle_name = $request->mother_middle_name;
        $user->mother_last_name = $request->mother_last_name;
        $user->mother_qualification = $request->mother_qualification;
        $user->mother_profession = $request->mother_profession;
        $user->mother_designation = $request->mother_designation;
        $user->mother_office_address = $request->mother_office_address;
        $user->mother_email = $request->mother_email;
        $user->mother_mobile_no = $request->mother_mobile_no;
        $user->mother_alt_contact = $request->mother_alt_contact;
        $user->mother_adhaar_no = $request->mother_adhaar_no;
        $user->mother_pan_no = $request->mother_pan_no;
        $user->house_no = $request->house_no;
        $user->area = $request->area;
        $user->pincode = $request->pincode;
        $user->city_name = $request->city_name;
        $user->residential_type = $request->residential_type;
        $user->no_of_months = $request->no_of_months;
        $user->same_address = $request->same_address;
        $user->p_house_no = $request->p_house_no;
        $user->p_area = $request->p_area;
        $user->p_pincode = $request->p_pincode;
        $user->p_city_name = $request->p_city_name;
        $user->blood_group = $request->blood_group;
        $user->landmark = $request->landmark;
        $user->p_landmark = $request->p_landmark;

        $user->created_by = Auth::user()->id;
        // dd($user); die;
        $user->save();

        $register = User::findOrFail(Auth::user()->id);
        $register->first_name = $request->first_name;
        $register->middle_name = $request->middle_name;
        $register->last_name = $request->last_name;
        $register->mobile_no = $request->mobile_no;
        $register->email = $request->email;

        $register->save();

        $documentTypeIds = $request->input('document_id');

        if ($request->hasFile("document_file")) {
            $files = $request->file("document_file");

            foreach ($files as $key => $file) {
                $documentTypeId = $documentTypeIds[$key];

                $imageName = time() . '_' . $file->getClientOriginalName();
                $file->move('documents/', $imageName);

                ApplicationDocument::create([
                    "document_file" => $imageName,
                    'document_id' => $documentTypeId,
                    "application_id" => $user->id,
                    "created_by" => Auth::user()->id
                ]);
            }
        }

        session()->flash('message', 'Application form completed Successful');
        Session::flash('alert-type', 'success');
        return redirect('student_application');
    }

    public function edit(string $id)
    {

        $cast = Cast_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $distance = Distance_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $financial_y = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $standard = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $user = User::where('id', Auth::user()->id)->first();
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $document    = DB::table('trans_documents AS t1')
            ->select('t1.*', 't2.document')
            ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.application_id', $id)
            ->get();

        $result = DB::table('student_application AS t1')
            ->select('t1.*', 't5.school_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
             ->leftJoin('mst_school AS t5', 't5.id', '=', 't1.school_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.id', $id)
            ->first();


        return view('user.application.edit', compact('result', 'cast', 'standard', 'document', 'school', 'religion', 'user', 'financial_y'));
    }

    public function update(Request $request, string $id)
    {

        $user = StudentApplication::findOrFail($id);
        $user->standard_id = $request->standard_id;
        $user->cast_id     = $request->cast_id;
        // $user->distance_id = $request->distance_id;
        $user->school_id = $request->school_id;
        $user->fy_id = $request->fy_id;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->dob = $request->dob;
        $user->age = $request->age;
        $user->gender = $request->gender;
        $user->nationality = $request->nationality;
        $user->birth_place = $request->birth_place;
        $user->mother_tongue = $request->mother_tongue;
        $user->religion = $request->religion;
        $user->adhaar_no = $request->adhaar_no;

        $user->father_qualification = $request->father_qualification;
        $user->father_profession = $request->father_profession;
        $user->father_designation = $request->father_designation;
        $user->father_office_address = $request->father_office_address;

        $user->father_alt_contact = $request->father_alt_contact;
        $user->father_adhaar_no = $request->father_adhaar_no;
        $user->father_pan_no = $request->father_pan_no;
        $user->mother_first_name = $request->mother_first_name;
        $user->mother_middle_name = $request->mother_middle_name;
        $user->mother_last_name = $request->mother_last_name;
        $user->mother_qualification = $request->mother_qualification;
        $user->mother_profession = $request->mother_profession;
        $user->mother_designation = $request->mother_designation;
        $user->mother_office_address = $request->mother_office_address;
        $user->mother_email = $request->mother_email;
        $user->mother_mobile_no = $request->mother_mobile_no;
        $user->mother_alt_contact = $request->mother_alt_contact;
        $user->mother_adhaar_no = $request->mother_adhaar_no;
        $user->mother_pan_no = $request->mother_pan_no;
        $user->house_no = $request->house_no;
        $user->area = $request->area;
        $user->district = $request->district;
        $user->state = $request->state;

        $user->pincode = $request->pincode;
        $user->city_name = $request->city_name;
        $user->residential_type = $request->residential_type;
        $user->no_of_months = $request->no_of_months;
        $user->same_address = $request->same_address;
        $user->p_house_no = $request->p_house_no;
        $user->p_area = $request->p_area;
        $user->p_pincode = $request->p_pincode;
        $user->p_city_name = $request->p_city_name;
        $user->p_district = $request->p_district;
        $user->p_state = $request->p_state;
        $user->blood_group = $request->blood_group;
        $user->landmark = $request->landmark;
        $user->p_landmark = $request->p_landmark;
        $user->updated_by = Auth::user()->id;
        $user->save();


        $register = User::findOrFail(Auth::user()->id);
        $register->first_name = $request->first_name;
        $register->middle_name = $request->middle_name;
        $register->last_name = $request->last_name;
        $register->mobile_no = $request->mobile_no;
        $register->email = $request->email;
        $register->save();

        $documentTypeIds = $request->input('document_id');

        if ($request->hasFile("document_file")) {
            $files = $request->file("document_file");

            foreach ($files as $key => $file) {
                $documentTypeId = $documentTypeIds[$key];
                $imageName = time() . '_' . $file->getClientOriginalName();
                $file->move('documents/', $imageName);

                ApplicationDocument::where('id', $documentTypeId)
                    ->update(["document_file" => $imageName, "updated_by" => Auth::user()->id]);
            }
        }

        session()->flash('message', 'Application form updated Successful');
        Session::flash('alert-type', 'success');
        return redirect('student_application');
    }

    public function destroy(string $id)
    {
        $data = StudentApplication::findOrFail($id);
        $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        ApplicationDocument::where('application_id', $id)
            ->update([
                'deleted_by' => Auth::user()->id,
                'deleted_at' => date("Y-m-d H:i:s"),
            ]);
        session()->flash('message', 'Application form deleted Successful');
        Session::flash('alert-type', 'success');
        return redirect('student_application');
    }


    public function show($id)
    {
        $data = DB::table('student_application AS t1')
            ->select('t1.*', 't2.standard_name', 't3.cast_name', 't4.distance_name', 't5.school_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
            ->leftJoin('mst_school AS t5', 't5.id', '=', 't1.school_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.id', $id)
            ->first();

        $document = DB::table('trans_documents AS t1')
            ->select('t1.*', 't2.document')
            ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.application_id', $id)
            ->get();

        //dd($document);
        return view('user.application.view', compact('data', 'document'));
    }
}
