<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Session;
use Hash;
use App\Models\Financial_Year;


class RegisterController extends Controller
{
    //
    public function create(){
        $financial_y = Financial_Year::where('is_active', 1)->orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('user.register', compact('financial_y'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'first_name' => 'required',
            'mobile_no' => 'required|min:10|max:12|unique:users,mobile_no',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required',

           ]);
        $user = new User;
        $user->academy_year = $request->fy_id;
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->mobile_no = $request->mobile_no;
        $user->usertype = 1;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        session()->flash('message', 'Registered Successfully.');
        Session::flash('alert-type', 'success');
       return redirect('/login');

    }
}
