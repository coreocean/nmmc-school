<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Standard_Master;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class StandardMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $standard = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('admin.standard.grid',compact('standard'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.standard.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'standard_name' => 'required',

           ],[
            'standard_name.required' => 'Standard Name is required',

            ]);

          $data = new Standard_Master();

          $data->standard_name = $request->get('standard_name');

          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('standard_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Standard_Master::find($id);

        return view('admin.standard.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'standard_name' => 'required',

           ],[
            'standard_name.required' => 'Stadard Name is required',
            ]);

          $data = Standard_Master::find($id);

          $data->standard_name = $request->get('standard_name');

          $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('standard_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Standard_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('standard_master.index')->with('message','Your Record Deleted Successfully.');
    }
}
