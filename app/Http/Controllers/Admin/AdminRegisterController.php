<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Admin_Master;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $list = Admin_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        $list = Admin_Master::orderBy('id', 'DESC')
        ->whereNull('users.deleted_at')
        ->leftjoin('mst_school', 'users.school_id', '=', 'mst_school.id')
        ->select('users.*', 'mst_school.school_name')
        ->get();

        return view('admin.admin.grid',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.admin.create', compact('school','ayear'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
     // print_r($request->get('school_id'));exit;
        $request->validate([
            'first_name' => 'required|string|min:4|max:255',
            'email' => 'required|string|email|max:255|unique:users|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
            'password' => 'required|string',
            'school_id' => 'required',
            'academy_year'=> 'required',
        ], [
            'name.required' => ' Username is required',
            'email.required' => 'Email Id is required',
            'password.required' => 'Password is required',
            'school_id.required' => ' School name is required',
            'academy_year.required' => ' Year is required',
        ]);

          $data = new Admin_Master();

          $data->first_name = $request->get('first_name');
          $data->middle_name = $request->get('middle_name');
          $data->last_name = $request->get('last_name');
          $data->mobile_no = $request->get('mobile_no');
          $data->email  = $request->get('email');
          $data->password =  Hash::make($request->get('password'));
          $data->usertype = $request->get('usertype');
          $data->school_id  = $request->get('school_id');
          $data->academy_year  = $request->get('academy_year');
          $data->created_by = Auth::user()->id;
          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('admin_register.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Admin_Master::find($id);
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.admin.edit',compact('data', 'school','ayear'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // $request->validate([
        //     'first_name' => 'required|string|min:4|max:255',
        //     'email' => 'required|string|email|max:255|unique:users|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
        //     'password' => 'required|string',
        // ], [
        //     'name.required' => ' Username is required',
        //     'email.required' => 'Email Id is required',
        //     'password.required' => 'Password is required',
        // ]);

        $data = Admin_Master::find($id);

        $data->first_name = $request->get('first_name');
        $data->middle_name = $request->get('middle_name');
        $data->last_name = $request->get('last_name');
        $data->mobile_no = $request->get('mobile_no');
        $data->email  = $request->get('email');
        //   $data->password =  Hash::make($request->get('password'));
        $data->school_id  = $request->get('school_id');
        $data->academy_year  = $request->get('academy_year');
        $data->created_by = Auth::user()->id;
        $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('admin_register.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Admin_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('admin_register.index')->with('message','Your Record Deleted Successfully.');
    }
}
