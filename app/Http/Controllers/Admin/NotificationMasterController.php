<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Notification_Master;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use Session;
use PDF;

class NotificationMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $note = Notification_Master::orderBy('mst_notification.id', 'DESC')
                ->whereNull('mst_notification.deleted_at')
                ->leftjoin('mst_school', 'mst_notification.school_id', '=', 'mst_school.id')
                ->leftjoin('mst_fy', 'mst_notification.year', '=', 'mst_fy.id')
                ->select('mst_notification.*', 'mst_school.school_name','mst_fy.year')
                ->get();

        return view('admin.notification.grid',compact('note'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.notification.create', compact('school','ayear'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'school_id' => 'required',
            'academy_year'=> 'required',
            'title'=> 'required',
        ], [
            'title.required' => ' Title is required',

            'school_id.required' => ' School name is required',
            'academy_year.required' => ' Year is required',
        ]);

          $data = new Notification_Master();

          $data->school_id  = $request->get('school_id');
          $data->year  = $request->get('academy_year');
          $data->title = $request->get('title');
          $data->created_by = Auth::user()->id;
          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('notification_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Notification_Master::find($id);
        $school   = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.notification.edit',compact('data', 'school','ayear'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Notification_Master::find($id);

        $data->school_id  = $request->get('school_id');
        $data->year  = $request->get('academy_year');
        $data->title = $request->get('title');
        $data->created_by = Auth::user()->id;
        $data->updated_at = date("Y-m-d H:i:s");
      //   $data->modified_by = Auth::user()->id;
        $data->save();


      return redirect()->route('notification_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Notification_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('notification_master.index')->with('message','Your Record Deleted Successfully.');
    }
}
