<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Documents_Master;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class DocumentsMasterController extends Controller
{
    public function index()
    {
        $document = Documents_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('admin.document.grid',compact('document'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.document.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'document' => 'required',
            'is_required' => 'required',

           ],[
            'document.required' => 'Document Name is required',
            'is_required.required' => 'is_required is required',

            ]);

          $data = new Documents_Master();

          $data->document = $request->get('document');
          $data->document_initial = $request->get('document_initial');
          $data->is_required = $request->get('is_required');

          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('documents_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Documents_Master::find($id);

        return view('admin.document.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'document' => 'required',
            'is_required' => 'required',

           ],[
            'document.required' => 'Document Name is required',
            'is_required.required' => 'is_required is required',

            ]);

          $data = Documents_Master::find($id);

          $data->document = $request->get('document');
          $data->document_initial = $request->get('document_initial');
          $data->is_required = $request->get('is_required');

          $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('documents_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Documents_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('documents_master.index')->with('message','Your Record Deleted Successfully.');
    }
}
