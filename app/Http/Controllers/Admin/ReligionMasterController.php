<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Religion_Master;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ReligionMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('admin.religion.grid',compact('religion'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.religion.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'religion_name' => 'required',

           ],[
            'religion_name.required' => 'Religion Name is required',

            ]);

          $data = new Religion_Master();

          $data->religion_name = $request->get('religion_name');

          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('religion_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Religion_Master::find($id);

        return view('admin.religion.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'religion_name' => 'required',

           ],[
            'religion_name.required' => 'religion_name is required',
            ]);

          $data = Religion_Master::find($id);

          $data->religion_name = $request->get('religion_name');

          $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('religion_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Religion_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('religion_master.index')->with('message','Your Record Deleted Successfully.');
    }
}
