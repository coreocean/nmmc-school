<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ApplicationDocument;
use App\Models\StudentApplication;
use App\Models\Standard_Master;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use Auth;
use Session;


use Barryvdh\DomPDF\Facade as PDF;

class AdminController extends Controller
{
    //

    public function index()
    {

        //print_r(Auth::user()->id);exit;

        $result = DB::table('student_application AS t1')
            ->select('t1.*', 't2.id as standard_id', 't2.standard_name', 't3.id as cast_id', 't3.cast_name', 't4.id as dist_id', 't4.distance_name', 't5.year')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
            ->leftJoin('mst_fy AS t5', 't5.id', '=', 't1.fy_id')
            ->where('t1.created_by', '=', Auth::user()->id)
            ->whereNull('t1.deleted_at')
            ->whereNull('t2.deleted_at')
            ->whereNull('t3.deleted_at')
            ->whereNull('t4.deleted_at')
            ->orderBy('t1.id', 'DESC')
            ->get(); // Use first() instead of get()

        $standard = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        $newApp_counts = [];
        $Approve_counts = [];
        $reject_counts = [];
        $AllApp_counts = [];

        if (Auth::user()->usertype == 3) {

            //new application superadmin code for section card
            $newApp_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.c_status', '=', 0)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();


            $Approve_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.c_status', '=', 1)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();

            $reject_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.c_status', '=', 2)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();

            foreach ($standard as $data) {
                $standard_id = $data->id;

                $AllApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.standard_id', '=', $standard_id)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();


                $newApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.c_status', '=', 0)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();




                $Approve_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.c_status', '=', 1)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();

                $reject_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.c_status', '=', 2)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();
            }
        } else {

            //code for cards section
            $newApp_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.school_id', '=', Auth::user()->school_id)
                ->where('t1.c_status', '=', 0)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();

            $Approve_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.school_id', '=', Auth::user()->school_id)
                ->where('t1.c_status', '=', 1)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();

            $reject_count =  DB::table('student_application AS t1')
                ->select('t1.id')
                ->where('t1.school_id', '=', Auth::user()->school_id)
                ->where('t1.c_status', '=', 2)
                ->whereNull('t1.deleted_at')
                ->orderBy('t1.id', 'DESC')
                ->count();

            //table cound start
            foreach ($standard as $data) {

                $standard_id = $data->id;

                $AllApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.school_id', '=', Auth::user()->school_id)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();


                $newApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.school_id', '=', Auth::user()->school_id)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->where('t1.c_status', '=', 0)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();

                $Approve_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.school_id', '=', Auth::user()->school_id)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->where('t1.c_status', '=', 1)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();

                $reject_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                    ->select('t1.id')
                    ->where('t1.school_id', '=', Auth::user()->school_id)
                    ->where('t1.standard_id', '=', $standard_id)
                    ->where('t1.c_status', '=', 2)
                    ->whereNull('t1.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->count();
            }
        }
        $standards = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('dashboard', compact('result', 'newApp_count', 'Approve_count', 'reject_count', 'standards', 'AllApp_counts', 'newApp_counts', 'Approve_counts', 'reject_counts'));
    }


    public function application_view($id)
    {
        $data = DB::table('student_application AS t1')
            ->select('t1.*', 't2.standard_name', 't3.cast_name', 't4.distance_name', 't5.school_name', 't6.year', 't7.religion_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
            ->leftJoin('mst_school AS t5', 't5.id', '=', 't1.school_id')
            ->leftJoin('mst_fy AS t6', 't6.id', '=', 't1.fy_id')
            ->leftJoin('mst_religion AS t7', 't7.id', '=', 't1.religion')
            ->whereNull('t1.deleted_at')
            ->where('t1.id', $id)
            ->first();

        $document = DB::table('trans_documents AS t1')
            ->select('t1.*', 't2.document')
            ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.application_id', $data->id)
            ->get();
        $user = User::where('id', Auth::user()->id)->first();
        //dd($document);
        return view('user.application.application_view', compact('data', 'document', 'user'));
    }

    public function uploaded_doc($id)
    {

        $data = DB::table('student_application AS t1')
            ->select('t1.*', 't2.standard_name', 't3.cast_name', 't4.distance_name', 't5.school_name', 't6.year', 't7.religion_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
            ->leftJoin('mst_school AS t5', 't5.id', '=', 't1.school_id')
            ->leftJoin('mst_fy AS t6', 't6.id', '=', 't1.fy_id')
            ->leftJoin('mst_religion AS t7', 't7.id', '=', 't1.religion')
            ->whereNull('t1.deleted_at')
            ->where('t1.id', $id)
            ->first();

        $document = DB::table('trans_documents AS t1')
            ->select('t1.*', 't2.document')
            ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
            ->whereNull('t1.deleted_at')
            ->where('t1.application_id', $data->id)
            ->get();

        return view('user.application.uploaded_doc', compact('document'));
    }

    public function pdf()
    {
        $pdfdata =  DB::table('student_application AS t1')
            ->select('t1.*', 't2.id as standard_id', 't2.standard_name', 't3.id as cast_id', 't3.cast_name', 't4.id as dist_id', 't4.distance_name')

            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
            ->whereNull('t1.deleted_at')
            ->whereNull('t2.deleted_at')
            ->whereNull('t3.deleted_at')
            ->whereNull('t4.deleted_at')
            ->orderBy('t1.id', 'DESC')
            ->get();
        $pdf = PDF::loadView('Admin.application_form_pdf', $pdfdata);

        return $pdf->download('document.pdf');
    }

    function dateToWords($date)
    {
        // Create a DateTime object from the date string
        $dateTime = new DateTime($date);

        // Define arrays for months and numbers
        $months = [
            1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
            5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
            9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'
        ];

        $ordinalNumbers = [
            1 => 'First', 2 => 'Second', 3 => 'Third', 4 => 'Fourth', 5 => 'Fifth',
            6 => 'Sixth', 7 => 'Seventh', 8 => 'Eighth', 9 => 'Ninth', 10 => 'Tenth',
            11 => 'Eleventh', 12 => 'Twelfth', 13 => 'Thirteenth', 14 => 'Fourteenth', 15 => 'Fifteenth',
            16 => 'Sixteenth', 17 => 'Seventeenth', 18 => 'Eighteenth', 19 => 'Nineteenth', 20 => 'Twentieth',
        ];

        $tens = [
            2 => 'Twenty', 3 => 'Thirty', 4 => 'Forty', 5 => 'Fifty',
            6 => 'Sixty', 7 => 'Seventy', 8 => 'Eighty', 9 => 'Ninety'
        ];

        // Get day, month, and year components
        $day = $dateTime->format('j');
        $month = $months[(int)$dateTime->format('n')];
        $year = $dateTime->format('Y');

        // Convert day to ordinal number
        if ($day >= 1 && $day <= 20) {
            $dayInWords = $ordinalNumbers[$day];
        } else {
            $firstDigit = (int)($day / 10);
            $secondDigit = $day % 10;

            $dayInWords = $tens[$firstDigit];
            if ($secondDigit > 0) {
                $dayInWords .= '-' . $ordinalNumbers[$secondDigit];
            }
        }

        // Create the date in words
        $dateInWords = "$dayInWords $month $year";

        return $dateInWords;
    }


    public function logout(){
        Auth::logout();
        Session::flush();
        session()->flash('message', 'Welcome To Login Page');
        Session::flash('alert-type', 'success');
        return redirect('/');
    }
}
