<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Seat_Master;
use App\Models\Standard_Master;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SeatMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $seat = Seat_Master::orderBy('mst_seat.id', 'DESC')
                ->whereNull('mst_seat.deleted_at')
                ->leftjoin('mst_statderd', 'mst_seat.class', '=', 'mst_statderd.id')
                ->leftjoin('mst_fy', 'mst_seat.year', '=', 'mst_fy.id')
                ->select('mst_seat.*', 'mst_statderd.standard_name','mst_fy.year')
                ->get();

        return view('admin.seat.grid',compact('seat'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $school  = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $class   = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.seat.create', compact('class','ayear','school'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'school_id' => 'required',
            'class' => 'required',
            'year'=> 'required',
            'seat'=> 'required',
        ], [
            'class.required' => ' Class is required',
            'school_id.required' => ' school is required',
            'year.required' => ' Year is required',
            'seat.required' => ' Seat is required',
        ]);

          $data = new Seat_Master();
          $data->school_id  = $request->get('school_id');
          $data->class  = $request->get('class');
          $data->year  = $request->get('year');
          $data->seat = $request->get('seat');
          $data->created_by = Auth::user()->id;
          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('seat_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data    = Seat_Master::find($id);
        $school  = SchoolMaster::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $standerd   = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.seat.edit',compact('data', 'standerd','ayear','school'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Seat_Master::find($id);

        $data->school_id  = $request->get('school_id');
        $data->class  = $request->get('class');
        $data->year  = $request->get('year');
        $data->seat = $request->get('seat');
        $data->created_by = Auth::user()->id;
        $data->updated_at = date("Y-m-d H:i:s");
      //   $data->modified_by = Auth::user()->id;
        $data->save();


      return redirect()->route('seat_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Seat_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('seat_master.index')->with('message','Your Record Deleted Successfully.');
    }
}
