<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Distance_Master;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class DistanceMasterController extends Controller
{
    public function index()
    {
        $distance = Distance_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        return view('admin.distance.grid',compact('distance'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.distance.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'distance_name' => 'required',

           ],[
            'distance_name.required' => 'Distance Name is required',

            ]);

          $data = new Distance_Master();

          $data->distance_name = $request->get('distance_name');

          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('distance_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Distance_Master::find($id);

        return view('admin.distance.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'distance_name' => 'required',

           ],[
            'distance_name.required' => 'distance Name is required',
            ]);

          $data = Distance_Master::find($id);

          $data->distance_name = $request->get('distance_name');

          $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('distance_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Distance_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('distance_master.index')->with('message','Your Record Deleted Successfully.');
    }
}

