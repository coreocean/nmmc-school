<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Cast_Master;
use App\Models\Religion_Master;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CastMasterController extends Controller
{
    public function index()
    {
         //$religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        // $cast = Cast_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $cast = Cast_Master::orderBy('id', 'DESC')
                ->whereNull('mst_cast.deleted_at')
                ->join('mst_religion', 'mst_cast.religion_id', '=', 'mst_religion.id')
                ->select('mst_cast.*', 'mst_religion.religion_name')
                ->get();
        return view('admin.cast.grid',compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('admin.cast.create',compact('religion'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'religen_id' => 'required',
            'cast_name' => 'required',

           ],[
            'religen_id.required' => 'religen Name is required',
            'cast_name.required' => 'Cast Name is required',

            ]);

          $data = new Cast_Master();

          $data->religion_id = $request->get('religen_id');
          $data->cast_name = $request->get('cast_name');

          $data->created_at = date("Y-m-d H:i:s");
        //   $data->inserted_by = Auth::user()->id;
          $data->save();

          return redirect()->route('cast_master.index')->with('message','Your Record Added Successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $religion = Religion_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        $data = Cast_Master::find($id);

        return view('admin.cast.edit',compact('data','religion'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'cast_name' => 'required',

           ],[
            'cast_name.required' => 'Cast Name is required',
            ]);

          $data = Cast_Master::find($id);

          $data->cast_name = $request->get('cast_name');
          $data->religion_id = $request->get('religen_id');

          $data->updated_at = date("Y-m-d H:i:s");
        //   $data->modified_by = Auth::user()->id;
          $data->save();


        return redirect()->route('cast_master.index')->with('message','Your Record Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Cast_Master::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        return redirect()->route('cast_master.index')->with('message','Your Record Deleted Successfully.');
    }
}

