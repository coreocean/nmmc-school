<?php


namespace App\Http\Controllers\Admin;

// use Barryvdh\DomPDF\Facade as PDF;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ApplicationDocument;
use App\Models\Seat_Master;
use App\Models\StudentApplication;
use App\Models\Cast_Master;
use App\Models\Distance_Master;
use App\Models\Standard_Master;
use App\Models\Documents_Master;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use Auth;
use Session;
use PDF;

set_time_limit(300);
class AdminApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $appData =  DB::table('student_application AS t1')
                        ->select('t1.*', 't2.id as standard_id','t2.standard_name','t3.id as cast_id','t3.cast_name', 't4.id as dist_id','t4.distance_name')

                        ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                        ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                        ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                        // ->leftJoin('users AS t5', 't5.id', '=', 't1.created_by')
                        // ->Join('trans_documents AS t6', 't6.application_id', '=', 't1.id')

                        ->whereNull('t1.deleted_at')
                        ->whereNull('t2.deleted_at')
                        ->whereNull('t3.deleted_at')
                        ->whereNull('t4.deleted_at')

                        // ->whereNull('t6.deleted_at')

                        ->orderBy('t1.id', 'DESC')
                        ->get();

        return view('admin.application.grid', compact('cast', 'distance', 'standard', 'document', 'school', 'financial_y','appData'));
    }

    /**
     * Show the form for creating a new resource.
     */

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function view_generate_pdf(request $request, $id)
    {
        // $data = StudentApplication::find($id);

        $id = $id;

        $pdfdata =  DB::table('student_application AS t1')
                        ->select('t1.*', 't2.id as standard_id','t2.standard_name','t3.id as cast_id','t3.cast_name', 't4.id as dist_id','t4.distance_name','t5.year')

                        ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                        ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                        ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                        ->leftJoin('mst_fy AS t5', 't5.id', '=', 't1.fy_id')
                        // ->Join('trans_documents AS t6', 't6.application_id', '=', 't1.id')
                        ->where('t1.id', '=', $id)
                        ->whereNull('t1.deleted_at')
                        ->whereNull('t2.deleted_at')
                        ->whereNull('t3.deleted_at')
                        ->whereNull('t4.deleted_at')

                        // ->whereNull('t6.deleted_at')
                        ->orderBy('t1.id', 'DESC')
                        ->get();

     $document =  DB::table('trans_documents AS t1')
                        ->select('t1.*')
                        ->where('t1.application_id', '=', $id)
                        ->whereNull('t1.deleted_at')
                        ->orderBy('t1.id', 'DESC')
                        ->get();


    //dd($document);

    return view('Admin.application_pdf_view' , compact('pdfdata','document'));

    }

    public function generate_pdf(request $request, $id)
    {
        // $data = StudentApplication::find($id);

        $id = $id;
        $pdfdata =  DB::table('student_application AS t1')
                        ->select('t1.*', 't2.id as standard_id','t2.standard_name','t3.id as cast_id','t3.cast_name', 't4.id as dist_id','t4.distance_name')
                        ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                        ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                        ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                        ->where('t1.id', '=', $id)
                        ->whereNull('t1.deleted_at')
                        ->whereNull('t2.deleted_at')
                        ->whereNull('t3.deleted_at')
                        ->whereNull('t4.deleted_at')
                        ->orderBy('t1.id', 'DESC')
                        ->get();


        $document =  DB::table('trans_documents AS t1')
        ->select('t1.*')
        ->where('t1.application_id', '=', $id)
        ->whereNull('t1.deleted_at')
        ->orderBy('t1.id', 'DESC')
        ->get();

        $data = [
            'pdfdata' => $pdfdata,
            'document' => $document,
        ];

        $pdf = PDF::loadView('Admin.application_pdf_view.blade', $data);

        return $pdf->stream('download_application.pdf');

        //return view('Admin.view_pdf', compact('pdf'));

       // return $pdf->download('sample.pdf');
    // return view('Admin.application_form_pdf' , compact('pdfdata','document'));

    }

    public function applicationStatusList(request $request, $status)
    {
        $status;
        if(Auth::user()->usertype ==3){

            $appData =  DB::table('student_application AS t1')
                            ->select('t1.*', 't2.id as standard_id','t2.standard_name','t3.id as cast_id','t3.cast_name', 't4.id as dist_id','t4.distance_name')
                            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                            // ->where('t1.school_id', '=', Auth::user()->school_id)
                            ->where('t1.c_status', '=', $status)
                            ->whereNull('t1.deleted_at')
                            ->whereNull('t2.deleted_at')
                            ->whereNull('t3.deleted_at')
                            ->whereNull('t4.deleted_at')
                            // ->whereNull('t6.deleted_at')
                            ->orderBy('t1.id', 'DESC')
                            ->get();
        }else{

            $appData =  DB::table('student_application AS t1')
                            ->select('t1.*', 't2.id as standard_id','t2.standard_name','t3.id as cast_id','t3.cast_name', 't4.id as dist_id','t4.distance_name')
                            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                            ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                            ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                            ->where('t1.school_id', '=', Auth::user()->school_id)
                            ->where('t1.c_status', '=', $status)
                            ->whereNull('t1.deleted_at')
                            ->whereNull('t2.deleted_at')
                            ->whereNull('t3.deleted_at')
                            ->whereNull('t4.deleted_at')
                            // ->whereNull('t6.deleted_at')
                            ->orderBy('t1.id', 'DESC')
                            ->get();


        }

        return view('admin.application.grid',compact('appData','status'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function viewapplicationStatusList(request $request, $id)
    {
        $document    = DB::table('trans_documents AS t1')
                       ->select('t1.*', 't2.document')
                       ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
                       ->whereNull('t1.deleted_at')
                       ->where('t1.application_id',$id)
                       ->get();

        $appview = DB::table('student_application AS t1')
                    ->select('t1.*', 't2.id as standard_id', 't2.standard_name', 't3.id as cast_id', 't3.cast_name', 't4.id as dist_id', 't4.distance_name','t5.year','t6.religion_name', 't7.id as school_id','t7.school_name')
                    ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                    ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                    ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                    ->leftJoin('mst_fy AS t5', 't5.id', '=', 't1.fy_id')
                    ->leftJoin('mst_religion AS t6', 't6.id', '=', 't1.religion')
                    ->leftJoin('mst_school AS t7', 't7.id', '=', 't1.school_id')
                    ->where('t1.id', '=', $id)
                    ->whereNull('t1.deleted_at')
                    ->whereNull('t2.deleted_at')
                    ->whereNull('t3.deleted_at')
                    ->whereNull('t4.deleted_at')
                    ->whereNull('t5.deleted_at')
                    ->whereNull('t6.deleted_at')
                    ->whereNull('t7.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->first(); // Use first() instead of get()

        return view('admin.application.view',compact('appview','document'));

    }

    public function ApproveApplication(request $request, $id)
    {
        $update = [
            'c_status' => 1,
            'approve_by' => Auth::user()->id,
        ];

        StudentApplication::where('id', $id)->update($update);

        return redirect('/status_application_list/1')->with('message', 'Clerk Application Form Approved Successfully'); //Redirect user somewhere
    }

    public function rejectApplicationbyclerk(request $request, $id)
    {
        $data = StudentApplication::where('id','=',$id)->firstOrFail();
        $data->reject_reason = $request->get('reject_reason');
        $data->c_status = 2; //Rejected
        $data->rejected_by = Auth::user()->id;
        $data->save();

        return redirect('/status_application_list/2')->with('message', 'Clerk Application Form Rejected Successfully'); //Redirect user somewhere
    }

    public function AppPullList(request $request)
    {

        $list = DB::table('student_application AS t1')
                    ->select('t1.*', 't2.id as standard_id', 't2.standard_name', 't3.id as cast_id', 't3.cast_name', 't4.id as dist_id', 't4.distance_name','t5.year')
                    ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                    ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                    ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                    ->leftJoin('mst_fy AS t5', 't5.id', '=', 't1.fy_id')
                    ->where('t1.school_id', '=', Auth::user()->school_id)
                    ->where('t1.c_status', '=', 1)
                    ->whereNull('t1.deleted_at')
                    ->whereNull('t2.deleted_at')
                    ->whereNull('t3.deleted_at')
                    ->whereNull('t4.deleted_at')
                    ->orderBy('t1.id', 'DESC')
                    ->get(); // Use first() instead of get()

        //dd($list);

        return view('admin.application.application_pool_list',compact('list'));

    }

    public function applicationReportlist(request $request){

        $standard = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();

        $newApp_counts = [];
        $Approve_counts = [];
        $reject_counts = [];
        $AllApp_counts = [];

        if(Auth::user()->usertype ==3){

        foreach($standard as $data){
                $standard_id = $data->id;

                $AllApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.standard_id', '=', $standard_id)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();


                $newApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.c_status', '=', 0)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();




                $Approve_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.c_status', '=', 1)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();

                $reject_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.c_status', '=', 2)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();

             }

         }else{

            foreach($standard as $data){

                $standard_id = $data->id;

            $AllApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                                        ->select('t1.id')
                                        ->where('t1.school_id', '=', Auth::user()->school_id)
                                        ->where('t1.standard_id', '=', $standard_id)
                                        ->whereNull('t1.deleted_at')
                                        ->orderBy('t1.id', 'DESC')
                                        ->count();


             $newApp_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.school_id', '=', Auth::user()->school_id)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->where('t1.c_status', '=', 0)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();

             $Approve_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.school_id', '=', Auth::user()->school_id)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->where('t1.c_status', '=', 1)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();

             $reject_counts[$standard_id]['count'] =  DB::table('student_application AS t1')
                             ->select('t1.id')
                             ->where('t1.school_id', '=', Auth::user()->school_id)
                             ->where('t1.standard_id', '=', $standard_id)
                             ->where('t1.c_status', '=', 2)
                             ->whereNull('t1.deleted_at')
                             ->orderBy('t1.id', 'DESC')
                             ->count();

         }}


         $standards = Standard_Master::orderBy('id', 'DESC')->whereNull('deleted_at')->get();


         return view('admin.report.application_report', compact('standards','AllApp_counts', 'newApp_counts', 'Approve_counts', 'reject_counts'));
        }

        public function reservationReport(request $request)
        {

          return view('admin.report.reservation_report');

        }

        public function poolProcess(request $request)
        {

        $pool = DB::table('mst_seat AS t1')
                            ->select('t1.*', 't2.id as standard_id', 't2.standard_name','t3.year','t4.id as school_id', 't4.school_name')
                            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.class')
                            ->leftJoin('mst_fy AS t3', 't3.id', '=', 't1.year')
                            ->leftJoin('mst_school AS t4', 't4.id', '=', 't1.school_id')
                            ->where('t1.school_id', '=', Auth::user()->school_id)
                            ->whereNull('t1.deleted_at')
                            ->whereNull('t2.deleted_at')
                            ->whereNull('t3.deleted_at')
                            ->whereNull('t4.deleted_at')
                            ->orderBy('t1.id', 'DESC')
                            ->get(); // Use first() instead of get()


        return view('admin.application.pool_process',compact('pool'));

        }

        public function randomselection(request $request)
        {

            $pool = DB::table('mst_seat AS t1')
            ->select('t1.*', 't2.id as standard_id', 't2.standard_name','t3.year','t4.id as school_id', 't4.school_name')
            ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.class')
            ->leftJoin('mst_fy AS t3', 't3.id', '=', 't1.year')
            ->leftJoin('mst_school AS t4', 't4.id', '=', 't1.school_id')
            ->where('t1.school_id', '=', Auth::user()->school_id)
            ->whereNull('t1.deleted_at')
            ->whereNull('t2.deleted_at')
            ->whereNull('t3.deleted_at')
            ->whereNull('t4.deleted_at')
            ->orderBy('t1.id', 'DESC')
            ->get(); // Use first() instead of get()

            $list = DB::table('student_application AS t1')
                        ->select('t1.*', 't2.id as standard_id', 't2.standard_name', 't3.id as cast_id', 't3.cast_name', 't4.id as dist_id', 't4.distance_name','t5.year')
                        ->leftJoin('mst_statderd AS t2', 't2.id', '=', 't1.standard_id')
                        ->leftJoin('mst_cast AS t3', 't3.id', '=', 't1.cast_id')
                        ->leftJoin('mst_distance AS t4', 't4.id', '=', 't1.distance_id')
                        ->leftJoin('mst_fy AS t5', 't5.id', '=', 't1.fy_id')
                        ->where('t1.school_id', '=', Auth::user()->school_id)
                        ->where('t1.c_status', '=', 1)
                        ->whereNull('t1.deleted_at')
                        ->whereNull('t2.deleted_at')
                        ->whereNull('t3.deleted_at')
                        ->whereNull('t4.deleted_at')
                        ->orderBy('t1.id', 'DESC')
                        ->get(); // Use first() instead of get()

            //dd($list);

            // return view('admin.application.pool_process',compact('list','pool'));

        }

        public function view_documents(request $request, $id)
        {

            $document    = DB::table('trans_documents AS t1')
                       ->select('t1.*', 't2.document')
                       ->leftJoin('mst_document AS t2', 't2.id', '=', 't1.document_id')
                       ->whereNull('t1.deleted_at')
                       ->where('t1.application_id',$id)
                       ->get();

         return view('user.application.view_doc',compact('document'));

        }


        // code for random selection for pradnya 15/02/2024

        // $allApplications = StudentApplication::where('c_status', 0)->get();

        // $numberOfWinners = 100;

        // if (count($allApplications) >= $numberOfWinners) {
        //     $randomWinners = $allApplications->random($numberOfWinners);


        // } else {

        // }





}
