<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SchoolMaster;
use App\Models\Financial_Year;
use Auth;
use Session;
class SchoolController extends Controller
{
    //
    public function index(){
        $data = SchoolMaster::orderBy('mst_school.id', 'DESC')
                ->whereNull('mst_school.deleted_at')
                ->leftjoin('mst_fy', 'mst_school.year', '=', 'mst_fy.id')
                ->select('mst_school.*', 'mst_fy.year')
                ->get();
        return view('master.school.grid', compact('data'));
    }

    public function create(){
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('master.school.create' , compact('ayear'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'school_name' => 'required',
            'year'=> 'required',
            'start_date'=> 'required',
            'end_date'=> 'required',
        ], [
            'start_date.required' => ' start_date is required',
            'year.required' => 'Year is required',
            'end_date.required' => ' end_date is required',
            'school_name.required' => ' school_name is required',
        ]);

          $data = new SchoolMaster();
          $data->school_name = $request->school_name;
          $data->year = $request->year;
          $data->latitude = $request->latitude;
          $data->longitude = $request->longitude;
          $data->start_date = $request->start_date;
          $data->end_date = $request->end_date;
          $data->school_address = $request->school_address;
          $data->created_at = date("Y-m-d H:i:s");
          $data->created_by = Auth::user()->id;
          $data->save();

          session()->flash('message', 'School Name Added Successfully.');
          Session::flash('alert-type', 'success');
          return redirect('school');
    }

    public function edit($id){
        $data = SchoolMaster::findOrFail($id);
        $ayear   = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('master.school.edit', compact('data','ayear'));
    }

    public function update(Request $request, $id){
          $data = SchoolMaster::findOrFail($id);
          $data->school_name = $request->school_name;
          $data->school_address = $request->school_address;
          $data->year = $request->year;
          $data->latitude = $request->latitude;
          $data->longitude = $request->longitude;
          $data->start_date = $request->start_date;
          $data->end_date = $request->end_date;
          $data->updated_at = date("Y-m-d H:i:s");
          $data->updated_by = Auth::user()->id;
          $data->save();

          session()->flash('message', 'School Name Updated Successfully.');
          Session::flash('alert-type', 'success');
          return redirect('school');

    }

    public function destroy(string $id)
    {
        $data = SchoolMaster::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        session()->flash('message', 'Your Record Deleted Successfully.');
        Session::flash('alert-type', 'success');
        return redirect('school');
    }
}
