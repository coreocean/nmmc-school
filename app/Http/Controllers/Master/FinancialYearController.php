<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Financial_Year;
use Auth;
use Session;

class FinancialYearController extends Controller
{
    //

    public function index(){
        $data = Financial_Year::orderBy('id', 'DESC')->whereNull('deleted_at')->get();
        return view('master.financial-year.grid', compact('data'));
    }

    public function create(){
        return view('master.financial-year.create');
    }

    public function store(Request $request)
    {
          $data = new Financial_Year();
          $data->year = $request->year;
          $data->start_date = $request->start_date;
          $data->end_date = $request->end_date;

        //   $data->is_active = ($request->is_active) ? '1' : '0';
          $data->created_at = date("Y-m-d H:i:s");
          $data->created_by = Auth::user()->id;
          $data->save();

          session()->flash('message', 'School Name Added Successfully.');
          Session::flash('alert-type', 'success');
          return redirect('financial_year');
    }

    public function edit($id){
        $data = Financial_Year::findOrFail($id);
        return view('master.financial-year.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $data = Financial_Year::findOrFail($id);
        $data->year = $request->year;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        // $data->is_active = ($request->is_active) ? '1' : '0';
          $data->updated_at = date("Y-m-d H:i:s");
          $data->updated_by = Auth::user()->id;
          $data->save();

          session()->flash('message', 'School Name Updated Successfully.');
          Session::flash('alert-type', 'success');
          return redirect('financial_year');

    }

    public function destroy(string $id)
    {
        $data = Financial_Year::findOrFail($id);
        // $data->deleted_by = Auth::user()->id;
        $data->deleted_at = date("Y-m-d H:i:s");
        $data->update();

        session()->flash('message', 'Your Record Deleted Successfully.');
        Session::flash('alert-type', 'success');
        return redirect('financial_year');
    }

}
