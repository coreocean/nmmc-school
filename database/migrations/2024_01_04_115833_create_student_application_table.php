<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('student_application', function (Blueprint $table) {
            $table->id();
            $table->string('application_no')->nullable();
            $table->unsignedBigInteger('standard_id');
            $table->unsignedBigInteger('cast_id');
            $table->unsignedBigInteger('distance_id');
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('fy_id');
            $table->string('firstname')->nullable();
            $table->string('middlename')->nullable();
            $table->string('lastname')->nullable();
            $table->string('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('nationality')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('mother_tongue')->nullable();
            $table->string('religion')->nullable();
            $table->string('adhaar_no')->nullable();

            $table->string('father_qualification')->nullable();
            $table->string('father_profession')->nullable();
            $table->string('father_office_address')->nullable();
            $table->string('father_email')->nullable();
            $table->string('father_mobile_no')->nullable();
            $table->string('father_adhaar_no')->nullable();
            $table->string('father_pan_no')->nullable();

            $table->string('mother_qualification')->nullable();
            $table->string('mother_profession')->nullable();
            $table->string('mother_office_address')->nullable();
            $table->string('mother_email')->nullable();
            $table->string('mother_mobile_no')->nullable();
            $table->string('mother_adhaar_no')->nullable();
            $table->string('mother_pan_no')->nullable();

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('student_application');
    }
};
