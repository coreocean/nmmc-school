<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            $table->string('approve_by')->after('clerk_status')->nullable()->default(null);
            $table->string('rejected_by')->after('approve_by')->nullable()->default(null);
            $table->string('reject_reason')->after('rejected_by')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            $table->dropColumn('approve_by'); // Rollback the field in case of migration rollback
            $table->dropColumn('rejected_by'); // Rollback the field in case of migration rollback
            $table->dropColumn('reject_reason'); // Rollback the field in case of migration rollback
        });
    }
};