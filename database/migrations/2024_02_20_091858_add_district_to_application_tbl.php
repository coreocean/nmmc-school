<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            $table->string('district')->after('landmark')->nullable()->default(null);
            $table->string('state')->after('district')->nullable()->default(null);
            $table->string('p_district')->after('state')->nullable()->default(null);
            $table->string('p_state')->after('p_district')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            $table->dropColumn('district'); // Rollback the field in case of migration rollback
            $table->dropColumn('state'); // Rollback the field in case of migration rollback
            $table->dropColumn('p_district'); // Rollback the field in case of migration rollback
            $table->dropColumn('p_state'); // Rollback the field in case of migration rollback
        });

    }
};
