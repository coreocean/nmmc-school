<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('mst_school', function (Blueprint $table) {
            $table->string('latitude')->after('school_address')->nullable()->default(null);
            $table->string('longitude')->after('latitude')->nullable()->default(null);
            $table->string('start_date')->after('longitude')->nullable()->default(null);
            $table->string('end_date')->after('start_date')->nullable()->default(null);
            $table->string('year')->after('end_date')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('mst_school', function (Blueprint $table) {
            $table->dropColumn('latitude'); // Rollback the field in case of migration rollback
            $table->dropColumn('longitude'); // Rollback the field in case of migration rollback
            $table->dropColumn('start_date'); // Rollback the field in case of migration rollback
            $table->dropColumn('end_date'); // Rollback the field in case of migration rollback
            $table->dropColumn('year'); // Rollback the field in case of migration rollback

        });
    }
};
