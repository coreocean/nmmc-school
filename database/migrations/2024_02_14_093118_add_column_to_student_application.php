<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            $table->string('blood_group')->nullable()->before('twins');
            $table->string('house_no')->nullable()->after('mother_pan_no');
            $table->string('area')->nullable()->after('house_no');
            $table->string('pincode')->nullable()->after('area');
            $table->string('city_name')->nullable()->after('pincode');
            $table->string('residential_type')->nullable()->after('city_name');
            $table->string('no_of_months')->nullable()->after('residential_type');
            $table->tinyInteger('same_address')->nullable()->after('no_of_months');
            $table->string('p_house_no')->nullable()->after('same_address');
            $table->string('p_area')->nullable()->after('p_house_no');
            $table->string('p_pincode')->nullable()->after('p_area');
            $table->string('p_city_name')->nullable()->after('p_pincode');

            $table->integer('clerk_status')->comment('0 => pending, 1 => approve, 2 => reject')->default(0)->after('p_pincode');
            $table->integer('admin_status')->comment('0 => pending, 1 => approve, 2 => reject')->default(0)->after('clerk_status');


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            //
        });
    }
};
