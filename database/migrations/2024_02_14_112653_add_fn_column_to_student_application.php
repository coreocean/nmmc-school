<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            //
            $table->tinyInteger('twins')->nullable()->after('adhaar_no');
            $table->string('father_first_name')->nullable()->after('twins');
            $table->string('father_middle_name')->nullable()->after('father_first_name');
            $table->string('father_last_name')->nullable()->after('father_middle_name');
            $table->string('father_alt_contact')->nullable()->after('father_mobile_no');
            $table->string('father_designation')->nullable()->after('father_profession');
            $table->string('mother_first_name')->nullable()->after('father_designation');
            $table->string('mother_middle_name')->nullable()->after('mother_first_name');
            $table->string('mother_last_name')->nullable()->after('mother_middle_name');
            $table->string('mother_designation')->nullable()->after('mother_profession');
            $table->string( 'mother_alt_contact')->nullable()->after('mother_mobile_no');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('student_application', function (Blueprint $table) {
            //
        });
    }
};
