<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Standard_Master;

class standerd_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $standerd = [
            [
                'standard_name'=>'Nursery'
            ],

            [
                'standard_name'=>'Junior KG'
            ],

            [
                'standard_name'=>'Senior KG'
            ]


        ];

        foreach($standerd as $key => $value){
            Standard_Master::create($value);
        }
    }
}
