<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Documents_Master;

class documents_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $documents = [
            [
                'document'=>'Birth Certificate of Student',
                'is_required'=>'1'
            ],

            [
                'document'=>'Adhar Card Of Student',
                'is_required'=>'1'
            ],
            [
                'document'=>'Cast Certificate of Student',
                'is_required'=>'1'
            ],
            [
                'document'=>'Adhar Card Of Parent',
                'is_required'=>'1'
            ],

            [
                'document'=>'Cast Certificate of Student',
                'is_required'=>'1'
            ],
            [
                'document'=>'Cast Certificate of Parent',
                'is_required'=>'1'
            ],
            [
                'document'=>'Upload Signature Father/Mother/Guardian',
                'is_required'=>'1'
            ],

            [
                'document'=>'Upload Photo of Student',
                'is_required'=>'1'
            ],

            [
                'document'=>'Registered Rent Agreement/Light bill(6 month Older)',
                'is_required'=>'1'
            ]


        ];

        foreach($documents as $key => $value){
            Documents_Master::create($value);
        }
    }
}
