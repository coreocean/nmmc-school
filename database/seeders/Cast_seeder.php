<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cast_Master;

class Cast_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cast = [
            [
                'cast_name'=>'Open'
            ],

            [
                'cast_name'=>'OBC'
            ],

            [
                'cast_name'=>'SC'
            ],
            [
                'cast_name'=>'ST'
            ],
            [
                'cast_name'=>'NT'
            ],
            [
                'cast_name'=>'Other'
            ]

        ];

        foreach($cast as $key => $value){
            Cast_Master::create($value);
        }
    }
}
