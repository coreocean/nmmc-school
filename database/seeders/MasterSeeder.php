<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Cast_Master;

class MasterSeeder extends Seeder
{
    /**
     * 
     * Run the database seeds.
     */
    public function run()
    {
        //
        $cast = [
        [
            'cast_name'=>'Adhaar'
        ],

        [
            'cast_name'=>'Pan'
        ],

        [
            'cast_name'=>'Photo'
        ]

    ];

    foreach($cast as $key => $value){
        Cast_Master::create($value);
    }
    
}
}