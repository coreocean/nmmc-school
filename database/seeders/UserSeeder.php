<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {

        $users = [
            [
                'first_name' => 'user',
                'middle_name' => 'uuu',
                'last_name' => 'kkk',
                'mobile_no' => '8888888888',
                'email' => 'user@gmail.com',
                'usertype' => '1',
                'password' => bcrypt('1234567890')
            ],

            [
                'first_name' => 'admin',
                'middle_name' => 'aaa',
                'last_name' => 'sss',
                'mobile_no' => '7777777777',
                'email' => 'admin@gmail.com',
                'usertype' => '0',
                'password' => bcrypt('1234567890')
            ],

            [
                'first_name' => 'super admin',
                'middle_name' => 'ssaa',
                'last_name' => 'sa',
                'mobile_no' => '9999999999',
                'email' => 'superadmin@gmail.com',
                'usertype' => '3',
                'password' => bcrypt('1234567890')
            ]

        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }
    }
}
