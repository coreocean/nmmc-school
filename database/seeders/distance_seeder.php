<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Distance_Master;

class distance_seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $distance = [
            [
                'distance_name'=>'1'
            ],

            [
                'distance_name'=>'2'
            ],

            [
                'distance_name'=>'3'
            ],
            [
                'distance_name'=>'4'
            ],
            [
                'distance_name'=>'5'
            ],
            [
                'distance_name'=>'6'
            ]

        ];

        foreach($distance as $key => $value){
            Distance_Master::create($value);
        }
    }
}
