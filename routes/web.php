<?php

use Illuminate\Support\Facades\Route;

// -------Master-----
use App\Http\Controllers\Admin\StandardMasterController;
use App\Http\Controllers\Admin\CastMasterController;
use App\Http\Controllers\Admin\DistanceMasterController;
use App\Http\Controllers\Admin\DocumentsMasterController;
use App\Http\Controllers\Admin\ReligionMasterController;
use App\Http\Controllers\Admin\TermsConditionController;
use App\Http\Controllers\Admin\SeatMasterController;
use App\Http\Controllers\Admin\NotificationMasterController;

use App\Http\Controllers\Master\SchoolController;
use App\Http\Controllers\Master\FinancialYearController;

// -----Admin-------
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminRegisterController;
use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\AdminApplicationController;
// ------User-------
use App\Http\Controllers\User\LoginController;
use App\Http\Controllers\User\RegisterController;
use App\Http\Controllers\User\ApplicationController;

// use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// -----User-----
 Route::get('login', [LoginController::class, 'login']);
Route::get('/', [LoginController::class, 'landingpage']);
Route::post('dologin', [LoginController::class, 'dologin']);
Route::get('register', [RegisterController::class, 'create']);
Route::post('register', [RegisterController::class, 'store']);
// ----end-----
Route::group(['middleware' => ['baseuser']], function () {

Route::get('admin/index', [AdminController::class, 'index']);
Route::get('logout', [AdminController::class, 'logout']);
Route::get('admin/pdf', [AdminController::class, 'pdf']);
//-----User Panel------
Route::resource('student_application', ApplicationController::class);
Route::get('school_list', [ApplicationController::class, 'schoolList']);
Route::get('termsandconditions/{id}', [ApplicationController::class, 'termsAndCondition']);
Route::post('/get-cast', [ApplicationController::class, 'get_cast']);

Route::get('create/{id}', [ApplicationController::class, 'create']);


    //-------Admin Panel------
    Route::resource('/admin_register', AdminRegisterController::class);
    Route::resource('/application_list', AdminApplicationController::class);
    //------Master-------
    Route::resource('/standard_master', StandardMasterController::class);
    Route::resource('/cast_master', CastMasterController::class);
    Route::resource('/religion_master', ReligionMasterController::class);
    Route::resource('/distance_master', DistanceMasterController::class);
    Route::resource('/documents_master', DocumentsMasterController::class);
    Route::resource('/school', SchoolController::class);
    Route::resource('/financial_year', FinancialYearController::class);
    Route::resource('/terms_condition', TermsConditionController::class);
    Route::resource('/seat_master', SeatMasterController::class);
    Route::resource('/notification_master', NotificationMasterController::class);
    //----end----
    Route::get('/generate_pdf/{id}', [AdminApplicationController::class, 'generate_pdf'])->name('generate_pdf');

    Route::get('/view_generate_pdf/{id}', [AdminApplicationController::class, 'view_generate_pdf'])->name('view_generate_pdf');

    // application list pending/approve/reject
    Route::get('/status_application_list/{status}', [AdminApplicationController::class, 'applicationStatusList'])->name('applicationStatusList');
    Route::get('/view_application/{id}', [AdminApplicationController::class, 'viewapplicationStatusList'])->name('viewapplicationStatusList');
    Route::get('/approve_application/{id}', [AdminApplicationController::class, 'ApproveApplication'])->name('ApproveApplication');
    Route::post('/reject_applicationbyclerk/{id}', [AdminApplicationController::class, 'rejectApplicationbyclerk'])->name('rejectApplicationbyclerk');
    Route::get('/app_pool_list', [AdminApplicationController::class, 'AppPullList'])->name('AppPullList');
    Route::get('/application_report', [AdminApplicationController::class, 'applicationReportlist'])->name('applicationReportlist');
    Route::get('/reservation_report', [AdminApplicationController::class, 'reservationReport'])->name('reservationReport');
    Route::get('/pool_process', [AdminApplicationController::class, 'poolProcess'])->name('poolProcess');
    Route::get('/random_selection', [AdminApplicationController::class, 'randomselection'])->name('randomselection');

    Route::get('/view_documents/{id}', [AdminApplicationController::class, 'view_documents'])->name('view_documents');
});
