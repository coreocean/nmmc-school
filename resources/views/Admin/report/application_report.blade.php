@extends('layouts.master')

 @section('content')

 <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">

                <!-- Column starts -->

                <!-- Column ends -->
                <!-- Column starts -->
                <div class="col-xl-12">
                    <div class="card" id="accordion-three">
                        <div class="card-header flex-wrap d-flex justify-content-between px-3">
                            <div>
                            <h4 class="card-title">Application Report</h4>

                            </div>
                            <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                                <li class="nav-item" role="presentation">
                                <!-- Button trigger modal -->

                                </li>

                                </ul>
                                </div>

                                <!-- /tab-content -->
                                <div class="tab-content" id="myTabContent-2">
                                <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                                <div class="card-body p-0">


                            <div class="table-responsive">
                                <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">Standerd</th>
                                            <th scope="col">Application Count</th>
                                            <th scope="col">Pending</th>
                                            <th scope="col">Approved</th>
                                            <th scope="col">Rejected</th>
                                            <th scope="col">Selected</th>
                                            <th scope="col">Waiting List</th>
                                            <th scope="col">Canceled</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($standards as $standard)
                                            <tr>
                                                <td>{{ $standard->standard_name }}</td>
                                                <td>{{ $AllApp_counts[$standard->id]['count'] ?? 0 }}</td>
                                                <td>{{ $newApp_counts[$standard->id]['count'] ?? 0 }}</td>
                                                <td>{{ $Approve_counts[$standard->id]['count'] ?? 0 }}</td>
                                                <td>{{ $reject_counts[$standard->id]['count'] ?? 0 }}</td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>0</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>



                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- /tab-content -->

                    </div>
                </div>



        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>

@endsection
