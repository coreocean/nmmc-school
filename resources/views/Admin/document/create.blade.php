@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Add Document </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ url('/documents_master') }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf

							<div class="row">
								<div class="col-xl-4 col-sm-4">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Document <span class="required">*</span></label>
									  <input type="text" class="form-control"  name="document" id="document" placeholder="Enter Document Name " required>
									</div>
								</div>
                                <div class="col-xl-4 col-sm-4">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Document Initial </label>
									  <input type="text" class="form-control"  name="document_initial" id="document_initial" placeholder="Enter Document Initial " >
									</div>
								</div>
                                <div class="col-xl-4 col-sm-4">
									<div class="mb-3">
                                        <label for="exampleFormControlInput9" class="text-label text-primary">Is Required<span class="required">*</span></label>
                                        <select class=" default-select form-control wide" name="is_required" id="is_required" aria-label="Default select example" required>
                                          <option selected></option>
                                          <option value="1">Mandatory</option>
                                          <option value="2">Not Mandatory</option>
                                        </select>
									</div>
								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
