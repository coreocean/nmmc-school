@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-10">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Add Document </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('documents_master.update', $data->id) }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf

                                @if (!empty($data->id) || 1 == 1)
                                    <input type="hidden" name="_method" value="PATCH">
                                @endif

                                <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">

							<div class="row">
								<div class="col-xl-4 col-sm-4">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Document <span class="required">*</span></label>
									  <input type="text" class="form-control"  value="{{ $data->document }}" name="document" id="document" placeholder="Enter Cast Name " required>
									</div>
								</div>
                                <div class="col-xl-4 col-sm-4">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Document Initial </label>
									  <input type="text" class="form-control"  value="{{ $data->document_initial }}" name="document_initial" id="document_initial" placeholder="Enter Cast Name " >
									</div>
								</div>
                                <div class="col-xl-4 col-sm-4">
									<div class="mb-3">
                                        <label class="text-label form-label">Is Required<span class="required">*</span></label>
                                        <select class=" default-select form-control wide"  name="is_required" id="is_required" aria-label="Default select example" required>
                                          <option selected></option>

                                          <option value="1" {{ $data->is_required == "1" ? 'selected' : '' }}>Mandatory</option>
                                          <option value="2" {{ $data->is_required == "2" ? 'selected' : '' }}>Not Mandatory</option>
                                        </select>
									</div>
								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
