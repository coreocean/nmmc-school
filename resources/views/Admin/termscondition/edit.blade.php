@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Edit Terms And Conditions </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('terms_condition.update', $data->id) }}" class="new-added-form" enctype="multipart/form-data">
                                @csrf
                            @if (!empty($data->id) || 1 == 1)
                                <input type="hidden" name="_method" value="PATCH">
                            @endif

                            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">
							<div class="row">

                                <div class="col-xl-6 col-sm-6">
                                    <label class="text-label form-label">School Name<span class="required">*</span></label>
                                      <select class=" default-select form-control wide" name="school_id"  aria-label="Default select example" >
                                          <option value="" selected="selected">Select School</option>
                                              @foreach($school as  $schools)
                                              <option value="{{$schools->id}}" {{$data->school_id == $schools->id ? 'selected':''}}>{{$schools->school_name}}</option>
                                              @endforeach
                                      </select>
                                      <span class="text-danger">@error('school_id'){{$message}}@enderror</span>
                                  </div>

                                  <div class="col-xl-6 col-sm-6">
                                    <label class="text-label form-label">Academy Year<span class="required">*</span></label>
                                      <select class=" default-select form-control wide" name="academy_year"  aria-label="Default select example" >
                                          <option value="" selected="selected">Select Year</option>
                                           @foreach($ayear as  $year)
                                              <option value="{{$year->id}}" {{$data->year == $year->id ? 'selected':''}}>{{$year->year}}</option>
                                           @endforeach
                                      </select>
                                      <span class="text-danger">@error('academy_year'){{$message}}@enderror</span>
                                  </div>
                                </div>
                                <div class="row">
								<div class="col-xl-12 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Terms <span class="required">*</span></label>
                                      <textarea  class="form-control" id="long_desc2" name="terms">{{ $data->terms }}</textarea>

									</div>

								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>

        <script src="https://cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
        <script>
               CKEDITOR.replace('terms');
              CKEDITOR.replace('long_desc2');
        </script>

        <!--**********************************
            Content body end
        ***********************************-->

@endsection
