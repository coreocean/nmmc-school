@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Edit Admin </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('seat_master.update', $data->id) }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf
                                @if (!empty($data->id) || 1 == 1)
                                    <input type="hidden" name="_method" value="PATCH">
                                @endif

                                <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">

							<div class="row">
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
                                        <label class="text-label form-label">School Name<span class="required">*</span></label>
                                              <select class=" default-select form-control wide" name="school_id"  aria-label="Default select example" >
                                                  <option value="" selected="selected">Select School</option>
                                                  @foreach($school as  $schools)
                                                  <option value="{{$schools->id}}" {{$data->school_id == $schools->id ? 'selected':''}}>{{$schools->school_name}}</option>
                                                  @endforeach
                                              </select>
                                              <span class="text-danger">@error('year'){{$message}}@enderror</span>
									</div>
								</div>
								<div class="col-xl-4 col-sm-6">
									<div class="mb-3">
                                        <label class="text-label form-label">Academy Year<span class="required">*</span></label>
                                              <select class=" default-select form-control wide" name="year"  aria-label="Default select example" >
                                                  <option value="" selected="selected">Select Year</option>
                                                   @foreach($ayear as  $year)
                                                      <option value="{{$year->id}}" {{$data->year == $year->id ? 'selected':''}}>{{$year->year}}</option>
                                                   @endforeach
                                              </select>
                                              <span class="text-danger">@error('year'){{$message}}@enderror</span>
									</div>
								</div>
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Class <span class="required">*</span></label>
									  <select class=" default-select form-control wide" name="class"  aria-label="Default select example" >
                                        <option value="" selected="selected">Select Class</option>
                                         @foreach($standerd as $class)
                                            <option value="{{$class->id}}" {{$data->class == $class->id ? 'selected':''}}>{{$class->standard_name}}</option>
                                         @endforeach
                                    </select>
                                    <span class="text-danger">@error('class'){{$message}}@enderror</span>
                                    </div>
								</div>
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Seat <span class="required">*</span></label>
									  <input type="text" class="form-control" value="{{ $data->seat }}"  name="seat" id="seat" placeholder="Enter Last Name ">
									</div>
								</div>

							</div>

							<div class="float-end">
								<a href='/seat_master'><button type=" button" class="btn btn-outline-primary me-3">Cancle</button></a>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
