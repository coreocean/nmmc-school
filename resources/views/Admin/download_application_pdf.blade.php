
 <style>
span.float-end {
    display: flex;
}

strong.header_to_no {
    margin-right: 11px;
}

strong.input-group.search-area.mb-md-0.mb-3 {
    width: 140px;
    height: 32px;
}

.card-header {
    padding: 9px;
}

h1.top_name {
    font-size: 23px;
    font-weight: 600;
    text-align: center;
}
.top_namesecsection {
    text-align: center;

}
h1.top_name_app {
    font-size: 22px;
    text-align: center;
    margin-top: 20px;
    /* margin-bottom: 50px; */
}

strong.input-group.search-area.photo {
    width: 132px;
    height: 126px;
    margin-bottom: 10px;
    padding: 21px;
    float: inline-end;
}
   table {
            /* width: 100%; */
            border-collapse: collapse;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
            vertical-align: baseline;
        }

        th {
            /* background-color: #f2f2f2; */
        }

        .last_para {
    margin-top: 24px;
    font-size: 14px;
}

h3.last_name_app {
    text-align: center;
    font-size: 17px;
}

strong.footer_to_no {
    height: 32px;
    width: 50px;
}

/* Hide header and footer */
header, footer {
        display: none;
      }

      @media print {
  @page {
    size: A4;
    margin: 1cm;
    mso-header-space: 0;
    mso-footer-space: 0;
  }

  body {
    margin: 0;
    padding: 20px;
  }


  header, footer {
    display: none;
  }

  /* For Webkit browsers like Chrome and Safari */
  @media print and (-webkit-min-device-pixel-ratio:0) {
    header, footer {
      display: none !important;
    }
  }
  @media print {
    @page {
      size: A4;
      margin: 1cm;
      -moz-transform: scale(1);
      -moz-transform-origin: 0 0;
    }
  }
}


</style>
 <!--**********************************
            Content body start
        ***********************************-->

        <div class="content-body mh-auto" >
            <div class=" mh-auto" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                 @foreach ($pdfdata as $data)
                        <div class="card mt-3">
                            <div class="card-header"> <img class="img-fluid " src="{{asset('assets/images/logo.png')}}" alt="Awesome Image" style="height: 90px;width: 124px;">  <span class="float-end" >
                                    <strong class="header_to_no" >No.:</strong> <strong class="input-group search-area mb-md-0 mb-3" > {{ $data->application_no }} </strong>  </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-xl-12 col-lg-3 col-md-6 col-sm-12">
                                        <h1 class="top_name">Navi Mumbai Municipal Corporation,Education Department</h1>
                                        <div class="top_namesecsection">Head Office,Plot No.1,Palm Beach Rd, Sector 15,CBD Belapur , Navi Mumbai,</div>
                                        <div class="top_namesecsection"><b>Tel.no.</b>22 2756 7070 <b>Web Site:</b> www.nmmcedu.com <b>Email:</b> nmmcedu@gmail.com</div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                      <h1 class="top_name_app">APPLICATION FORM 2023-24</h1>
                                </div>
                                <div class="row">
                                    <div class="col-xl-4 col-lg-6 col-md-10 col-sm-12">

                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-10 col-sm-12">

                                    </div>
                                    <div class="col-xl-4 col-lg-3 col-md-6 col-sm-12">
                                        <strong class="input-group search-area photo" >

                                               @if (!empty($document))
                                                @foreach ($document as $key => $value)

                                                    {{-- Check if the property "document" exists in the object --}}
                                                    @if (property_exists($value, 'document') && $value->document_id == '5')
                                                        <img src="{{ asset('documents/'.$value->document) }}" alt="" style="width: 100%; height: 100%;">
                                                    @endif
                                                @endforeach
                                            @endif
                                        </strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-9 col-lg-9 col-md-10 col-sm-12">
                                        <h6>To,</h6>
                                        <div> <strong>The Principal</strong> </div>
                                        <div>NMMC School no.93 CBSE School, Sector-50, Nerul.</div>
                                    </div>
                                    {{-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                                        <strong class="input-group search-area photo" >Affix Identity Card Size Photograph of the student</strong>
                                    </div> --}}
                                </div>
                                <div>Resp, Sir/Madam</div>
                                <p style="color:#09070b;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I, the undersigned, request you to kindly give admission for my son/daughter/ward in <b>Nursery </b>of your school.</p>

                                <div>Details are given Below.</div>
                                {{-- table start  --}}

                                <table>
                                    <thead>
                                        {{-- <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Age</th>
                                        </tr> --}}
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td style="width: 19%;height: 57px;"><b>1.&nbsp;Name in full (in CAPITAL)</b></td>
                                            <td style="text-align: start;width: 59%;">{{ $data->lastname }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $data->firstname }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $data->middlename }}</td>
                                        </tr>
                                        <table>
                                                <tr>
                                                    <td colspan="2" style="width: 300px;"><b>2. &nbsp;Age on (31 Dec 2023 )</b></td>
                                                    <td colspan="1" style="width: 145px;"><b>Male / Female : </b></td>
                                                    <td rowspan="3" style="width: 148px;"><b>Date of Birth</b></td>

                                                    <?php

                                                    $dateOfBirth = $data->dob;
                                                    $dateTime = new DateTime($dateOfBirth);

                                                    $day = $dateTime->format('d');   // Day
                                                    $month = $dateTime->format('m'); // Month
                                                    $year = $dateTime->format('Y');  // Year

                                                    // inword calculate



                                                    ?>
                                                    <td colspan="3" style="width: 200px;"><b>In Figure</b></td>


                                                    <td rowspan="3" style="width: 161px;"><b>In Word :- <br> </b></td>

                                                </tr>
                                                <tr>
                                                    <td colspan="1">Years</td>
                                                    <td colspan="1">Month</td>

                                                    <td rowspan="2">&nbsp;&nbsp;{{ $data->gender }}</td>

                                                    <td rowspan="2">{{$day}}</td>

                                                    <td rowspan="2">{{$month}}</td>
                                                    <td rowspan="2">{{$year}}</td>



                                                </tr>
                                                <tr>

                                                    <?php
                                                        $dateOfBirth = $data->dob;
                                                        $referenceDate ='2023-12-31';
                                                            //print_r($dateOfBirth);

                                                            $dob = new DateTime($dateOfBirth);
                                                            $refDate = new DateTime($referenceDate);

                                                            // Calculate the interval between the two dates
                                                            $interval = $dob->diff($refDate);

                                                            // Get the number of years and months
                                                            $years = $interval->y;
                                                            $months = $interval->m;

                                                        ?>
                                                    <td style="height: 35px;">{{$years}}</td>
                                                    <td style="height: 35px;">{{$months}}</td>

                                                </tr>



                                         <table>

                                            <tr>
                                                <td style="height: 55px;width: 300px;text-align: left;"><b>3. Nationality :</b> &nbsp;&nbsp;{{$data->nationality}}</td>
                                                <td style="height: 55px;width: 260px;text-align: left;"><b>4. Place of Birth :</b> &nbsp;&nbsp;{{$data->birth_place}}</td>
                                                <td style="height: 55px;width: 394px;text-align: left;"><b>5. Mother Tongue: </b>&nbsp;&nbsp;{{$data->mother_tongue}}</td>

                                            </tr>
                                            <table>
                                            <tr>
                                                <td style="height: 55px;width: 300px;text-align: left;"><b>6. Religion: </b>&nbsp;&nbsp;{{$data->religion}}</td>
                                                <td style="height: 55px;width: 655px;text-align: left;"><b>4. If member of backward community,specify(SC,ST,OBC,etc.):</b> &nbsp;&nbsp;{{$data->cast_name}}</td>

                                            </tr>
                                            <tr>
                                                <td style="height: 16px;width: 300px;text-align: left;"></td>
                                                <td style="height: 16px;width: 655px;text-align: left;"></td>
                                            </tr>
                                        </table>

                                        <table>
                                            <tr>
                                                <td style="height: 200px;width: 437px;text-align: left;"> <b>8 . Father's Details </b><br> Name of Father :- &nbsp;&nbsp;{{$data->father_name}}<br> Qualification :- &nbsp;&nbsp;{{$data->father_qualification}}<br> Occupation/Profession  :- &nbsp;&nbsp;{{$data->father_profession}}<br> Office Address(if Applicable) :-&nbsp;&nbsp;{{$data->father_office_address}}<br><br><br><br><br>Mobile.No. :- &nbsp;&nbsp;{{$data->father_mobile_no}}<br>Email :-&nbsp;&nbsp;{{$data->father_email}}<br>Adharcard No :-&nbsp;&nbsp;{{$data->father_adhaar_no}}<br>PanCard No. :-&nbsp;&nbsp;{{$data->father_pan_no}}</td>
                                                <td style="height: 200px;width: 418px;text-align: left;"><b>8 . Mother's Details </b><br> Name of Mother :- &nbsp;&nbsp;{{$data->mother_name}}<br> Qualification :- &nbsp;&nbsp;{{$data->mother_qualification}}<br> Occupation/Profession  :- &nbsp;&nbsp;{{$data->mother_profession}}<br> Office Address(if Applicable) :-&nbsp;&nbsp;{{$data->mother_office_address}}<br><br><br><br><br>Mobile.No. :- &nbsp;&nbsp;{{$data->mother_mobile_no}}<br>Email :-&nbsp;&nbsp;{{$data->mother_email}}<br>Adharcard No :-&nbsp;&nbsp;{{$data->mother_adhaar_no}}<br>PanCard No. :-&nbsp;&nbsp;{{$data->mother_pan_no}}</td>

                                            </tr>
                                            <tr>
                                                <td style="height: 80px;width: 463px;text-align: left;"> <b>12. Residential address(local) of parent/Guardian :-</b> &nbsp;&nbsp;{{$data->local_address}}</td>
                                                <td style="height:80px;width: 492px;text-align: left;"><b>13. Permanent Address of parent :- </b>&nbsp;&nbsp;{{$data->permanent_address}}</td>

                                            </tr>
                                        </table>
                                        <table>
                                            <td style="height: 20px;width: 955px;text-align: left;"> <b>14. Distance Between School & Residence :-</b> &nbsp;&nbsp;{{$data->distance_name}} Km</td>
                                        </table>

                                    </tbody>
                                </table>
                              {{-- table end  --}}

                              <div class="last_para">
                                   <p style="color:#09070b;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I <b> hereby Declare that the information given above is thrue to the best of my Knowledge.</b> If the information/documents goes wrong, i am aware that I will be eligible for action as a rule and my <b>son/daughter/ward  </b> admission will be cancelled at any level.</p>

                              </div>

                              <div class="row" style="margin-top: 31px;">
                                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12">
                                    <?php
                                        $currentDate = now(); // Current date and time
                                        $currentDateOnly = now()->toDateString();
                                       $mainDate =  date('d-m-Y', strtotime( $currentDateOnly));
                                        ?>
                                    <h6>Date : {{$mainDate}}</h6>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12">



                                    <h6 style="text-align: end;">


                                        @if (!empty($document))
                                            @foreach ($document as $key => $value)
                                                @if (property_exists($value, 'document') && $value->document_id == '6')
                                                    <img src="{{ asset('documents/'.$value->document) }}" alt="image" class="img-fluid rounded" width="200" height="100" style="max-height: 100px;">
                                                @endif
                                            @endforeach
                                        @endif
                                             <br>
                                        Signature of Father's/Mother's/Gardian


                                    </h6>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 31px;">
                                <h6>Documents Required :</h6>
                                <div class=" col-xl-6 col-lg-6 col-md-10 col-sm-12">

                                    <ul>
                                         <li>1. Birth Certificate of Student</li>
                                         <li>3. Cast Certificate of Student/Parent</li>
                                    </ul>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                     <ul>
                                        <li> 2. Adharcard of Student/Parent</li>
                                        <li>4. Registered Rent agreement / light bill (6th month old)</li>

                                     </ul>
                                </div>
                            </div>
                         <hr style="border: 2px dashed #000;">

                        <div class="table-responsive">
                            <h3 class="last_name_app">Acknowledgment Slip</h3>
                        </div>

                        <div class="row" style="margin-top: 31px;">

                            <div class=" col-xl-6 col-lg-6 col-md-10 col-sm-12">

                                <ul>
                                     <li style="margin-bottom: 27px;">Name : _______________________</li>
                                     <li>Date :- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/2023</li>
                                </ul>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                 <ul style="    float: inline-end;">
                                    <li style="margin-bottom: 27px;display: flex;"> Admission for Std. <strong class="footer_to_no" ></strong> <strong class="input-group search-area mb-md-0 mb-3" > </strong>  </li>
                                    <li style="    display: flex;">Application No. <strong class="footer_to_no" ></strong> <strong class="input-group search-area mb-md-0 mb-3" > </strong></li>

                                </ul>
                            </div>
                        </div>

                        <div  style="margin-top: 66px;
                        float: inline-end;
                        font-size: 16px;
                        color: black;">

                            Signature of Receiver

                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- <button  class="btn btn-primary" type="button" onClick="printDiv('divToPrint')" ><i class="fa fa-print fa-lg text-light"></i> &nbsp;&nbsp;Print</button> --}}
        </div>

        <!--**********************************
            Content body end
        ***********************************-->

        <script src="{{asset('assets/vendor/global/global.min.js')}}"></script>
	<script src="{{asset('assets/vendor/chart.js/Chart.bundle.min.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
	<!-- Apex Chart -->
	<script src="{{asset('assets/vendor/apexchart/apexchart.js')}}"></script>
	<!-- Chart piety plugin files -->
    <script src="{{asset('assets/vendor/peity/jquery.peity.min.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-nice-select/js/jquery.nice-select.min.js')}}"></script>
	<!--swiper-slider-->
	<script src="{{asset('assets/vendor/swiper/js/swiper-bundle.min.js')}}"></script>


    <!-- Datatable -->
    <script src="{{asset('assets/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins-init/datatables.init.js')}}"></script>

	<!-- Dashboard 1 -->
	<script src="{{asset('assets/js/dashboard/dashboard-1.js')}}"></script>
	<script src="{{asset('assets/vendor/wow-master/dist/wow.min.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap-datetimepicker/js/moment.js')}}"></script>
	<script src="{{asset('assets/vendor/datepicker/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap-select-country/js/bootstrap-select-country.min.js')}}"></script>

	<script src="{{asset('assets/js/dlabnav-init.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>
	<script src="{{asset('assets/js/demo.js')}}"></script>

<script>

    $(document).ready(function () {
        // Function to calculate and update age when Date of Birth changes
        $('#inputGroupPrepend45').on('change', function () {
            var dob = $(this).val();
            if (dob) {
                var age = calculateAge(new Date(dob));
                $('#age').val(age);
            }
        });

        // Function to calculate age based on Date of Birth
        function calculateAge(dob) {
            var today = new Date();
            var birthDate = new Date(dob);
            var age = {};

            // Calculate years
            age.years = today.getFullYear() - birthDate.getFullYear();

            // Calculate months
            var months = today.getMonth() - birthDate.getMonth();
            if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
                age.months = 12 + months;
            } else {
                age.months = months;
            }

            // Calculate days
            var days = today.getDate() - birthDate.getDate();
            if (days < 0) {
                var lastMonth = new Date(today.getFullYear(), today.getMonth() - 1, birthDate.getDate());
                age.days = Math.floor((today - lastMonth) / (1000 * 60 * 60 * 24));
            } else {
                age.days = days;
            }

            // Format the age for display
            var formattedAge = age.years + ' years, ' + age.months + ' months, ' + age.days + ' days';

            return formattedAge;
        }
    });
</script>


</body>

</html>

{{-- @endsection --}}
