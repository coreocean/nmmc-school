@extends('layouts.master')

 @section('content')

 <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">

                <!-- Column starts -->

                <!-- Column ends -->
                <!-- Column starts -->
                <div class="col-xl-12">
                    <div class="card" id="accordion-three">
                        <div class="card-header flex-wrap d-flex justify-content-between px-3">
                            <div>
                            <h4 class="card-title">Standard List</h4>

                            </div>
                            <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                                <li class="nav-item" role="presentation">
                                <!-- Button trigger modal -->
                               <a href="{{ route('admin_register.create') }}"><button type="button" class="btn btn-primary" >
                                 + Add Admin
                                </button></a>
                                </li>

                                </ul>
                                </div>

                                <!-- /tab-content -->
                                <div class="tab-content" id="myTabContent-2">
                                <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                                <div class="card-body p-0">
                                <div class="table-responsive">
                                <table id="example3" class="display table" style="min-width: 845px">

                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>School</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>User Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($list as $key => $value)
                                                    <tr>
                                                        <td><b>{{ $key+1 }}</b></td>
                                                        <td><b>{{ $value->school_name }} </b></td>
                                                        <td><b>{{ $value->first_name }} {{ $value->middle_name }} {{ $value->last_name }}</b></td>
                                                        <td><b>{{ $value->email }} </b></td>
                                                        <td><b>{{ $value->mobile_no }} </b></td>
                                                        <td><b>
                                                          <?php
                                                            if($value->usertype == 0){

                                                                $type = 'School Wise Admin' ;

                                                            }else if($value->usertype == 1){
                                                                $type = 'User' ;

                                                            }else if($value->usertype == 2){

                                                                $type = 'School Wise Clerk';
                                                            }else {
                                                                $type = 'Super Admin';
                                                            }

                                                            ?>

                                                            {{ $type }}

                                                        </b></td>

                                                        <td>
                                                            <div class="d-flex">
                                                                <a href="{{ route('admin_register.edit', $value->id) }}" class="btn btn-primary shadow btn-xs sharp me-1"><i class="fas fa-pencil-alt"></i></a>
                                                                <form action="{{ route('admin_register.destroy', $value->id) }}" method="post">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                  <button class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i>
                                                                  </button>
                                                               </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach


                                </tbody>
                            </table>
                            </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- /tab-content -->

                    </div>
                </div>



        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>

@endsection
