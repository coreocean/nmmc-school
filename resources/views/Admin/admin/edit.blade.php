@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Edit Admin </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('admin_register.update', $data->id) }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf
                                @if (!empty($data->id) || 1 == 1)
                                    <input type="hidden" name="_method" value="PATCH">
                                @endif

                                <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">

							<div class="row">
								<div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">First Name <span class="required">*</span></label>
									  <input type="text" class="form-control"  value="{{ $data->first_name }}" name="first_name" id="first_name" placeholder="Enter First Name " required>
									</div>
								</div>
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Middle Name <span class="required">*</span></label>
									  <input type="text" class="form-control"  value="{{ $data->middle_name }}" name="middle_name" id="middle_name" placeholder="Enter Middle Name ">
									</div>
								</div>
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Last Name <span class="required">*</span></label>
									  <input type="text" class="form-control" value="{{ $data->last_name }}"  name="last_name" id="last_name" placeholder="Enter Last Name ">
									</div>
								</div>

							</div>
                            <div class="row">
								<div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Mobile Number <span class="required">*</span></label>
									  <input type="text" class="form-control"  value="{{ $data->mobile_no }}" name="mobile_no" id="mobile_no" placeholder="Enter Mobile Number  " required>
									</div>
								</div>
                                <div class="col-xl-4 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Email <span class="required">*</span></label>
									  <input type="text" class="form-control"  value="{{ $data->email }}" name="email" id="email" placeholder="Enter Email Name ">
									</div>
								</div>

								<div class="col-xl-4 col-sm-6">
								  <label class="text-label form-label">School Name<span class="required">*</span></label>
                                    <select class=" default-select form-control wide" name="school_id"  aria-label="Default select example" >
                                        <option value="" selected="selected">Select School</option>
                                            @foreach($school as  $schools)
                                            <option value="{{$schools->id}}" {{$data->school_id == $schools->id ? 'selected':''}}>{{$schools->school_name}}</option>
                                            @endforeach
                                    </select>
								    <span class="text-danger">@error('school_id'){{$message}}@enderror</span>
								</div>

                                <div class="col-xl-4 col-sm-6">
                                    <label class="form-label text-primary">User Type<span class="required">*</span></label>
                                        <select class=" default-select form-control wide" name="usertype"  aria-label="Default select example" >
                                            <option value="" selected="selected">Select User Type</option>
                                            <option value="0" {{ $data->usertype == "0" ? 'selected' : '' }}>School Wise Admin</option>
                                            <option value="2" {{ $data->usertype == "2" ? 'selected' : '' }}>School Wise Clerk</option>
                                            <option value="3" {{ $data->usertype == "3" ? 'selected' : '' }}>SuperAdmin</option>
                                        </select>
                                    <span class="text-danger">@error('usertype'){{$message}}@enderror</span>
                                </div>

                                <div class="col-xl-4 col-sm-6">
                                    <label class="text-label form-label">Academy Year<span class="required">*</span></label>
                                      <select class=" default-select form-control wide" name="academy_year"  aria-label="Default select example" >
                                          <option value="" selected="selected">Select Year</option>
                                           @foreach($ayear as  $year)
                                              <option value="{{$year->id}}" {{$data->academy_year == $year->id ? 'selected':''}}>{{$year->year}}</option>
                                           @endforeach
                                      </select>
                                      <span class="text-danger">@error('academy_year'){{$message}}@enderror</span>
                                  </div>


							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
