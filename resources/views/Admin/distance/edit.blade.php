@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Edit Distance </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('distance_master.update', $data->id) }}" class="new-added-form" enctype="multipart/form-data">
                                @csrf
                            @if (!empty($data->id) || 1 == 1)
                                <input type="hidden" name="_method" value="PATCH">
                            @endif

                            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">
							<div class="row">
								<div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Distance in KM <span class="required">*</span></label>
									  <input type="text" class="form-control" value="{{ $data->distance_name }}" name="distance_name" id="distance_name" placeholder="Enter Distance Name ">
									</div>

								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>

        <!--**********************************
            Content body end
        ***********************************-->

@endsection
