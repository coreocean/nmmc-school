@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Edit Cast </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('cast_master.update', $data->id) }}" class="new-added-form" enctype="multipart/form-data">
                                @csrf
                            @if (!empty($data->id) || 1 == 1)
                                <input type="hidden" name="_method" value="PATCH">
                            @endif

                            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">
							<div class="row">
                                <div class="col-xl-6 col-sm-6">
                                    <label class="form-label text-primary">Religen<span class="required">*</span></label>
                                       <select class=" default-select form-control wide" name="religen_id"  if="religen_id" aria-label="Default select example" >
                                           <option value="" selected="selected">Select Religen</option>
                                               @foreach($religion as  $row)
                                               <option value="{{ $row->id }}" @if($data->religion_id == $row->id) selected @endif>
                                                {{ $row->religion_name }}
                                               </option>
                                               @endforeach
                                       </select>
                                       <span class="text-danger">@error('religen_id'){{$message}}@enderror</span>
                                   </div>
								<div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Cast Name <span class="required">*</span></label>
									  <input type="text" class="form-control" value="{{ $data->cast_name }}" name="cast_name" id="cast_name" placeholder="Enter Cast Name ">
									</div>

								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>

        <!--**********************************
            Content body end
        ***********************************-->

@endsection
