@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Add Notification </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ url('/notification_master') }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf

							<div class="row mb-4">
								<div class="col-xl-4 col-sm-6">
                                    <label class="form-label text-primary">School Name<span class="required">*</span></label>
                                        <select class=" default-select form-control wide" name="school_id"  aria-label="Default select example" >
                                            <option value="" selected="selected">Select School</option>
                                            @foreach($school as  $schools)
                                            <option value="{{$schools->id}}">{{$schools->school_name}}</option>
                                            @endforeach
                                        </select>
                                    <span class="text-danger">@error('school_id'){{$message}}@enderror</span>
                                </div>

                                <div class="col-xl-4 col-sm-6">
                                    <label class="form-label text-primary">Academy Year<span class="required">*</span></label>
                                        <select class=" default-select form-control wide" name="academy_year"  aria-label="Default select example" >
                                            <option value="" selected="selected">Select Year</option>
                                            @foreach($ayear as  $year)
                                            <option value="{{$year->id}}">{{$year->year}}</option>
                                            @endforeach
                                        </select>
                                    <span class="text-danger">@error('academy_year'){{$message}}@enderror</span>
                                </div>

                                <div class="col-xl-4 col-sm-6">
                                    <label class="form-label text-primary">Title<span class="required">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Notification " required>
                                    <span class="text-danger">@error('academy_year'){{$message}}@enderror</span>
                                </div>

                            </div>

							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>

        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
