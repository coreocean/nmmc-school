@extends('layouts.master')

 @section('content')

 <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">

                <!-- Column starts -->

                <!-- Column ends -->
                <!-- Column starts -->
                <div class="col-xl-12">
                    <div class="card" id="accordion-three">
                        <div class="card-header flex-wrap d-flex justify-content-between px-3">
                            <div>

                                @if( $status == 0)
                                  <h4 class="card-title">Pending Application List</h4>
                                @elseif ($status == 1)
                                  <h4 class="card-title">Approve Application List</h4>
                                @else
                                <h4 class="card-title">Rejected Application List</h4>
                                @endif

                            </div>
                            {{-- <div>

                                <!-- Button trigger modal -->
                               <a href="{{ route('distance_master.create') }}"><button type="button" class="btn btn-primary" >
                                 + Add Distance
                                </button></a>
                            </div> --}}
                            </div>
                            <!-- /tab-content -->
                            <div class="tab-content" id="myTabContent-2">
                            <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                            <div class="card-body p-0">
                            <div class="table-responsive">
                            <table id="example3" class="display table" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Application No.</th>
                                        <th>Student Name</th>
                                        <th>Date of Birth</th>
                                        <th>Standerd</th>
                                        <th>Father Contact No.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($appData as $key => $value)
                                                    <tr>
                                                        <td><b>{{ $key+1 }}</b></td>
                                                        <td>{{ $value->application_no }}</td>
                                                        <td>{{ $value->firstname }} {{ $value->middlename }} {{ $value->lastname }}</td>
                                                        <td>{{ $value->dob }}</td>
                                                        <td>{{ $value->standard_name }}</td>
                                                        <td>{{ $value->father_mobile_no }}</td>

                                                        <td>

                                                            <div class="d-flex">
                                                                {{-- <a href="{{ url("/generate_pdf/{$value->id}") }}" class="btn  shadow btn-xs sharp me-1" style="" target="_blank"><i class="fas fa-file-pdf" style="    color: #4d44b5;
                                                                    font-size: 23px;"></i></a> --}}

                                                                <a href="{{ url("/view_application/{$value->id}") }}" class="btn  shadow btn-xs sharp me-1" style=""><i class="fa fa-eye" style="    color: #4d44b5;
                                                                    font-size: 23px;"></i></a>

                                                                {{-- <form action="{{ route('distance_master.destroy', $value->id) }}" method="post">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                  <button class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i>
                                                                  </button>
                                                               </form> --}}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach


                                </tbody>
                            </table>
                            </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- /tab-content -->

                    </div>
                </div>



        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>

@endsection
