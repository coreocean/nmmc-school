@extends('layouts.master')
<style>
    a.btn.btn-primary.shadow.btn-xs.sharp.me-1 {
    padding: 23px;
    font-size: 25px;
}
#progressbar li {

    width: 25% !important;;

}
    #msform .action-button1 {
    width: 100px;
    background: #fb7d5b;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
#msform .action-button2 {
    width: 100px;
    background: #189848;;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
    </style>
@section('content')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">Add Student Application </h5>
                        </div>

                        <div class="card-body">
                            <form id="msform" action="{{ route('student_application.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active" id="account"><strong>Student Personal Info</strong></li>
                                    <li id="personal"><strong>Parent's Details</strong></li>
                                    {{-- <li id="payment"><strong>Mother</strong></li> --}}
                                    <li id="confirm"><strong>Address</strong></li>
                                    <li id="upload"><strong>Document Upload</strong></li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> <br> <!-- fieldsets -->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Student Personal Info:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 1 - 4</h2>
                                            </div>


                                            <div class="col-lg-6 mb-2">
                                                <label class="text-label form-label">Admission for Standard<span class="required">*</span></label>
                                                <input type="text" name="standard_id"  value="{{ $appview->standard_name }}" class="form-control" placeholder="First Name" readonly>

                                                <span class="text-danger">
                                                    @error('standard_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>

                                            <div class="col-lg-6 mb-2">
                                                <label class="text-label form-label">Academic Year<span class="required">*</span></label>
                                                <input type="text" name="year"  value="{{ $appview->year }}" class="form-control" placeholder="First Name" readonly >

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            {{-- <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="twins" id="twins" value="2">
                                                    <label class="text-label form-label">Twins</label>
                                                </div>
                                            </div> --}}

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="firstname" class="form-control" placeholder="First Name" value="{{ $appview->firstname }}" readonly>
                                                    <span class="text-danger">
                                                        @error('firstname')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="middlename" class="form-control" placeholder="Middle Name" value="{{ $appview->middlename }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="lastname"  class="form-control" placeholder="Last Name" value="{{ $appview->lastname }}" readonly>
                                                    <span class="text-danger">
                                                        @error('lastname')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="dob" id="inputGroupPrepend45" value="{{ $appview->dob }}" readonly aria-describedby="inputGroupPrepend45" placeholder="Enter your D.O.B">
                                                    <span class="text-danger">
                                                        @error('dob')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="age" id="age" value="{{ $appview->age }}" aria-describedby="inputGroupPrepend2" readonly placeholder="Age" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>

                                                <?PHP
                                                if($appview->gender == 1){
                                                    $gender = 'Male';

                                                }else{

                                                    $gender = 'Female';

                                                }

                                                ?>
                                                <input type="text" class="form-control" name="age" id="age" value="{{ $gender }}" aria-describedby="inputGroupPrepend2" placeholder="Age" readonly>

                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="nationality" value="Indian" aria-describedby="inputGroupPrepend2" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="birth_place" value="{{ $appview->birth_place }}" aria-describedby="inputGroupPrepend2" placeholder="Place Of Birth" readonly>
                                                    <span class="text-danger">
                                                        @error('birth_place')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_tongue" value="{{ $appview->mother_tongue }}" aria-describedby="inputGroupPrepend2" placeholder="Mother Tongue" readonly>
                                                    <span class="text-danger">
                                                        @error('mother_tongue')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="religion" value="{{ $appview->religion_name }}" aria-describedby="inputGroupPrepend2" placeholder="Religion" readonly>
                                                    <span class="text-danger">
                                                        @error('religion')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <input type="text" class="form-control" name="religion" value="{{ $appview->cast_name }}"  aria-describedby="inputGroupPrepend2" placeholder="Religion" readonly>
                                                <span class="text-danger">@error('cast_id'){{$message}}@enderror</span>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="adhaar_no" value="{{ $appview->adhaar_no }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" maxlength="12" readonly>
                                                    <span class="text-danger">
                                                        @error('adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Types of Blood Group</label>


                                                <?php
                                                $bloods = '';
                                                if(!empty($appview->blood_group) && $appview->blood_group == ''){

                                                    $bloods = $appview->blood_group;
                                                }else{

                                                    if($appview->blood_group == 1){
                                                        $bloods = 'A+';
                                                    }elseif($appview->blood_group == 2){
                                                        $bloods = 'A-';
                                                    }elseif($appview->blood_group == 3){
                                                        $bloods = 'B+';
                                                    }elseif($appview->blood_group == 4){
                                                        $bloods = 'B-';
                                                    }elseif($appview->blood_group == 5){
                                                        $bloods = 'AB+';
                                                    }elseif($appview->blood_group == 6){
                                                        $bloods = 'AB-';
                                                    }elseif($appview->blood_group == 7){
                                                        $bloods = 'O+';
                                                    }elseif($appview->blood_group == 8){
                                                        $bloods = 'O-';
                                                    }

                                            }
                                                ?>

                                                <input type="text" class="form-control" readonly name="adhaar_no" value="{{ $bloods }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" maxlength="12">

                                            </div>
                                        </div>
                                        <br>

                                        <div class="row">
                                            <h2 class="fs-title second_section" style="display:none">Second Child Form:</h2>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="t_firstname" readonly class="form-control" placeholder="First Name" value="{{ old('t_firstname') }}">

                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="t_middlename" readonly class="form-control" placeholder="Middle Name" value="{{ old('t_middlename') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="t_lastname" value="{{ old('t_lastname') }}" class="form-control" placeholder="Last Name">

                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="t_dob" id="inputGroupPrepend45" aria-describedby="inputGroupPrepend45" placeholder="Enter your D.O.B">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_age" id="age" aria-describedby="inputGroupPrepend2" placeholder="Age" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="t_gender">
                                                    <option value="" selected="selected">Select Gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                </select>

                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_nationality" value="Indian" aria-describedby="inputGroupPrepend2" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_birth_place" value="{{ old('t_birth_place') }}" aria-describedby="inputGroupPrepend2" placeholder="Place Of Birth">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_mother_tongue" value="{{ old('t_mother_tongue') }}" aria-describedby="inputGroupPrepend2" placeholder="Mother Tongue">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_religion" value="{{ old('t_religion') }}" aria-describedby="inputGroupPrepend2" placeholder="Religion">

                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <input type="text" class="form-control" name="religion" value="{{ $appview->cast_name }}"  aria-describedby="inputGroupPrepend2" placeholder="Religion" >
                                                <span class="text-danger">@error('cast_id'){{$message}}@enderror</span>

                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_adhaar_no" value="{{ old('t_adhaar_no') }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" maxlength="12">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Types of Blood Group</label>
                                                <select class=" default-select form-control wide" readonly name="t_blood_group" aria-label="Default select example">
                                                    <option value="">Select Blood Group</option>
                                                    <option value="1">A+</option>
                                                    <option value="2">A-</option>
                                                    <option value="3">B+</option>
                                                    <option value="4">B-</option>
                                                    <option value="5">AB+</option>
                                                    <option value="6">AB-</option>
                                                    <option value="7">O+</option>
                                                    <option value="8">O-</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div> <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Father's Details:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 2 - 4</h2>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label"> Father's First Name<span class="required">*</span></label>
                                                    <input type="text" name="father_first_name" readonly value="{{ $appview->father_first_name }}" class="form-control" placeholder="Father's First Name">
                                                    <span class="text-danger">
                                                        @error('father_first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Middle Name <span class="required">*</span></label>
                                                    <input type="text" name="father_middle_name" readonly value="{{ $appview->father_middle_name }}" class="form-control" placeholder="Father's Middle Name ">
                                                    <span class="text-danger">
                                                        @error('father_middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Last Name <span class="required">*</span></label>
                                                    <input type="text" name="father_last_name" readonly value="{{ $appview->father_last_name }}" class="form-control" placeholder="Father's Last Name">
                                                    <span class="text-danger">
                                                        @error('father_last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="father_mobile_no" readonly class="form-control" value="{{ $appview->father_mobile_no }}" placeholder="(+1)408-357-9007" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('father_mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" readonly name="father_alt_contact" value="{{ $appview->father_alt_contact }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" readonly name="father_email" value="{{ $appview->father_email }}" placeholder="Email Address">
                                                    <span class="text-danger">
                                                        @error('father_email')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" readonly name="father_qualification" value="{{ $appview->father_qualification }}" placeholder="Qualification">

                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" readonly name="father_profession" value="{{ $appview->father_profession }}" placeholder="Occupation/profession">
                                                    <span class="text-danger">
                                                        @error('father_profession')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" readonly name="father_designation" value="{{ $appview->father_designation }}" placeholder="Designation">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" readonly name="father_office_address" value="{{ $appview->father_office_address }}" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="father_adhaar_no" readonly value="{{ $appview->father_adhaar_no }}" class="form-control" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('father_adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="father_pan_no" readonly value="{{ $appview->father_pan_no }}" class="form-control">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>

                                            <h2 class="fs-title">Mother's Details:</h2>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's First Name <span class="required">*</span></label>
                                                    <input type="text" name="mother_first_name" readonly class="form-control" value="{{ $appview->mother_first_name }}" placeholder="Mother's First Name">
                                                    <span class="text-danger">
                                                        @error('mother_first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_middle_name" readonly class="form-control" value="{{ $appview->mother_middle_name }}" placeholder="Mother's Middle Name">
                                                    <span class="text-danger">
                                                        @error('mother_middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Last Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_last_name" readonly class="form-control" value="{{ $appview->mother_last_name }}" placeholder="Mother's Last Name">
                                                    <span class="text-danger">
                                                        @error('mother_last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_mobile_no" readonly value="{{ $appview->mother_mobile_no }}" class="form-control" placeholder="(+1)408-357-9007" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('mother_mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" readonly name="mother_email" value="{{ $appview->mother_email }}" placeholder="Email Address">
                                                    <span class="text-danger">
                                                        @error('mother_email')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" readonly name="mother_qualification" value="{{ $appview->mother_qualification }}" placeholder="Qualification">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" readonly  name="mother_profession" value="{{ $appview->mother_profession }}" placeholder="Occupation/profession">
                                                    <span class="text-danger">
                                                        @error('mother_profession')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" readonly name="mother_designation" value="{{ $appview->mother_designation }}" placeholder="Designation">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" readonly name="mother_alt_contact" value="{{ $appview->mother_alt_contact }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" readonly name="mother_office_address" value="{{ $appview->mother_office_address }}" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_adhaar_no" readonly value="{{ $appview->mother_adhaar_no }}" class="form-control" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('mother_adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="mother_pan_no" readonly value="{{ $appview->mother_pan_no }}" class="form-control">

                                                </div>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Current Residential Address:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 3 - 4</h2>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Residential House Type<span class="required">*</span></label>

                                                <?php

                                                 if($appview->residential_type ==1){
                                                    $residential = 'Rental';

                                                 }else{
                                                    $residential = 'Own';
                                                 }

                                                 ?>
                                                <input type="text" name="mother_pan_no" readonly value="{{  $residential }}" class="form-control">

                                            </div>

                                            <div id="months_section" class="col-lg-4 mb-2" style="display: none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">How many months in current address<span class="required">*</span></label>
                                                    <input type="text" name="no_of_months" readonly value="{{ $appview->no_of_months }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="house_no" readonly value="{{ $appview->house_no }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="area" readonly value="{{ $appview->area }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="landmark" readonly value="{{ $appview->landmark }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="pincode" readonly value="{{ $appview->pincode }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="city_name" readonly value="{{ $appview->city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <h2 class="fs-title">Permanent Address:</h2>

                                            <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="same_address" readonly value="1">
                                                    <label class="text-label form-label">Same as above</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="p_house_no" readonly value="{{ $appview->p_house_no }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="p_area" readonly value="{{ $appview->p_area }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="p_landmark" readonly value="{{ $appview->p_landmark }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="p_pincode" readonly value="{{ $appview->p_pincode }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="p_city_name" readonly value="{{ $appview->p_city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>

                                            <h2 class="fs-title">Distance:</h2>
                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Distance between School & Resident<span class="required">*</span></label>
                                                <input type="text" name="distance_id" readonly value="{{ $appview->distance_id }}" class="form-control">

                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">School Name<span class="required">*</span></label>
                                                <input type="text" id="school_id" readonly name="school_id" value="{{ $appview->school_name }}" class="form-control" readonly>
                                                 <span class="text-danger">@error('school_id'){{$message}}@enderror</span>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Application Date<span class="required">*</span></label>

                                                    <input type="text" id="current_date" readonly  name="current_date" value="{{ now()->format('Y-m-d') }}" class="form-control" readonly>
                                                </div>
                                            </div>


                                        </div>
                                    </div><input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>



                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Document Uploads:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 4 - 4</h2>
                                            </div>

                                            @foreach($document as $documents)
                                                <div class="col-lg-4 mb-2">
                                                    <div class="mb-3">
                                                    <label>{{$documents->document}} </label>
                                                        <input type="hidden" name="document_id[]" class="form-control" value="{{$documents->id}}">
                                                        {{-- <input type="file" name="document_file[]" class="form-control" multiple><br> --}}
                                                        <a href="{{ asset('documents/'.$documents->document_file)}}" class="btn btn-primary shadow btn-xs sharp me-1" target="_blank"> <i class="fas fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach


                                        </div>
                                    </div>


                                    @if(Auth::user()->usertype == 0 || Auth::user()->usertype == 3)
                                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />


                                    @elseif(Auth::user()->usertype == 2)
                                        <?php if($appview->c_status == 0){ ?>
                                                <a href="{{ url('/status_application_list/0') }}"><button type="button"  class="btn btn-danger action-button1">Cancel</button></a>&nbsp;&nbsp;

                                                <button type="button" class="btn btn-warning waves-effect m-r-10 action-button2" data-bs-toggle="modal" data-bs-target="#basicModal">Reject</button>

                                                <a href='{{ url("/approve_application/{$appview->id}") }}'><button  type="button" class="action-button btn btn-success  m-r-10">Approve </button> </a>

                                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />

                                        <?php }elseif($appview->c_status == 1){?>

                                                <a href="{{ url('/status_application_list/1') }}"><button type="button"  class="btn btn-danger action-button1">Cancel</button></a>&nbsp;&nbsp;

                                                <button type="button" class="btn btn-warning waves-effect m-r-10 action-button2" data-bs-toggle="modal" data-bs-target="#basicModal">Reject</button>

                                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />


                                        <?php }else {?>

                                                <a href="{{ url('/status_application_list/2') }}"><button type="button"  class="btn btn-danger action-button1">Cancel</button></a>&nbsp;&nbsp;
                                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />

                                        <?php } ?>



                                    @endif

                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="basicModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Rejected By Clerk</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form method="POST" action="{{ url('/reject_applicationbyclerk', $appview->id ) }}" enctype="multipart/form-data">
                @csrf

                <input type="hidden" class="form-control " id="id" name="id" value="{{ $appview->id }}" >

            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-sm-12"><strong>Reasons for Rejection  : <span style="color:red;">*</span> </strong></label>
                    <div class="col-sm-12 col-md-12 p-2">
                        <textarea type="text" name="reject_reason" id="reject_reason"  required class="form-control @error('reject_reason') is-invalid @enderror" value="{{ old('reject_reason') }}" placeholder="Enter Reasons for Rejection"></textarea>
                        @error('reject_reason')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save </button>
            </div>
            </form>
        </div>
    </div>
</div>


    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        function toggleSecondSection() {
            var twinsCheckbox = document.getElementById('twins');
            var secondSection = document.getElementsByClassName('second_section');


            for (var i = 0; i < secondSection.length; i++) {
                if (twinsCheckbox.checked) {
                    secondSection[i].style.display = 'block';
                } else {
                    secondSection[i].style.display = 'none';
                }
            }
        }


        document.getElementById('twins').addEventListener('change', toggleSecondSection);


        toggleSecondSection();
    </script>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var textInputs = document.querySelectorAll('input[type="text"]:not([name="father_email"]):not([name="mother_email"]), textarea');
            textInputs.forEach(function(input) {
                input.addEventListener('input', function() {
                    this.value = this.value.toUpperCase();
                });
            });
        });


        $(document).ready(function() {
            $('#inputGroupPrepend45').on('change', function() {
                var dob = $(this).val();
                if (dob) {
                    var age = calculateAge(new Date(dob));
                    $('#age').val(age);
                }
            });

            function calculateAge(dob) {
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }
        });
    </script>

    <script>
        document.getElementById('residential_type').addEventListener('change', function() {
            var monthsSection = document.getElementById('months_section');
            if (this.value === '1') {
                monthsSection.style.display = 'block';
            } else {
                monthsSection.style.display = 'none';
            }
        });
    </script>


    <script>
        function copyAddress() {
            var sameAddressCheckbox = document.getElementsByName('same_address')[0];
            var permanentFields = ['p_house_no', 'p_area', 'p_landmark', 'p_pincode', 'p_city_name'];

            if (sameAddressCheckbox.checked) {

                permanentFields.forEach(function(field) {
                    document.getElementsByName(field)[0].disabled = true;
                });


                var residentialFields = ['house_no', 'area', 'landmark', 'pincode', 'city_name'];
                residentialFields.forEach(function(field) {
                    var residentialValue = document.getElementsByName(field)[0].value;
                    var permanentField = document.getElementsByName('p_' + field)[0];
                    permanentField.value = residentialValue;
                });
            } else {

                permanentFields.forEach(function(field) {
                    document.getElementsByName(field)[0].disabled = false;
                });

                document.getElementsByName('p_house_no')[0].value = '';
                document.getElementsByName('p_area')[0].value = '';
                document.getElementsByName('p_landmark')[0].value = '';
                document.getElementsByName('p_pincode')[0].value = '';
                document.getElementsByName('p_city_name')[0].value = '';
            }
        }

        document.getElementsByName('same_address')[0].addEventListener('change', copyAddress);

        copyAddress();
    </script>
@endsection
