@extends('layouts.master')
<style>
    #msform .action-button1 {
    width: 100px;
    background: #fb7d5b;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
#msform .action-button2 {
    width: 100px;
    background: #189848;;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
    </style>
 @section('content')

 <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">

                <!-- Column starts -->

                <!-- Column ends -->
                <!-- Column starts -->
                <div class="col-xl-12">
                    <div class="card" id="accordion-three">
                        <div class="card-header flex-wrap d-flex justify-content-between px-3">
                            <div>
                                  <h4 class="card-title">Approve Application Pool List</h4>
                            </div>
                            {{-- <div>

                                <!-- Button trigger modal -->
                               <a href="{{ route('distance_master.create') }}"><button type="button" class="btn btn-primary" >
                                 + Add Distance
                                </button></a>
                            </div> --}}
                            </div>
                            <!-- /tab-content -->
                            <div class="tab-content" id="myTabContent-2">
                            <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                            <div class="card-body p-0">
                            <div class="table-responsive">
                            <table id="example3" class="display table" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Application No.</th>
                                        <th>Student Name</th>
                                        <th>Date of Birth</th>
                                        <th>Standerd</th>
                                        <th>Father Contact No.</th>
                                        {{-- <th>Action</th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($list as $key => $value)
                                                    <tr>
                                                        <td><b>{{ $key+1 }}</b></td>
                                                        <td>{{ $value->application_no }}</td>
                                                        <td>{{ $value->firstname }} {{ $value->middlename }} {{ $value->lastname }}</td>
                                                        <td>{{ $value->dob }}</td>
                                                        <td>{{ $value->standard_name }}</td>
                                                        <td>{{ $value->father_mobile_no }}</td>

                                                        <td>

                                                            <div class="d-flex">
                                                                {{-- <a href='{{ url("/approve_application/{$value->id}") }}'>
                                                                    <button type="button" class="btn btn-warning waves-effect action-button2" style="color: blue !important;
                                                                    padding: 5px;font-size: 11px;">Start Random <br>Selection</button>
                                                                </a> --}}

                                                                {{-- <a href="{{ url("/view_application/{$value->id}") }}" class="btn  shadow btn-xs sharp me-1" style=""><i class="fa fa-eye" style="color: #4d44b5;
                                                                    font-size: 23px;"></i></a> --}}

                                                            </div>
                                                        </td>

                                                    </tr>
                                                    @endforeach
                                          </tbody>
                                       </table>
                                     </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- /tab-content -->

                    </div>
                </div>



        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>

@endsection
