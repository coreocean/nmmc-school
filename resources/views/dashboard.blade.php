
@extends('layouts.master')


<style>
    .page-titles {
    display: flow-root !important;
    align-items: center;
    justify-content: space-between;
    padding: 0.825rem 1.875rem;
    background: #ffffff;
    margin-bottom: 1.875rem;
    border-radius: 0.375rem;
    position: relative;
    z-index: 1;
    flex-wrap: wrap;
}

.page-titles h4, .page-titles .h4 {
    margin-bottom: 0;
    margin-top: 0;
    color: #171343 !important;
    font-size: 15px !important;
}
</style>
@section('content')

@if(Auth::user()->usertype != 1)
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="d-flex align-items-center">
                            <li class="icon-box icon-box-lg bg-primary me-3">
                                <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.9288 37.75H3.75C1.67875 37.75 0 36.0713 0 34V23.5863C0 21.7738 1.29625 20.2213 3.07875 19.8975C5.72125 19.4163 10.2775 18.5875 12.855 18.12C14.2737 17.8612 15.7263 17.8612 17.145 18.12C19.7225 18.5875 24.2788 19.4163 26.9213 19.8975C28.7038 20.2213 30 21.7738 30 23.5863C30 26.3125 30 31.0825 30 34C30 36.0713 28.3212 37.75 26.25 37.75H12.9288ZM24.785 22.05L24.79 22.0563C25.0088 22.3838 25.06 22.795 24.9287 23.1662L24.0462 25.6662C23.9312 25.9925 23.685 26.2575 23.3675 26.3963L21.7075 27.12L22.3675 28.4412C22.5525 28.81 22.5425 29.2462 22.3425 29.6075L19.2075 35.25H26.25C26.94 35.25 27.5 34.69 27.5 34C27.5 31.0825 27.5 26.3125 27.5 23.5863C27.5 22.9825 27.0675 22.465 26.4738 22.3562L24.785 22.05ZM21.3663 21.4275L16.6975 20.5788C15.575 20.375 14.425 20.375 13.3025 20.5788L8.63375 21.4275L7.63625 22.9238L8.13 24.3213L10.5 25.3537C10.8138 25.4912 11.0575 25.7512 11.175 26.0737C11.2925 26.3962 11.2712 26.7525 11.1175 27.0588L10.1625 28.9688L13.6525 35.25H16.3475L19.8375 28.9688L18.8825 27.0588C18.7288 26.7525 18.7075 26.3962 18.825 26.0737C18.9425 25.7512 19.1862 25.4912 19.5 25.3537L21.87 24.3213L22.3638 22.9238L21.3663 21.4275ZM5.215 22.05L3.52625 22.3562C2.9325 22.465 2.5 22.9825 2.5 23.5863V34C2.5 34.69 3.06 35.25 3.75 35.25H10.7925L7.6575 29.6075C7.4575 29.2462 7.4475 28.81 7.6325 28.4412L8.2925 27.12L6.6325 26.3963C6.315 26.2575 6.06875 25.9925 5.95375 25.6662L5.07125 23.1662C4.94 22.795 4.99125 22.3838 5.21 22.0563L5.215 22.05ZM23.75 29V31.5C23.75 32.19 24.31 32.75 25 32.75C25.69 32.75 26.25 32.19 26.25 31.5V29C26.25 28.31 25.69 27.75 25 27.75C24.31 27.75 23.75 28.31 23.75 29ZM15 0.25C10.5163 0.25 6.875 3.89125 6.875 8.375C6.875 12.8587 10.5163 16.5 15 16.5C19.4837 16.5 23.125 12.8587 23.125 8.375C23.125 3.89125 19.4837 0.25 15 0.25ZM15 2.75C18.105 2.75 20.625 5.27 20.625 8.375C20.625 11.48 18.105 14 15 14C11.895 14 9.375 11.48 9.375 8.375C9.375 5.27 11.895 2.75 15 2.75Z" fill="white"/>
                                    </svg>
                            </li>
                            <li>
                                <span>New Appication</span>
                                <h3 class="my-1">{{ $newApp_count }}</h3>
                                {{-- <span>+10% than last month</span> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="d-flex align-items-center">
                            <li class="icon-box bg-success icon-box-lg me-3">
                                <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 34C0 36.0713 1.67875 37.75 3.75 37.75H26.25C28.3212 37.75 30 36.0713 30 34C30 31.0825 30 26.3125 30 23.5863C30 21.7738 28.7038 20.2213 26.9213 19.8975C24.2788 19.4163 19.7225 18.5875 17.145 18.12C15.7263 17.8612 14.2737 17.8612 12.855 18.12C10.2775 18.5875 5.72125 19.4163 3.07875 19.8975C1.29625 20.2213 0 21.7738 0 23.5863V34ZM17.885 20.795L19.7612 27.9288C20.0075 28.865 19.6775 29.8588 18.92 30.4638C18.28 30.9738 17.2713 31.7788 16.5713 32.3388C15.6525 33.0713 14.3475 33.0713 13.4287 32.3388C12.7287 31.7788 11.72 30.9738 11.08 30.4638C10.3225 29.8588 9.9925 28.865 10.2388 27.9288L12.115 20.795L3.52625 22.3562C2.9325 22.465 2.5 22.9825 2.5 23.5863V34C2.5 34.69 3.06 35.25 3.75 35.25C8.98 35.25 21.02 35.25 26.25 35.25C26.94 35.25 27.5 34.69 27.5 34C27.5 31.0825 27.5 26.3125 27.5 23.5863C27.5 22.9825 27.0675 22.465 26.4738 22.3562L17.885 20.795ZM15.2038 20.4288C15.0675 20.425 14.9325 20.425 14.7962 20.4288L12.6663 28.5312L14.9887 30.3837C14.995 30.39 15.005 30.39 15.0113 30.3837L17.3337 28.5312L15.2038 20.4288ZM15 0.25C10.5163 0.25 6.875 3.89125 6.875 8.375C6.875 12.8587 10.5163 16.5 15 16.5C19.4837 16.5 23.125 12.8587 23.125 8.375C23.125 3.89125 19.4837 0.25 15 0.25ZM15 2.75C18.105 2.75 20.625 5.27 20.625 8.375C20.625 11.48 18.105 14 15 14C11.895 14 9.375 11.48 9.375 8.375C9.375 5.27 11.895 2.75 15 2.75Z" fill="white"/>
                                </svg>
                            </li>
                            <li>
                                <span>Approve Application</span>
                                <h3 class="my-1">{{ $Approve_count }}</h3>
                                {{-- <span>+10% than last month</span> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="d-flex align-items-center">
                            <li class="icon-box bg-danger icon-box-lg me-3">
                                <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 34C0 36.0713 1.67875 37.75 3.75 37.75H26.25C28.3212 37.75 30 36.0713 30 34C30 31.0825 30 26.3125 30 23.5863C30 21.7738 28.7038 20.2213 26.9213 19.8975C24.2788 19.4163 19.7225 18.5875 17.145 18.12C15.7263 17.8612 14.2737 17.8612 12.855 18.12C10.2775 18.5875 5.72125 19.4163 3.07875 19.8975C1.29625 20.2213 0 21.7738 0 23.5863V34ZM17.885 20.795L19.7612 27.9288C20.0075 28.865 19.6775 29.8588 18.92 30.4638C18.28 30.9738 17.2713 31.7788 16.5713 32.3388C15.6525 33.0713 14.3475 33.0713 13.4287 32.3388C12.7287 31.7788 11.72 30.9738 11.08 30.4638C10.3225 29.8588 9.9925 28.865 10.2388 27.9288L12.115 20.795L3.52625 22.3562C2.9325 22.465 2.5 22.9825 2.5 23.5863V34C2.5 34.69 3.06 35.25 3.75 35.25C8.98 35.25 21.02 35.25 26.25 35.25C26.94 35.25 27.5 34.69 27.5 34C27.5 31.0825 27.5 26.3125 27.5 23.5863C27.5 22.9825 27.0675 22.465 26.4738 22.3562L17.885 20.795ZM15.2038 20.4288C15.0675 20.425 14.9325 20.425 14.7962 20.4288L12.6663 28.5312L14.9887 30.3837C14.995 30.39 15.005 30.39 15.0113 30.3837L17.3337 28.5312L15.2038 20.4288ZM15 0.25C10.5163 0.25 6.875 3.89125 6.875 8.375C6.875 12.8587 10.5163 16.5 15 16.5C19.4837 16.5 23.125 12.8587 23.125 8.375C23.125 3.89125 19.4837 0.25 15 0.25ZM15 2.75C18.105 2.75 20.625 5.27 20.625 8.375C20.625 11.48 18.105 14 15 14C11.895 14 9.375 11.48 9.375 8.375C9.375 5.27 11.895 2.75 15 2.75Z" fill="white"/>
                                </svg>
                            </li>
                            <li>
                                <span>Reject Application</span>
                                <h3 class="my-1">{{ $reject_count }}</h3>
                                {{-- <span>+10% than last month</span> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="d-flex align-items-center">
                            <li class="icon-box icon-box-lg bg-secondary me-3">
                                <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 34C0 36.0713 1.67875 37.75 3.75 37.75H26.25C28.3212 37.75 30 36.0713 30 34C30 31.0825 30 26.3125 30 23.5863C30 21.7738 28.7038 20.2213 26.9213 19.8975C24.2788 19.4163 19.7225 18.5875 17.145 18.12C15.7263 17.8612 14.2737 17.8612 12.855 18.12C10.2775 18.5875 5.72125 19.4163 3.07875 19.8975C1.29625 20.2213 0 21.7738 0 23.5863V34ZM17.885 20.795L19.7612 27.9288C20.0075 28.865 19.6775 29.8588 18.92 30.4638C18.28 30.9738 17.2713 31.7788 16.5713 32.3388C15.6525 33.0713 14.3475 33.0713 13.4287 32.3388C12.7287 31.7788 11.72 30.9738 11.08 30.4638C10.3225 29.8588 9.9925 28.865 10.2388 27.9288L12.115 20.795L3.52625 22.3562C2.9325 22.465 2.5 22.9825 2.5 23.5863V34C2.5 34.69 3.06 35.25 3.75 35.25C8.98 35.25 21.02 35.25 26.25 35.25C26.94 35.25 27.5 34.69 27.5 34C27.5 31.0825 27.5 26.3125 27.5 23.5863C27.5 22.9825 27.0675 22.465 26.4738 22.3562L17.885 20.795ZM15.2038 20.4288C15.0675 20.425 14.9325 20.425 14.7962 20.4288L12.6663 28.5312L14.9887 30.3837C14.995 30.39 15.005 30.39 15.0113 30.3837L17.3337 28.5312L15.2038 20.4288ZM15 0.25C10.5163 0.25 6.875 3.89125 6.875 8.375C6.875 12.8587 10.5163 16.5 15 16.5C19.4837 16.5 23.125 12.8587 23.125 8.375C23.125 3.89125 19.4837 0.25 15 0.25ZM15 2.75C18.105 2.75 20.625 5.27 20.625 8.375C20.625 11.48 18.105 14 15 14C11.895 14 9.375 11.48 9.375 8.375C9.375 5.27 11.895 2.75 15 2.75Z" fill="white"/>
                                </svg>
                            </li>
                            <li>
                                <span>Pending Scrutiny</span>
                                <h3 class="my-1">0</h3>
                                {{-- <span>+10% than last month</span> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <ul class="d-flex align-items-center">
                            <li class="icon-box icon-box-lg bg-warning me-3">
                                <svg width="30" height="38" viewBox="0 0 30 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 34C0 36.0713 1.67875 37.75 3.75 37.75H26.25C28.3212 37.75 30 36.0713 30 34C30 31.0825 30 26.3125 30 23.5863C30 21.7738 28.7038 20.2213 26.9213 19.8975C24.2788 19.4163 19.7225 18.5875 17.145 18.12C15.7263 17.8612 14.2737 17.8612 12.855 18.12C10.2775 18.5875 5.72125 19.4163 3.07875 19.8975C1.29625 20.2213 0 21.7738 0 23.5863V34ZM17.885 20.795L19.7612 27.9288C20.0075 28.865 19.6775 29.8588 18.92 30.4638C18.28 30.9738 17.2713 31.7788 16.5713 32.3388C15.6525 33.0713 14.3475 33.0713 13.4287 32.3388C12.7287 31.7788 11.72 30.9738 11.08 30.4638C10.3225 29.8588 9.9925 28.865 10.2388 27.9288L12.115 20.795L3.52625 22.3562C2.9325 22.465 2.5 22.9825 2.5 23.5863V34C2.5 34.69 3.06 35.25 3.75 35.25C8.98 35.25 21.02 35.25 26.25 35.25C26.94 35.25 27.5 34.69 27.5 34C27.5 31.0825 27.5 26.3125 27.5 23.5863C27.5 22.9825 27.0675 22.465 26.4738 22.3562L17.885 20.795ZM15.2038 20.4288C15.0675 20.425 14.9325 20.425 14.7962 20.4288L12.6663 28.5312L14.9887 30.3837C14.995 30.39 15.005 30.39 15.0113 30.3837L17.3337 28.5312L15.2038 20.4288ZM15 0.25C10.5163 0.25 6.875 3.89125 6.875 8.375C6.875 12.8587 10.5163 16.5 15 16.5C19.4837 16.5 23.125 12.8587 23.125 8.375C23.125 3.89125 19.4837 0.25 15 0.25ZM15 2.75C18.105 2.75 20.625 5.27 20.625 8.375C20.625 11.48 18.105 14 15 14C11.895 14 9.375 11.48 9.375 8.375C9.375 5.27 11.895 2.75 15 2.75Z" fill="white"/>
                                </svg>
                            </li>
                            <li>
                                <span>Complete Scrutiny</span>
                                <h3 class="my-1">0</h3>
                                {{-- <span>+10% than last month</span> --}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            {{-- Application list start --}}

            <div class="col-xl-12">
                <div class="card" id="accordion-three">
                    <div class="card-header flex-wrap d-flex justify-content-between px-3">
                        <div>
                        <h4 class="card-title">Application Report</h4>

                        </div>
                        <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                            <li class="nav-item" role="presentation">
                            <!-- Button trigger modal -->

                            </li>

                            </ul>
                            </div>

                            <!-- /tab-content -->
                            <div class="tab-content" id="myTabContent-2">
                            <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                            <div class="card-body p-0">


                        <div class="table-responsive">
                            <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">Standerd</th>
                                        <th scope="col">Application Count</th>
                                        <th scope="col">Pending</th>
                                        <th scope="col">Approved</th>
                                        <th scope="col">Rejected</th>
                                        <th scope="col">Selected</th>
                                        <th scope="col">Waiting List</th>
                                        <th scope="col">Canceled</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($standards as $standard)
                                        <tr>
                                            <td>{{ $standard->standard_name }}</td>
                                            <td>{{ $AllApp_counts[$standard->id]['count'] ?? 0 }}</td>
                                            <td>{{ $newApp_counts[$standard->id]['count'] ?? 0 }}</td>
                                            <td>{{ $Approve_counts[$standard->id]['count'] ?? 0 }}</td>
                                            <td>{{ $reject_counts[$standard->id]['count'] ?? 0 }}</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>



                                </div>
                            </div>

                            </div>
                        </div>
                        <!-- /tab-content -->

                </div>
            </div>


{{-- reservation list start --}}
            <div class="col-xl-12">
                <div class="card" id="accordion-three">
                    <div class="card-header flex-wrap d-flex justify-content-between px-3">
                        <div>
                        <h4 class="card-title">Reservation Report</h4>

                        </div>
                        <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                            <li class="nav-item" role="presentation">
                            <!-- Button trigger modal -->

                            </li>

                            </ul>
                            </div>

                            <!-- /tab-content -->
                            <div class="tab-content" id="myTabContent-2">
                            <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                            <div class="card-body p-0">


                        <div class="table-responsive">
                            <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th scope="col">School Name</th>
                                        <th scope="col">Standerd Name</th>
                                        <th scope="col">0-1</th>
                                        <th scope="col">1-3</th>
                                        <th scope="col">3 and Above</th>
                                        <th scope="col">commissioner quota</th>
                                        <th scope="col">mayor</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>



                                </div>
                            </div>

                            </div>
                        </div>
                        <!-- /tab-content -->

                </div>
            </div>


        </div>
    </div>
</div>
@else



<div class="content-body">
    <div class="container-fluid">
        <?php foreach($result as $value){ ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="page-titles">
                    <div class="d-flex align-items-center">

                        <div class="col-xl-4 col-sm-6" style="text-align: center;">
                           <h4 class="heading">{{$value->firstname}} {{$value->middlename}} {{$value->lastname}} ( {{$value->standard_name}})</h4>
                        </div>
                        <div class="col-xl-4 col-sm-6" style="text-align: center;">
                            <h4 class="heading">Form No :  {{ $value->application_no }}</h4>
                         </div>
                         <div class="col-xl-4 col-sm-6" style="text-align: center;">
                            <h4 class="heading">Academic Year :  {{ $value->year }}</h4>
                         </div>


                        <!-- <p class="text-warning ms-2">Welcome Back Yatin Sharma !</p> -->
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6">
                <div class="card bg-primary plan-bx h-auto">
                    <div class="card-body">
                        <ul class="mb-3" style="text-align: center;">
                            <li class="plan-svg">
                                <svg width="85" height="228" viewBox="0 0 85 228" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="100" height="294" rx="50" fill="#FCC43E"></rect>
                                </svg>
                            </li>
                            <li class="plan-svg-1">
                                <svg width="100" height="180" viewBox="0 0 100 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="100" height="294" rx="50" fill="#FB7D5B"></rect>
                                    </svg>

                            </li>
                            <h3 class="text-white">Addmission Form</h3>

                        </ul>
                        <ul class=" text-white">
                            <li style='margin-bottom: 88px;'>
                                {{-- <span class="text-white">form submited Pending for Review</span> --}}
                            </li>

                        </ul>

                        <a href="/view_generate_pdf/{{ $value->id }}"><button class="btn btn-light btn-sm">Download</button></a>
                        <a href="/view_documents/{{ $value->id }}"> <button class="btn btn-light btn-sm">Documents</button></a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-6">
                <div class="card bg-primary plan-bx h-auto">
                    <div class="card-body">
                        <ul class="mb-4" style="text-align: center;">
                            <li class="plan-svg">
                                <svg width="85" height="228" viewBox="0 0 85 228" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="100" height="294" rx="50" fill="#FCC43E"></rect>
                                </svg>
                            </li>
                            <li class="plan-svg-1">
                                <svg width="100" height="180" viewBox="0 0 100 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="100" height="294" rx="50" fill="#FB7D5B"></rect>
                                    </svg>

                            </li>
                            <li class="text-white"><h3 class="text-white">Status</h3></li>

                        </ul>
                        <ul class="food-recipe text-white">
                            <li style='margin-bottom: 79px;'>

                                <?php if($value->c_status == 0){

                                    $text = 'Form Submitted Pending for Review';
                                }elseif($value->c_status == 1){

                                    $text = 'Your Admission Form is Scrutiny Process';

                                }
                                    ?>
                                <span class="text-white">{{ $text }}</span>

                            </li>

                        </ul>

                        {{-- <button class="btn btn-light btn-sm">Upgrade Plan</button> --}}
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-6">
                <div class="card bg-primary plan-bx h-auto">
                    <div class="card-body">
                        <ul class="d-flex align-items-baseline justify-content-between mb-3">
                            <li class="plan-svg">
                                <svg width="85" height="228" viewBox="0 0 85 228" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="100" height="294" rx="50" fill="#FCC43E"></rect>
                                </svg>
                            </li>
                            <li class="plan-svg-1">
                                <svg width="100" height="180" viewBox="0 0 100 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect width="100" height="294" rx="50" fill="#FB7D5B"></rect>
                                    </svg>

                            </li>
                            <li class="text-white"><h3 class="text-white"></h3></li>

                        </ul>
                        <ul class=" text-white">
                            <li style='margin-bottom: 112px;'>
                                {{-- <span class="text-white">form submited Pending for Review</span> --}}
                            </li>

                        </ul>

                        <button class="btn btn-light btn-sm" disabled>Cancled Application</button>

                    </div>
                </div>
            </div>





        </div>
        <?php } ?>
    </div>
</div>





@endif

 @endsection
