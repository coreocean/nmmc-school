<!DOCTYPE html>
<html lang="en" class="h-100">
    <style>


        .bgimage{
            background-image: url('http://127.0.0.1:8000/assets/images/background/1366x600.jpg');
    width: 100% !important;
    height: 100% !important;
    background-size: cover;
    background-position: center;
        }

        button.btn.btn-block.btn-primary.mainbutton {

            margin-top: 31px;
            width: 245px;
            padding: 12px;
            font-size: 15px;
            margin-left: 264px;
            background-color: #124d9b;
            border-radius: 14px;
        }
    </style>
<head>
   <!-- All Meta -->
	<meta charset="utf-8">
	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Page Title Here -->
	<title>NMMC-School</title>

<!-- FAVICONS ICON -->
	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" >
	<link href="{{asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/toastr/toastr.min.css')}}">

</head>

<body class="body" >

    <div class="bgimage">
        <div class='row'>
            <div class='col-md-6'>
            </div>
            <div class='col-md-6'>
                <a href="/login"><button class="btn btn-block btn-primary mainbutton" style='margin-top: 80px;'>Login</button></a>
                <a href="/register"><button class="btn btn-block btn-primary mainbutton">Registration</button></a>
                <button class="btn btn-block btn-primary mainbutton" data-bs-toggle="modal" data-bs-target="#admissionprocess">Online Admission Process</button>
                <button class="btn btn-block btn-primary mainbutton" data-bs-toggle="modal" data-bs-target="#admissioncriteria">Admission Criteria</button>
                <button class="btn btn-block btn-primary mainbutton" data-bs-toggle="modal" data-bs-target="#admissioneligbility">Admission Eligibility</button>
            </div>
        </div>

    </div>


    {{-- online admission process --}}

    <div class="modal fade" id="admissionprocess">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Online Admission Process</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal">
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Close</button>
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                </div>
            </div>
        </div>
    </div>


    {{-- online admission criteria --}}

    <div class="modal fade" id="admissioncriteria">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Admission Criteria</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal">
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Close</button>
                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                </div>
            </div>
        </div>
    </div>


   {{-- online admission eligbility --}}

   <div class="modal fade" id="admissioneligbility">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Admission Eligibility</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('assets/vendor/global/global.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>
    <script src="{{asset('assets/js/dlabnav-init.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('assets/toastr/toastr.min.js')}}"></script>
<!-- end -->

<script type="text/javascript">

@if(Session::has('message'))

   var type = "{{ Session::get('alert-type', 'info') }}";

   switch(type){

      case 'info':

         toastr.info("{{ Session::get('message') }}");

         break;



      case 'warning':

         toastr.warning("{{ Session::get('message') }}");

         break;



      case 'success':

         toastr.success("{{ Session::get('message') }}");

         break;



      case 'error':

         toastr.error("{{ Session::get('message') }}");

         break;

   }

@endif


</script>

</body>

</html>
