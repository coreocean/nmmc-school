<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">


	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Page Title Here -->
	<title>NMMC-School</title>

<!-- FAVICONS ICON -->
	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" >
	<link href="{{asset('assets/vendor/wow-master/css/libs/animate.css')}}" rel="stylesheet">
	<link href="{{asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-select-country/css/bootstrap-select-country.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendor/jquery-nice-select/css/nice-select.css')}}">
	<link href="{{asset('assets/vendor/datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

	 <link href="{{asset('assets/vendor/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
	<!--swiper-slider-->
	<link rel="stylesheet" href="{{asset('assets/vendor/swiper/css/swiper-bundle.min.css')}}">
	<!-- Style css -->
	<link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/wizard.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/toastr/toastr.min.css')}}">
	<style>
	.form-label{
		font-weight:bold;
		color:black;
	}
</style>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
	<!-- <div id="preloader">
	  <div class="loader">
		<div class="dots">
			  <div class="dot mainDot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
		</div>

		</div>
	</div> -->
    <!--*******************
        Preloader end
    ********************-->
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <!-- <div id="main-wrapper" class="wallet-open active"> -->

        <!--**********************************
            Nav header start
        ***********************************-->
      @include('includes.header')
        <!--**********************************
            Nav header end
        ***********************************-->

		<!--**********************************
            Header start
        ***********************************-->

			<!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
      @include('includes.sidebar')
        <!--**********************************
            Sidebar end
        ***********************************-->

		<!--****
		Wallet Sidebar
		****-->

		<!-- <div class="wallet-bar-close"></div> -->
		<!--**********************************
            Content body start
        ***********************************-->
        <!--**********************************
            Content body start
        ***********************************-->
		 @yield('content')

        <!--**********************************
            Content body end
        ***********************************-->
		<!--**********************************
			Footer start
		***********************************-->
		@include('includes.footer')

	<!-- </div> -->

    @yield('script')

</body>

</html>
