@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Add School </h5>
						</div>

						<div class="card-body">
                            <form action="{{ route('school.update', $data->id) }}" method="post" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf
                                @if (!empty($data->id) || 1 == 1)
                                <input type="hidden" name="_method" value="PATCH">
                            @endif

                            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">
							<div class="row">
								<div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">School Name <span class="required">*</span></label>
									  <input type="text" class="form-control"  name="school_name" id="school_name" value="{{$data->school_name}}" placeholder="Enter School Name " required>
                                      <span class="text-danger">@error('school_name'){{$message}}@enderror</span>
									</div>
								</div>
                                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
                                        <label class="form-label text-primary">Academy Year<span class="required">*</span></label>
                                              <select class=" default-select form-control wide" name="year"  aria-label="Default select example" >
                                                  <option value="" selected="selected">Select Year</option>
                                                   @foreach($ayear as  $year)
                                                      <option value="{{$year->id}}" {{$data->year == $year->id ? 'selected':''}}>{{$year->year}}</option>
                                                   @endforeach
                                              </select>
                                              <span class="text-danger">@error('year'){{$message}}@enderror</span>
									 </div>
								</div>

                                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Latitude<span class="required">*</span></label>
									  <input type="text" class="form-control"  name="latitude" id="latitude" value="{{$data->latitude}}" placeholder="Enter latitude " required>
                                       <span class="text-danger">@error('latitude'){{$message}}@enderror</span>
									</div>
								</div>
                                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Longitude <span class="required">*</span></label>
									  <input type="text" class="form-control"  name="longitude" id="longitude" value="{{$data->longitude}}" placeholder="Enter longitude " required>
                                       <span class="text-danger">@error('longitude'){{$message}}@enderror</span>
									</div>
								</div>
                                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Admission Start Date <span class="required">*</span></label>
									  <input type="date" class="form-control"  name="start_date" id="start_date" value="{{$data->start_date}}" placeholder="Enter Start Date " required>
                                       <span class="text-danger">@error('start_date'){{$message}}@enderror</span>
									</div>
								</div>
                                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Admission End Date <span class="required">*</span></label>
									  <input type="date" class="form-control"  name="end_date" id="end_date" value="{{$data->end_date}}" placeholder="Enter End Date " required>
                                       <span class="text-danger">@error('end_date'){{$message}}@enderror</span>
									</div>
								</div>

                               <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">School Address <span class="required">*</span></label>
									  <textarea  class="form-control" id="school_address" name="school_address"value="{{$data->school_address}}" >{{$data->school_address}}</textarea><br>

                                      {{-- <input type="text" class="form-control"  name="school_address" id="school_address" value="{{$data->school_address}}" placeholder="Enter School Address" required> --}}
                                       <span class="text-danger">@error('school_address'){{$message}}@enderror</span>
									</div>
								</div>

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
