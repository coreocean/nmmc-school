@extends('layouts.master')
<style>
    #msform .action-button1 {
    width: 100px;
    background: #fb7d5b;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
#msform .action-button2 {
    width: 100px;
    background: #189848;;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
}
    </style>
 @section('content')

 <div class="content-body">
            <!-- row -->
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">

                <!-- Column starts -->

                <!-- Column ends -->
                <!-- Column starts -->
                <div class="col-xl-12">
                    <div class="card" id="accordion-three">
                        <div class="card-header flex-wrap d-flex justify-content-between px-3">
                            <div>
                            <h4 class="card-title">Financial Year List</h4>

                            </div>
                            <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a href="{{ route('financial_year.create') }}" ><button class="nav-link active " id="home-tab-2" data-bs-toggle="tab" data-bs-target="#withoutSpace" type="button" role="tab" aria-selected="true">Add</button></a>
                                </li>

                            </ul>
                        </div>

                            <!-- /tab-content -->
                            <div class="tab-content" id="myTabContent-2">
                                <div class="tab-pane fade show active" id="withoutSpace" role="tabpanel" aria-labelledby="home-tab-2">
                                     <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <table id="example3" class="display table" style="min-width: 845px">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Financial Year</th>
                                                        <th>From Date</th>
                                                        <th>To Date</th>
                                                        <th>Is Active</th>
                                                        <th>Active</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                               @foreach($data As $row)
                                                    <tr>
                                                        <td><b>{{$loop->iteration}}</b></td>
                                                        <td><b>{{$row->year}}</b></td>
                                                        <td><b>{{date('d-m-Y', strtotime($row->start_date))}}</b></td>
                                                        <td><b>{{date('d-m-Y', strtotime($row->end_date))}}</b></td>
                                                        <td><b>@if($row->is_active == 1)Active @else Inactive @endif</b></td>
                                                        <td>
                                                            <a href="" class="nav-link active"><button type="button" class="btn btn-warning waves-effect m-r-10 action-button2" data-bs-toggle="modal" data-bs-target="#basicModal">Approve</button></a>
                                                        </td>

                                                        <td>
                                                            <div class="d-flex">
                                                             <a href="{{route('financial_year.edit', $row->id)}}" class="btn btn-primary shadow btn-xs sharp me-1"><i class="fas fa-pencil-alt"></i></a>
                                                                <form action="{{ route('financial_year.destroy', $row->id) }}" method="post">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                  <button class="btn btn-danger shadow btn-xs sharp" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i>
                                                                  </button>
                                                               </form>
                                                            </div>
                                                        </td>
                                                    </tr>

                                             @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                </div>
                            </div>
                            <!-- /tab-content -->

                    </div>
                </div>



        </div>
    </div>
</div>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>

