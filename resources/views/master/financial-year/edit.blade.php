@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-8">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Add Financial Year </h5>
						</div>

						<div class="card-body">
                            <form method="post" action="{{ route('financial_year.update', $data->id) }}" class="new-added-form needs-validation" enctype="multipart/form-data">
                                @csrf
                                @csrf
                                @if (!empty($data->id) || 1 == 1)
                                <input type="hidden" name="_method" value="PATCH">
                            @endif
                            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}">
							<div class="row">
								<div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">Financial Year <span class="required">*</span></label>
									  <input type="text" class="form-control"  name="year" value="{{$data->year}}" id="	year" placeholder="Enter Financial Year " required>
									</div>

								</div>

                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">From Date <span class="required">*</span></label>
									  <input type="date" class="form-control"  name="start_date" id="start_date" value="{{$data->start_date}}" required>
									</div>

								</div>

                <div class="col-xl-6 col-sm-6">
									<div class="mb-3">
									  <label for="exampleFormControlInput9" class="form-label text-primary">To Date <span class="required">*</span></label>
									  <input type="date" class="form-control"  name="end_date" id="end_date" value="{{$data->end_date}}" required>
									</div>

								</div>
                                {{-- <div class="col-xl-6 col-sm-6">
                                    <div class="mb-3">
                                        <label class="form-check-label" for="isActive">Is Active</label>
                                        <input class="form-check-input" type="checkbox"  @if($data->is_active == 1) checked @endif name="is_active" id="isActive">
                                    </div>
                                </div> --}}

							</div>
							<div class="float-end">
								<button class="btn btn-outline-primary me-3">Cancle</button>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
                        </form>
						</div>

					</div>
				</div>
			</div>
		</div>
        <script>
            (function () {
              'use strict'

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.querySelectorAll('.needs-validation')

              // Loop over them and prevent submission
              Array.prototype.slice.call(forms)
                .forEach(function (form) {
                  form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                      event.preventDefault()
                      event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                  }, false)
                })
            })()
        </script>
        <!--**********************************
            Content body end
        ***********************************-->

@endsection
