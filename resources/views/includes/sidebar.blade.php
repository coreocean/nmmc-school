<style>
    [data-sidebar-style="full"][data-layout="vertical"] .dlabnav .metismenu>li {
        padding: unset;
    }
</style>
<div class="dlabnav">
    <div class="dlabnav-scroll">
        <ul class="metismenu" id="menu">

            @if (Auth::user()->usertype == 3)
                <li><a href="{{ url('admin/index') }}" aria-expanded="false">
                        <i class="material-symbols-outlined">home</i>
                        <span class="nav-text">Dashboard</span>
                    </a>

                </li>

					<li><a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
						<i class="material-symbols-outlined">school</i>
						<span class="nav-text">Master</span>
					</a>
					<ul aria-expanded="false">
						<li><a href="{{ url('/standard_master') }}">Standard</a></li>
						<li><a href="{{ url('/cast_master') }}">Cast Category</a></li>
						<li><a href="{{ url('/cast_master') }}">Cast Category</a></li>
                        <li><a href="{{ url('/religion_master') }}">Religion</a></li>
						<li><a href="{{ url('/notification_master') }}">Notification</a></li>
                        <li><a href="{{ url('/documents_master') }}">Documents</a></li>
                        <li><a href="{{ url('/school') }}">School</a></li>
                        <li><a href="{{ url('/financial_year') }}">Financial Year</a></li>
                        <li><a href="{{ url('/terms_condition') }}">Terms & Condition</a></li>
                        <li><a href="{{ url('/seat_master') }}">Seat</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/admin_register') }}" aria-expanded="false">
                        <i class="material-symbols-outlined">person</i>
                        <span class="nav-text">Registration</span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">Application List</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ url('/status_application_list', 0) }}">Pending</a></li>
                        <li><a href="{{ url('/status_application_list', 1) }}">Approve</a></li>
                        <li><a href="{{ url('/status_application_list', 2) }}">Reject</a></li>
                    </ul>
                </li>



                <li><a href="{{ url('/app_pool_list') }}" aria-expanded="false">
                        <i class="material-icons"> favorite </i>
                        <span class="nav-text">Application Pool List</span>
                    </a>
                </li>


                    <li><a href="#" aria-expanded="false">
						<i class="material-icons">article</i>
						<span class="nav-text">Selected Application</span>
					</a>
					</li>


                    <li><a href="#" aria-expanded="false">
						<i class="material-icons"> widgets </i>
						<span class="nav-text">Waiting List </span>
					</a>
					</li>

                    <li>
                        <a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
                            <i class="material-icons">folder</i>
                            <span class="nav-text">Report</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="{{ url('/application_report') }}">Application </a></li>
                            <li><a href="{{ url('/reservation_report') }}">Reservation</a></li>
                            <li><a href="{{ url('/admin/index') }}">Cast Reservation</a></li>
                        </ul>
                    </li>

					@elseif(Auth::user()->usertype == 1)

                    <li><a  href="{{url('admin/index')}}" aria-expanded="false">
                        <i class="material-symbols-outlined">home</i>
                        <span class="nav-text">Dashboard</span>
                    </a>

                </li>


                <li><a href="{{ url('school_list') }}" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">User Application Form</span>
                    </a>
                </li> --}}

                {{-- <li><a href="{{ route('student_application.index') }}" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">User Application List</span>
                    </a>
                </li> --}}
            @elseif(Auth::user()->usertype == 2)
                <li><a href="{{ url('admin/index') }}" aria-expanded="false">
                        <i class="material-symbols-outlined">home</i>
                        <span class="nav-text">Dashboard</span>
                    </a>

                </li>
                <li>
                    <a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">Application List</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ url('/status_application_list', 0) }}">Pending</a></li>
                        <li><a href="{{ url('/status_application_list', 1) }}">Approve</a></li>
                        <li><a href="{{ url('/status_application_list', 2) }}">Reject</a></li>
                    </ul>
                </li>
                <li>
                    <a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">Report</span>
                    </a>
                    <ul aria-expanded="false">

                        <li><a href="{{ url('/application_report') }}">Application </a></li>
                        <li><a href="{{ url('/reservation_report') }}">Reservation</a></li>
                        <li><a href="{{ url('/admin/index') }}">Cast Reservation</a></li>
                    </ul>
                </li>
            @elseif(Auth::user()->usertype == 0)
                <li><a href="{{ url('admin/index') }}" aria-expanded="false">
                        <i class="material-symbols-outlined">home</i>
                        <span class="nav-text">Dashboard</span>
                    </a>

                </li>
                <li>
                    <a class="has-arrow " href="javascript:void(0);" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">Application Status</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ url('/status_application_list', 1) }}">Approve</a></li>
                        <li><a href="{{ url('/status_application_list', 2) }}">Reject</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('/app_pool_list') }}" aria-expanded="false">
                    <i class="material-icons"> app_registration </i>
                        <span class="nav-text">Application Pull List</span>
                    </a>
                </li>
                <li><a href="{{ url('/pool_process') }}" aria-expanded="false">
                    <i class="material-icons"> favorite </i>
                    <span class="nav-text">Pull Process</span>
                </a>
               </li>

                <li><a href="#" aria-expanded="false">
                        <i class="material-icons">article</i>
                        <span class="nav-text">Selected Application</span>
                    </a>
                </li>

                <li><a href="#" aria-expanded="false">
                        <i class="material-icons"> widgets </i>
                        <span class="nav-text">Waiting List </span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false">
                        <i class="material-icons">folder</i>
                        <span class="nav-text">Report</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ url('/application_report') }}">Application </a></li>
                        <li><a href="{{ url('/reservation_report') }}">Reservation</a></li>
                        <li><a href="{{ url('admin/index') }}">Cast Reservation</a></li>
                    </ul>
                </li>
            @endif

            <!-- <div class="copyright">
     <p><strong>School Admission Dashboard</strong></p>
     <p class="fs-12">Made with <span class="heart"></span> by DexignLab</p>
    </div>  -->
    </div>
</div>
