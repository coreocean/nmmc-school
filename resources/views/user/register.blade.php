<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
	<!-- All Meta -->
	<meta charset="utf-8">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Page Title Here -->
	<title>NMMC-School</title>

<!-- FAVICONS ICON -->
	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" >
	<link href="{{asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

</head>

<body class="body  h-100">
	<div class="authincation d-flex flex-column flex-lg-row flex-column-fluid">
		<div class="login-aside text-center  d-flex flex-column flex-row-auto" >
			<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
				<div class="text-center mb-lg-4 mb-2 logo">
					<img src="{{asset('assets/images/logo.png')}}" alt="" style="width: 39%;height: 90%;">
				</div>
				<h1 class="mb-2 text-white">नवी  मुंबई  महानगरपालिका</h1>

			</div>
			{{-- <div class="aside-image position-relative" style="background-image:url({{asset('assets/images/background/pic-2.png')}});"> --}}
            <div class="aside-image position-relative" style="">

                <h3 class="mb-2 text-white" style="padding: 12px;">शिक्षण विभाग </h3>

                <h4 class="mb-2 text-white">CBSE शाळांसाठी प्रवेश प्रक्रिया प्रणाली</h4>

			</div>
		</div>
		<div class="container flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
			<div class="d-flex justify-content-center h-100 align-items-center">
				<div class="authincation-content style-2">
					<div class="row no-gutters">
						<div class="col-xl-12 tab-content">
							<div id="sign-up" class="auth-form tab-pane fade show active  form-validation">
								<form action="#" method="post">
									@csrf
									<div class="text-center mb-4">
										<h3 class="text-center mb-2 " style="font-size: 30px;font-family: cursive;color: #4d44b5;">Register</h3>

									</div>

									@if(session()->has('message'))
										<div id="alert-message"  class="alert alert-success mb-3">
											{{ session()->get('message') }}
										</div>
									@endif

                                    <div class="mb-3">
                                    <label class="text-label form-label">Academic Year<span class="required">*</span></label>
                                    <select class=" default-select form-control wide" name="fy_id"  aria-label="Default select example" >
                                    <option value="" >Select Year</option>
                                    @foreach($financial_y as  $financial_year)
                                    <option value="{{$financial_year->id}}" selected>{{$financial_year->year}}</option>
                                    @endforeach
                                    </select>
                                    <span class="text-danger">@error('fy_id'){{$message}}@enderror</span>
                                    </div>


									<div class="mb-3">
										<label for="exampleFormControlInput1" class="form-label mb-2 fs-13 label-color font-w500">First Name</label>
									  <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="username" name="first_name" value="{{old('first_name')}}">
									  <span class="text-danger">@error('first_name'){{$message}}@enderror</span>
									</div>

									<div class="mb-3">
										<label for="exampleFormControlInput1" class="form-label mb-2 fs-13 label-color font-w500">Middle Name</label>
									  <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="username" name="middle_name" value="{{old('middle_name')}}">

									</div>

									<div class="mb-3">
										<label for="exampleFormControlInput1" class="form-label mb-2 fs-13 label-color font-w500">Last Name</label>
									  <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="username" name="last_name" value="{{old('last_name')}}">

									</div>


                                    <div class="mb-3">
										<label for="exampleFormControlInput2" class="form-label mb-2 fs-13 label-color font-w500">Mobile Number</label>
									  <input type="text" class="form-control" id="exampleFormControlInput2" placeholder="Mobile" name="mobile_no" value="{{old('mobile_no')}}" maxlength="12">
									  <span class="text-danger">@error('mobile_no'){{$message}}@enderror</span>
									</div>

									<div class="mb-3">
										<label for="exampleFormControlInput2" class="form-label mb-2 fs-13 label-color font-w500">Email address</label>
									  <input type="text" class="form-control" id="exampleFormControlInput2" placeholder="Email" name="email" value="{{old('email')}}">
									  <span class="text-danger">@error('email'){{$message}}@enderror</span>
									</div>
									<div class="mb-3">
										<label for="exampleFormControlInput3" class="form-label mb-2 fs-13 label-color font-w500">Password</label>
									  <input type="password" class="form-control" id="exampleFormControlInput3" placeholder="Password" name="password" value="{{old('password')}}">
									  <span class="text-danger">@error('password'){{$message}}@enderror</span>
									</div>
									<!-- <a href="javascript:void(0);" class="text-primary float-end mb-4">Forgot Password ?</a> -->
									<button type="submit" class="btn btn-block btn-primary">Register</button>

								</form>
								<div class="new-account mt-3 text-center">
									<p class="font-w500">Already have an account? <a class="text-primary" href="{{('/')}}">Login</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('assets/vendor/global/global.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>
    <script src="{{asset('assets/js/dlabnav-init.js')}}"></script>
	<script>
        // Use jQuery to wait for the document to be ready
        $(document).ready(function(){
            // Set a timeout function to hide the message after 5000 milliseconds (5 seconds)
            setTimeout(function(){
                $('#alert-message').fadeOut('slow');
            }, 1000); // Adjust the timeout value as needed
        });
    </script>



</body>

</html>
