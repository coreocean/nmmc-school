<!DOCTYPE html>
<html lang="en" class="h-100">
<head>
   <!-- All Meta -->
	<meta charset="utf-8">
	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Page Title Here -->
	<title>NMMC-School</title>

<!-- FAVICONS ICON -->
	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" >
	<link href="{{asset('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('assets/toastr/toastr.min.css')}}">

</head>

<body class="body  h-100">
	<div class="authincation d-flex flex-column flex-lg-row flex-column-fluid">
		<div class="login-aside text-center  d-flex flex-column flex-row-auto" >
			<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
				<div class="text-center mb-lg-4 mb-2 logo">
					<img src="{{asset('assets/images/logo.png')}}" alt="" style="width: 37%;height: 90%;">
				</div>
				<h1 class="mb-2 text-white">नवी  मुंबई  महानगरपालिका</h1>

			</div>
			{{-- <div class="aside-image position-relative" style="background-image:url({{asset('assets/images/background/pic-2.png')}});"> --}}
            <div class="aside-image position-relative" style="">

                <h3 class="mb-2 text-white" style="padding: 12px;">शिक्षण विभाग </h3>

                <h4 class="mb-2 text-white">CBSE शाळांसाठी प्रवेश प्रक्रिया प्रणाली</h4>

			</div>
		</div>
		<div class="container flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
			<div class="d-flex justify-content-center h-100 align-items-center">
				<div class="authincation-content style-2">
					<div class="row no-gutters">
						<div class="col-xl-12 tab-content">
                            <div class="text-center mb-4">
										<h4 class="text-center mb-2" style="font-size: 25px;color: #4d44b5;">Education Department</h4>
                                        <h4 class="text-center mb-2" style="font-size: 25px;color: #4d44b5;">CBSE School</h4>
                                        <h4 class="text-center mb-2" style="font-size: 25px;color: #4d44b5;">Admission </h4>


									</div>
							<div id="sign-up" class="auth-form tab-pane fade show active  form-validation">
								<form action="{{url('dologin')}}" method="post">
									@csrf

									<div class="text-center mb-4">
										<h3 class="text-center mb-2" style="font-size: 35px;font-family: cursive;color: #4d44b5;">Login</h3>
										{{-- <span>Your Social Campaigns</span> --}}
									</div>

									<div class="mb-3">
										<label for="exampleFormControlInput1" class="form-label mb-2 fs-13 label-color font-w500">Email address</label>
									  <input type="email" class="form-control" id="exampleFormControlInput1" name="email" value="{{old('email')}}">
									  <span class="text-danger">@error('email'){{$message}}@enderror</span>
									</div>
									<div class="mb-3">
										<label for="exampleFormControlInput1" class="form-label mb-2 fs-13 label-color font-w500">Password</label>
									  <input type="password" class="form-control" id="exampleFormControlInput2" name="password" value="{{old('password')}}">
									  <span class="text-danger">@error('password'){{$message}}@enderror</span>
									</div>
									<!-- <a href="javascript:void(0);" class="text-primary float-end mb-4">Login with Otp</a> -->
									<button class="btn btn-block btn-primary">Login</button>

								</form>
								<div class="new-account mt-3 text-center">
									<p class="font-w500">Don't have an account?  <a class="text-primary" href="{{url('register')}}" data-toggle="tab">Register here</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('assets/vendor/global/global.min.js')}}"></script>
    <script src="{{asset('assets/js/custom.min.js')}}"></script>
    <script src="{{asset('assets/js/dlabnav-init.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('assets/toastr/toastr.min.js')}}"></script>
<!-- end -->

<script type="text/javascript">

@if(Session::has('message'))

   var type = "{{ Session::get('alert-type', 'info') }}";

   switch(type){

      case 'info':

         toastr.info("{{ Session::get('message') }}");

         break;



      case 'warning':

         toastr.warning("{{ Session::get('message') }}");

         break;



      case 'success':

         toastr.success("{{ Session::get('message') }}");

         break;



      case 'error':

         toastr.error("{{ Session::get('message') }}");

         break;

   }

@endif


</script>

</body>

</html>
