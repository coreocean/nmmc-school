@extends('layouts.master')

@section('content')
    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">

            <!-- /tab-content -->
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="card">
                        <form class="needs-validation" novalidate id="invoice_form">
                            <div class="card-body border-bottom border-bottom-dashed p-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="">
                                            <h4 class="mb-sm-0" style="text-align: center !important;">आटी व शर्ती </h4>
                                        </div>
                                        <div>

                                        </div>
                                    </div>
                                    <!--end col-->

                                </div>
                                <!--end row-->
                            </div>

                            <div class="card-body p-4">
                                <!--end row-->
                                <div class="mt-4">
                                    {{-- <label for="exampleFormControlTextarea1" class="form-label text-muted text-uppercase fw-semibold">NOTES</label> --}}
                                    {!! $terms->terms ?? '' !!}
                                </div>
                                <div class="hstack gap-2 justify-content-end d-print-none mt-4">

                                    <a href="{{ isset($terms) ? url('create/' . $terms->id) : '#' }}" class="btn btn-primary"> Continue</a>
                                    <a href="{{ url('school_list') }}" class="btn btn-secondary"> Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
            <!-- /tab-content -->


        </div>
    </div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
    function confirmation() {
        var result = confirm("Are you sure to delete?");
        if (result) {
            // Delete logic goes here
        }
    }
</script>
