@extends('layouts.master')

@section('content')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">Add Student Application </h5>
                        </div>

                        <div class="card-body">
                            <form id="msform" action="{{ route('student_application.update', $result->id) }}" method="post" enctype="multipart/form-data">
                                @csrf

                                @if (!empty($result->id) || 1 == 1)
                                    <input type="hidden" name="_method" value="PATCH">
                                @endif

                                <input type="hidden" id="id" name="id" value="{{ $result->id or '' }}">


                                <!-- progressbar -->
                               <ul id="progressbar">
                                    <li class="active" id="account"><strong>Student Personal Info</strong></li>
                                    <li id="personal"><strong>Parent's Details</strong></li>
                                    <li id="confirm"><strong>Address</strong></li>
                                    <li id="upload"><strong>Document Upload</strong></li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> <br> <!-- fieldsets -->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Student Personal Info:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 1 - 4</h2>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Admission for Standard<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="standard_id" aria-label="Default select example">
                                                    <option selected>Select Standard </option>
                                                    @foreach ($standard as $standards)
                                                        <option value="{{ $standards->id }}" {{ $result->standard_id == $standards->id ? 'selected' : '' }}>{{ $standards->standard_name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Academic Year<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="fy_id" aria-label="Default select example">
                                                    <option value="" selected>Select Year</option>
                                                    @foreach ($financial_y as $financial_year)
                                                        <option value="{{ $financial_year->id }}" {{ $result->fy_id ==  $financial_year->id ? 'selected' : '' }}>{{ $financial_year->year }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="firstname" class="form-control" placeholder="First Name" value="{{ $result->firstname }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="middlename" class="form-control" placeholder="Middle Name" value="{{ $result->middlename }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="lastname" value="{{ $result->lastname }}" class="form-control" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="dob" id="inputGroupPrepend45" value="{{ $result->dob }}" aria-describedby="inputGroupPrepend45" placeholder="Enter your D.O.B">

                                                </div>
                                            </div>

                                            <?php $dob = $result->dob;
                                            $dob = new DateTime($dob);
                                            $currentDate = new DateTime();
                                            $age = $currentDate->diff($dob)->y; ?>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="age" value="{{ $age }}" id="age" aria-describedby="inputGroupPrepend2" placeholder="Age" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="gender">
                                                    <option value="" selected="selected">Select Gender</option>
                                                    <option value="1" {{ $result->gender == 1 ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ $result->gender == 2 ? 'selected' : '' }}>Female</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="nationality" value="Indian" aria-describedby="inputGroupPrepend2" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="birth_place" value="{{ $result->birth_place }}" aria-describedby="inputGroupPrepend2" placeholder="Place Of Birth">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_tongue" value="{{ $result->mother_tongue }}" aria-describedby="inputGroupPrepend2" placeholder="Mother Tongue">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="religion" id="religion">
                                                    <option selected>Select Religion</option>
                                                    @foreach ($religion as $religions)

                                                         <option value="{{ $religions->id }}" {{ $result->religion == $religions->id ? 'selected' : '' }}>{{ $religions->religion_name  }}</option>
                                                    @endforeach

                                                </select>
                                                <span class="text-danger">
                                                    @error('religion')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="cast_id">
                                                    <option selected>Select Cast Category </option>
                                                    @foreach ($cast as $casts)
                                                        <option value="{{ $casts->id }}" {{ $result->cast_id == $casts->id ? 'selected' : '' }}>{{ $casts->cast_name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="adhaar_no" value="{{ $result->adhaar_no }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" maxlength="12">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Types of Blood Group</label>
                                                <select class=" default-select form-control wide" name="blood_group" aria-label="Default select example">
                                                    <option value="">Select Blood Group</option>
                                                    <option value="1" {{ $result->blood_group == 1 ? 'selected' : '' }}>A+</option>
                                                    <option value="2" {{ $result->blood_group == 2 ? 'selected' : '' }}>A-</option>
                                                    <option value="3" {{ $result->blood_group == 3 ? 'selected' : '' }}>B+</option>
                                                    <option value="4" {{ $result->blood_group == 4 ? 'selected' : '' }}>B-</option>
                                                    <option value="5" {{ $result->blood_group == 5 ? 'selected' : '' }}>AB+</option>
                                                    <option value="6" {{ $result->blood_group == 6 ? 'selected' : '' }}>AB-</option>
                                                    <option value="7" {{ $result->blood_group == 7 ? 'selected' : '' }}>O+</option>
                                                    <option value="8" {{ $result->blood_group == 9 ? 'selected' : '' }}>O-</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Father's Details:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 2 - 4</h2>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label"> Father's First Name<span class="required">*</span></label>
                                                    <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" placeholder="Father's First Name">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Middle Name <span class="required">*</span></label>
                                                    <input type="text" name="middle_name" value="{{ $user->middle_name }}" class="form-control" placeholder="Father's Middle Name ">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Last Name <span class="required">*</span></label>
                                                    <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" placeholder="Father's Last Name">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mobile_no" class="form-control" value="{{ $user->mobile_no }}" placeholder="(+1)408-357-9007" maxlength="12">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="father_alt_contact" value="{{ $result->father_alt_contact }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email Address">

                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_qualification" value="{{ $result->father_qualification }}" id="emial1" placeholder="Qualification">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_profession" value="{{ $result->father_profession }}" id="emial1" placeholder="Occupation/profession">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="father_designation" value="{{ $result->father_designation }}" placeholder="Designation">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="father_office_address" value="{{ $result->father_office_address }}" id="emial1" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>

                                            {{-- <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_email" value="{{ $result->father_email }}" id="emial1" placeholder="Email Address">
                                                </div>
                                            </div> --}}
                                            {{-- <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Phone Number<span class="required">*</span></label>
                                                    <input type="text" name="father_mobile_no" class="form-control" value="{{ $result->father_mobile_no }}" placeholder="(+1)408-657-9007" maxlength="12">
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="father_adhaar_no" value="{{ $result->father_adhaar_no }}" class="form-control" maxlength="12">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="father_pan_no" value="{{ $result->father_pan_no }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>

                                            <h2 class="fs-title">Mother's Details:</h2>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's First Name <span class="required">*</span></label>
                                                    <input type="text" name="mother_first_name" class="form-control" value="{{ $result->mother_first_name }}" placeholder="Mother's First Name">
                                                    <span class="text-danger">
                                                        @error('mother_first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_middle_name" class="form-control" value="{{ $result->mother_middle_name }}" placeholder="Mother's Middle Name">
                                                    <span class="text-danger">
                                                        @error('mother_middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Last Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_last_name" class="form-control" value="{{ $result->mother_last_name }}" placeholder="Mother's Last Name">
                                                    <span class="text-danger">
                                                        @error('mother_last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_mobile_no" value="{{ $result->mother_mobile_no }}" class="form-control" placeholder="(+1)408-657-9007" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('mother_mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_email" value="{{ $result->mother_email }}" id="emial1" placeholder="Email Address">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" name="mother_qualification" value="{{ $result->mother_qualification }}" placeholder="Qualification">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_profession" value="{{ $result->mother_profession }}" id="emial1" placeholder="Occupation/profession">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="mother_designation" value="{{$result->mother_designation }}" placeholder="Designation">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="mother_alt_contact" value="{{ $result->mother_alt_contact }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="mother_office_address" value="{{ $result->mother_office_address }}" id="emial1" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_adhaar_no" value="{{ $result->mother_adhaar_no }}" class="form-control" maxlength="12">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="mother_pan_no" value="{{ $result->mother_pan_no }}" class="form-control" required>

                                                </div>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Current Residential Address:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 3 - 4</h2>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Residential House Type<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="residential_type" id="residential_type" aria-label="Default select example">
                                                    <option value="">Select Residential Type</option>
                                                    <option value="1" {{ $result->residential_type == 1 ? 'selected' : ''  }}>Rental</option>
                                                    <option value="2" {{ $result->residential_type == 2 ? 'selected' : ''  }}>Own</option>
                                                </select>
                                            </div>



                                            <div id="months_section" class="col-lg-4 mb-2" @if($result->residential_type == 1) style="display: block" @else style="display: none" @endif>
                                                <div class="mb-3">
                                                    <label class="text-label form-label">How many months in current address<span class="required">*</span></label>
                                                    <input type="text" name="no_of_months" value="{{ $result->no_of_months}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="house_no" value="{{ $result->house_no}}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="area" value="{{ $result->area }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="landmark" value="{{ $result->landmark }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="pincode" value="{{ $result->pincode }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="city_name" value="{{ $result->city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <h2 class="fs-title">Permanent Address:</h2>

                                            <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="same_address" value="1" {{ $result->same_address == 1 ? 'checked' : '' }}>
                                                    <label class="text-label form-label">Same as above</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="p_house_no" value="{{ $result->p_house_no}}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="p_area" value="{{ $result->p_area }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="p_landmark" value="{{ $result->p_landmark }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="p_pincode" value="{{ $result->p_pincode }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="p_city_name" value="{{ $result->p_city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>
                                            <h2 class="fs-title">Distance:</h2>
                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Distance between School & Resident<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="distance_id" value="{{ old('distance_id') }}" aria-label="Default select example">
                                                    <option selected>Select Distance </option>
                                                    @foreach ($distance as $distances)
                                                        <option value="{{ $distances->id }}" {{ $result->distance_id == $distances->id ? 'selected' : '' }}>{{ $distances->distance_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">School Name<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="school_id" aria-label="Default select example">
                                                    <option value="" selected="selected">Select School</option>
                                                    @foreach ($school as $schools)
                                                        <option value="{{ $schools->id }}" {{ $result->school_id == $schools->id ? 'selected' : '' }}>{{ $schools->school_name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger">
                                                    @error('school_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Application Date<span class="required">*</span></label>

                                                    <input type="text" id="current_date" name="current_date" value="{{ now()->format('Y-m-d') }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                        </div>
                                    </div><input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>


                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Document Uploads:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 4 - 4</h2>
                                            </div>
                                            @foreach ($document as $documents)

                                            @php
                                               $twins_id = [6, 7, 8];

                                               $displayStyle = $result->twins == 2 ? (in_array($documents->id, $twins_id) ? 'none' : 'block') : 'block';

                                            //    dd($displayStyle);

                                            @endphp

                                             <div class="col-lg-4 mb-2" style="display: {{ $displayStyle }};">
                                                    <div class="mb-3">
                                                        <label>{{ $documents->document }} </label>
                                                        <input type="hidden" name="document_id[]" class="form-control" value="{{ $documents->id }}">
                                                        <input type="file" name="document_file[]" class="form-control" multiple><br>
                                                        <a href="{{ asset('documents/' . $documents->document_file) }}" class="btn btn-primary shadow btn-xs sharp me-1" target="_blank"> <i class="fas fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div><button class="btn btn-primary action-button" value="Submit">Save</button> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add this script to the end of your Blade file -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        document.getElementById('residential_type').addEventListener('change', function() {
            var monthsSection = document.getElementById('months_section');
            if (this.value === '1') {
                monthsSection.style.display = 'block';

            } else {

                monthsSection.style.display = 'none';
            }
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var textInputs = document.querySelectorAll('input[type="text"]:not([name="email"]):not([name="mother_email"]), textarea');
            textInputs.forEach(function(input) {
                input.addEventListener('input', function() {
                    this.value = this.value.toUpperCase();
                });
            });
        });

        $(document).ready(function() {
            // Function to calculate and update age when Date of Birth changes
            $('#inputGroupPrepend45').on('change', function() {
                var dob = $(this).val();
                if (dob) {
                    var age = calculateAge(new Date(dob));
                    $('#age').val(age);
                }
            });

            // Function to calculate age based on Date of Birth
            function calculateAge(dob) {
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }

            // Initialize age on page load
            var initialDob = $('#inputGroupPrepend45').val();
            if (initialDob) {
                var initialAge = calculateAge(new Date(initialDob));
                $('#age').val(initialAge);
            }
        });
    </script>

<script>
    function copyAddress() {
        var sameAddressCheckbox = document.getElementsByName('same_address')[0];
        var permanentFields = ['p_house_no', 'p_area', 'p_landmark', 'p_pincode', 'p_city_name'];

        if (sameAddressCheckbox.checked) {

            permanentFields.forEach(function(field) {
                document.getElementsByName(field)[0].disabled = true;
            });


            var residentialFields = ['house_no', 'area', 'landmark', 'pincode', 'city_name'];
            residentialFields.forEach(function(field) {
                var residentialValue = document.getElementsByName(field)[0].value;
                var permanentField = document.getElementsByName('p_' + field)[0];
                permanentField.value = residentialValue;
            });
        } else {

            permanentFields.forEach(function(field) {
                document.getElementsByName(field)[0].disabled = false;
            });

            document.getElementsByName('p_house_no')[0].value = '';
            document.getElementsByName('p_area')[0].value = '';
            document.getElementsByName('p_landmark')[0].value = '';
            document.getElementsByName('p_pincode')[0].value = '';
            document.getElementsByName('p_city_name')[0].value = '';
        }
    }

    document.getElementsByName('same_address')[0].addEventListener('change', copyAddress);

    copyAddress();
</script>
@endsection
