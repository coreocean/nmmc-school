@extends('layouts.master')

@section('content')

<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">View Documents </h5>
						</div>

						<div class="card-body">
                            <div class="row">
                                @foreach($document as $documents)
                                <div class="col-lg-4 mb-2">
                                    <div class="mb-3">
                                        <label><a href="{{ asset('documents/'.$documents->document_file)}}" style="color: #090b0b;
                                            font-weight: 600;font-size: 14px;" target="_blank">{{$documents->document}}</a></label>
                                        <input type="hidden" name="document_id[]" class="form-control" value="{{$documents->id}}">

                                        {{-- Check the file extension --}}
                                        @php
                                            $extension = pathinfo($documents->document_file, PATHINFO_EXTENSION);
                                        @endphp

                                        @if (in_array($extension, ['pdf']))
                                            {{-- Display PDF in an iframe --}}
                                            <iframe src="{{ asset('documents/'.$documents->document_file) }}" width="80%" height="180px"></iframe></a>
                                        @else
                                            {{-- Display image --}}
                                            <img src="{{ asset('documents/'.$documents->document_file) }}" alt="{{$documents->document}}" width="60%" height="auto">
                                        @endif

                                        {{-- Alternatively, you can use a link to open the PDF or image in a new tab --}}
                                        {{-- <a href="{{ asset('documents/'.$documents->document_file)}}" target="_blank">Open Document</a> --}}
                                    </div>
                                </div>
                            @endforeach

                                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
