@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Application Form </h5>
						</div>

						<div class="card-body">
                            <!-- <form action="#" method="post" class="new-added-form needs-validation" enctype="multipart/form-data"> -->
                            <div class="row">
                            <div class="mb-3 col-md-6">
                                    <label class="form-label">Application No.</label>
                                    <input type="text" class="form-control" value="{{ $data->application_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Admission for Standard</label>
                                    <input type="text" class="form-control" value="{{ $data->standard_name }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">First Name</label>
                                    <input  type="text" class="form-control" value="{{ $data->firstname }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Middle Name</label>
                                    <input type="text" class="form-control" value="{{ $data->middlename }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Last Name</label>
                                    <input type="text" class="form-control" value="{{ $data->lastname }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Date Of Birth</label>
                                    <input type="text" class="form-control" value="{{ $data->dob }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Age</label>
                                    <input  class="form-control"  value="{{ $data->age }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Nationality</label>
                                    <input  class="form-control" value="{{ $data->nationality }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Place Of Birth</label>
                                    <input type="text" class="form-control" value="{{ $data->birth_place }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Mother Tongue</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_tongue }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Religion</label>
                                    <input type="text" class="form-control" value="{{ $data->religion }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">If member of backword community, specify</label>
                                    <input type="text" class="form-control" value="{{ $data->cast_name }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Adhaar Number</label>
                                    <input type="text" class="form-control" value="{{ $data->adhaar_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Gender</label>
                                    <input type="text" class="form-control" value="{{$data->gender == 1 ? 'MALE' : 'FEMALE'}}" readonly>

                                </div>
                                <div >
                                <h2 >Father Details:</h2>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Name of Father</label>
                                    <input type="text" class="form-control" value="{{ $data->father_name }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Qualification</label>
                                    <input type="text" class="form-control" value="{{ $data->father_qualification }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Occupation/profession</label>
                                    <input type="text" class="form-control" value="{{ $data->father_profession }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Office Address (If applicable)</label>
                                    <input type="text" class="form-control" value="{{ $data->father_office_address }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Email Address</label>
                                    <input type="text" class="form-control" value="{{ $data->father_email }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Phone Number</label>
                                    <input type="text" class="form-control" value="{{ $data->father_mobile_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Adhaar Number</label>
                                    <input type="text" class="form-control" value="{{ $data->father_adhaar_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Pan Number</label>
                                    <input type="text" class="form-control" value="{{ $data->father_pan_no }}" readonly>
                                </div>
                                <div >
                                <h2 >Mother Details:</h2>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Name of Mother</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_name }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Qualification</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_qualification }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Occupation/profession</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_profession }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Office Address (If applicable)</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_office_address }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Email Address</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_email }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Phone Number</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_mobile_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Adhaar Number</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_adhaar_no }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Pan Number</label>
                                    <input type="text" class="form-control" value="{{ $data->mother_pan_no }}" readonly>
                                </div>
                                <div >
                                <h2 >Address:</h2>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Residential address(Local) of Parent/Gardian</label>
                                    <textarea class="form-control"  rows="3" readonly>{{ $data->local_address }}</textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Permanent address of Parent</label>
                                    <textarea class="form-control"  rows="3" readonly>{{ $data->permanent_address }}</textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Distance between School & Resident</label>
                                    <input type="text" class="form-control" value="{{ $data->distance_name }}" readonly>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">School Name</label>
                                    <input type="text" class="form-control" value="{{ $data->school_name }}" readonly>
                                </div>
                                <div >
                                <h2 > Document Uploaded:</h2>
                                </div>
                               
                                @foreach($document as $documents)
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">{{$documents->document}}</label>
                                    <a href="{{ asset('documents/'.$documents->document_file)}}" class="btn btn-primary shadow btn-xs sharp me-1" target="_blank"> <i class="fas fa-eye"></i></a>
                                </div>
                                @endforeach
                            </div>
                        <!-- </form> -->
						</div>
					</div>
				</div>
			</div>
		</div>
      

@endsection
