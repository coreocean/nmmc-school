@extends('layouts.master')

@section('content')

	<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
			<div class="container-fluid">
				<div class="row">

				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-header">
							<h5 class="mb-0">Uploaded Documents </h5>
						</div>

						<div class="card-body">
                            <!-- <form action="#" method="post" class="new-added-form needs-validation" enctype="multipart/form-data"> -->
                            <div class="row">

                                <div >
                                {{-- <h2 > Document Uploaded:</h2> --}}
                                </div>

                                @foreach($document as $documents)
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">{{$documents->document}}</label>
                                    <a href="{{ asset('documents/'.$documents->document_file)}}" class="btn btn-primary shadow btn-xs sharp me-1" target="_blank"> <i class="fas fa-eye"></i></a>
                                </div>
                                @endforeach
                            </div>
                        <!-- </form> -->
						</div>
					</div>
				</div>
			</div>
		</div>


@endsection
