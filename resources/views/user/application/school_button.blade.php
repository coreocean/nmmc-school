@extends('layouts.master')

@section('content')
    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                @foreach ($school as $schools)
                                    <div class="col-xl-4">
                                        <div class="content-box">

                                            <div class="chart-num">
                                                <div class="chart-num">
                                                    <ul class="nav nav-tabs dzm-tabs" id="myTab-2" role="tablist">
                                                        <li class="nav-item" role="presentation">
                                                            <a href="{{ url('termsandconditions/' . $schools->id) }}"><button class="btn btn-lg btn-primary" id="home-tab-2" data-bs-toggle="tab" data-bs-target="#withoutSpace" type="button" role="tab"
                                                                    aria-selected="true">{{ $schools->school_name }}</button></a>
                                                        </li>

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach



                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
@endsection
