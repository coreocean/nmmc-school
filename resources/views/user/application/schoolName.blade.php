@extends('layouts.master')

@section('content')


 <div class="content-body">
			<div class="container-fluid">
				<!-- Row -->
				<div class="row">
					<div class="col-xl-12">
                        <!-- Row -->
						<div class="row">
                            
                            <div class="col-xl-12">
                                 <!-- Row -->
                                <div class="row">
                                    <!--column-->
                                     @foreach ($school as $schools)
                                    <div class="col-xl-4 col-lg-4 col-sm-6">
                                        <div class="card contact_list text-center">
                                            
                                            
                                            <div class="card-body">
                                                <div class="user-content">
                                                    <div class="user-info">
                                                        
                                                        <div class="user-details">
                                                            <h4 class="user-name mb-0" style="font-size: 20px;">{{ $schools->school_name }}</h4>
                                                            <p>{{$schools->school_address}}</p>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="contact-icon">
                                                   <span class="badge badge-secondary light mx-2" style='font-size: 11px;'>{{$schools->end_date}}</span>
												   <!--<span class="badge badge-secondary light mx-2">Science</span> -->
												   <!--<span class="badge badge-danger light">Art</span>-->
                                                </div>
												<div class=" align-items-center">
													<a href="{{ url('termsandconditions/' . $schools->id ?? '') }}" class="btn btn-primary btn-sm w-50 me-2">Apply</a>
													<!--<a href="chat.html" class="btn  btn-light btn-sm w-50"><i class="fa-sharp fa-regular fa-envelope me-2"></i>Chat</a>-->
												</div>
                                            </div>
                                            
                                           
                                        </div>
                                    </div>
                                   
									  @endforeach
                                
								

								</div>	
							<!--/column-->
							</div>
                           
                        	<!--/Row -->
						</div>
					
						
					</div>
				</div>
			</div>
		</div>





@endsection