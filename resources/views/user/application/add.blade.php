@extends('layouts.master')

<style>
    #progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25% !important;
    float: left;
    position: relative;
    font-weight: 400;
}
</style>

@section('content')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">Add Student Application </h5>
                        </div>

                        <div class="card-body">
                            <form id="msform" action="{{ route('student_application.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active" id="account"><strong>Student Personal Info</strong></li>
                                    <li id="personal"><strong>Parent's Details</strong></li>
                                    {{-- <li id="payment"><strong>Mother</strong></li> --}}
                                    <li id="confirm"><strong>Address</strong></li>
                                    <li id="upload"><strong>Document Upload</strong></li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> <br> <!-- fieldsets -->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Student Personal Info:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 1 - 4</h2>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Admission for Standard<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="standard_id" aria-label="Default select example">
                                                    <option value="" selected="selected">Select Standard</option>
                                                    @foreach ($standard as $standards)
                                                         <option value="{{ $standards->id }}" {{ old('standard_id') == $standards->id ? 'selected' : '' }}>{{ $standards->standard_name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger">
                                                    @error('standard_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Academic Year<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="fy_id" aria-label="Default select example">
                                                    <option value="">Select Year</option>
                                                    @foreach ($financial_y as $financial_year)
                                                        <option value="{{ $financial_year->id }}" selected>{{ $financial_year->year }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger">
                                                    @error('fy_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="twins" id="twins" value="2">
                                                    <label class="text-label form-label">Twins</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="firstname" class="form-control" placeholder="First Name" value="{{ old('firstname') }}">
                                                    <span class="text-danger">
                                                        @error('firstname')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="middlename" class="form-control" placeholder="Middle Name" value="{{ old('middlename') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="lastname" value="{{ old('lastname') }}" class="form-control" placeholder="Last Name">
                                                    <span class="text-danger">
                                                        @error('lastname')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="dob" id="inputGroupPrepend45" aria-describedby="inputGroupPrepend45" placeholder="Enter your D.O.B">
                                                    <span class="text-danger">
                                                        @error('dob')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="age" id="age" aria-describedby="inputGroupPrepend2" placeholder="Age" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="gender">
                                                    <option value="" selected="selected">Select Gender</option>
                                                    <option value="1" {{ old('gender') == '1' ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ old('gender') == '2' ? 'selected' : '' }}>Female</option>
                                                </select>
                                                <span class="text-danger">
                                                    @error('gender')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="nationality" value="Indian" aria-describedby="inputGroupPrepend2" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="birth_place" value="{{ old('birth_place') }}" aria-describedby="inputGroupPrepend2" placeholder="Place Of Birth">
                                                    <span class="text-danger">
                                                        @error('birth_place')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_tongue" value="{{ old('mother_tongue') }}" aria-describedby="inputGroupPrepend2" placeholder="Mother Tongue">
                                                    <span class="text-danger">
                                                        @error('mother_tongue')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                <select class="default-select form-control wide" aria-label="Default select example" name="religion" id="religion" value="{{old('religion')}}">
                                                    <option value="">Select Religion</option>
                                                    @foreach ($religion as $religions)
                                                         <option value="{{ $religions->id }}" {{ old('religion') == $religions->id ? 'selected' : '' }}>{{ $religions->religion_name }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="text-danger">
                                                    @error('religion')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>



                                            <div class="col-lg-4 mb-2" >
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <select  name="cast_id" class="form-control" value="{{old('cast_id')}}">
                                                        <option value="">Select Cast Category</option>
                                                        @foreach ($cast as $casts)
                                                          <option value="{{ $casts->id }}" {{ old('cast_id') == $casts->id ? 'selected' : '' }}>{{ $casts->cast_name }}</option>
                                                    @endforeach
                                                    </select>
                                                <span class="text-danger">
                                                    @error('cast_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="adhaar_no" value="{{ old('adhaar_no') }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" value="{{ old('adhaar_no') }}" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Types of Blood Group</label>
                                                <select class=" default-select form-control wide" name="blood_group" aria-label="Default select example" value="{{ old('blood_group') }}">
                                                    <option value="">Select Blood Group</option>
                                                    <option value="1" {{ old('blood_group') == '1' ? 'selected' : '' }}>A+</option>
                                                    <option value="2" {{ old('blood_group') == '2' ? 'selected' : '' }}>A-</option>
                                                    <option value="3" {{ old('blood_group') == '3' ? 'selected' : '' }}>B+</option>
                                                    <option value="4" {{ old('blood_group') == '4' ? 'selected' : '' }}>B-</option>
                                                    <option value="5" {{ old('blood_group') == '5' ? 'selected' : '' }}>AB+</option>
                                                    <option value="6" {{ old('blood_group') == '6' ? 'selected' : '' }}>AB-</option>
                                                    <option value="7" {{ old('blood_group') == '7' ? 'selected' : '' }}>O+</option>
                                                    <option value="8" {{ old('blood_group') == '8' ? 'selected' : '' }}>O-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="row">
                                            <h2 class="fs-title second_section" style="display:none">Second Child Form:</h2>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="t_firstname" class="form-control" placeholder="First Name" value="{{ old('t_firstname') }}">

                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="t_middlename" class="form-control" placeholder="Middle Name" value="{{ old('t_middlename') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="t_lastname" value="{{ old('t_lastname') }}" class="form-control" placeholder="Last Name">

                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="t_dob" id="t_dob" aria-describedby="inputGroupPrepend45" placeholder="Enter your D.O.B">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_age" id="t_age" aria-describedby="inputGroupPrepend2" placeholder="Age" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" aria-label="Default select example" name="t_gender">
                                                    <option value="" selected="selected">Select Gender</option>
                                                    <option value="1" {{ old('t_gender') == '1' ? 'selected' : '' }}>Male</option>
                                                    <option value="2" {{ old('t_gender') == '2' ? 'selected' : '' }}>Female</option>
                                                </select>

                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_nationality" value="Indian" aria-describedby="inputGroupPrepend2" readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_birth_place" value="{{ old('t_birth_place') }}" aria-describedby="inputGroupPrepend2" placeholder="Place Of Birth">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_mother_tongue" value="{{ old('t_mother_tongue') }}" aria-describedby="inputGroupPrepend2" placeholder="Mother Tongue">

                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="t_adhaar_no" value="{{ old('t_adhaar_no') }}" aria-describedby="inputGroupPrepend2" placeholder="Adhaar Number" maxlength="12">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Types of Blood Group</label>
                                                <select class=" default-select form-control wide" name="t_blood_group" value="{{ old('t_blood_group') }}" aria-label="Default select example">
                                                    <option value="">Select Blood Group</option>
                                                    <option value="1" {{ old('t_blood_group') == '1' ? 'selected' : '' }}>A+</option>
                                                    <option value="2" {{ old('t_blood_group') == '2' ? 'selected' : '' }}>A-</option>
                                                    <option value="3" {{ old('t_blood_group') == '3' ? 'selected' : '' }}>B+</option>
                                                    <option value="4" {{ old('t_blood_group') == '4' ? 'selected' : '' }}>B-</option>
                                                    <option value="5" {{ old('t_blood_group') == '5' ? 'selected' : '' }}>AB+</option>
                                                    <option value="6" {{ old('t_blood_group') == '6' ? 'selected' : '' }}>AB-</option>
                                                    <option value="7" {{ old('t_blood_group') == '7' ? 'selected' : '' }}>O+</option>
                                                    <option value="8" {{ old('t_blood_group') == '8' ? 'selected' : '' }}>O-</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                <select class="default-select form-control wide" aria-label="Default select example" name="t_religion" id="t_religion" value="{{old('t_religion')}}">
                                                    <option value="">Select Religion</option>

                                                    @foreach ($religion as $religions)
                                                        <option value="{{ $religions->id }}" {{ old('t_religion') == $religions->id ? 'selected' : '' }}>{{ $religions->religion_name }}</option>
                                                    @endforeach

                                                </select>
                                                <span class="text-danger">
                                                    @error('t_religion')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>



                                            <div class="col-lg-4 mb-2 second_section" style="display:none">
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <select  name="t_cast_id" class="form-control" value="{{old('t_cast_id')}}">
                                                        <option value="">Select Cast Category</option>
                                                        @foreach ($cast as $casts)
                                                        <option value="{{ $casts->id }}">{{ $casts->cast_name }}</option>
                                                         <option value="{{ $casts->id }}" {{ old('t_cast_id') == $casts->id ? 'selected' : '' }}>{{ $casts->cast_name }}</option>
                                                    @endforeach
                                                    </select>

                                                <span class="text-danger">
                                                    @error('t_cast_id')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                            </div>
                                        </div>


                                    </div> <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Father's Details:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 2 - 4</h2>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label"> Father's First Name<span class="required">*</span></label>
                                                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" placeholder="Father's First Name">
                                                    <span class="text-danger">
                                                        @error('first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Middle Name <span class="required">*</span></label>
                                                    <input type="text" name="middle_name" value="{{ old('middle_name') }}" class="form-control" placeholder="Father's Middle Name ">
                                                    <span class="text-danger">
                                                        @error('middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Last Name <span class="required">*</span></label>
                                                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" placeholder="Father's Last Name">
                                                    <span class="text-danger">
                                                        @error('last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mobile_no" class="form-control" value="{{ old('mobile_no') }}" placeholder="(+1)408-357-9007" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="father_alt_contact" value="{{ old('father_alt_contact') }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email Address">
                                                    <span class="text-danger">
                                                        @error('email')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" name="father_qualification" value="{{ old('father_qualification') }}" placeholder="Qualification">

                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_profession" value="{{ old('father_profession') }}" placeholder="Occupation/profession">
                                                    <span class="text-danger">
                                                        @error('father_profession')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="father_designation" value="{{ old('father_designation') }}" placeholder="Designation">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="father_office_address" value="{{ old('father_office_address') }}" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="father_adhaar_no" value="{{ old('father_adhaar_no') }}" class="form-control" maxlength="12" placeholder="Father's Adhaar Card No.">
                                                    <span class="text-danger">
                                                        @error('father_adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="father_pan_no" value="{{ old('father_pan_no') }}" class="form-control" placeholder="PAN No.">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>

                                            <h2 class="fs-title">Mother's Details:</h2>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's First Name <span class="required">*</span></label>
                                                    <input type="text" name="mother_first_name" class="form-control" value="{{ old('mother_first_name') }}" placeholder="Mother's First Name">
                                                    <span class="text-danger">
                                                        @error('mother_first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_middle_name" class="form-control" value="{{ old('mother_middle_name') }}" placeholder="Mother's Middle Name">
                                                    <span class="text-danger">
                                                        @error('mother_middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Last Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_last_name" class="form-control" value="{{ old('mother_last_name') }}" placeholder="Mother's Last Name">
                                                    <span class="text-danger">
                                                        @error('mother_last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_mobile_no" value="{{ old('mother_mobile_no') }}" class="form-control" placeholder="(+1)408-357-9007" maxlength="12">
                                                    <span class="text-danger">
                                                        @error('mother_mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_email" value="{{ old('mother_email') }}" placeholder="Email Address">
                                                    <span class="text-danger">
                                                        @error('mother_email')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" name="mother_qualification" value="{{ old('mother_qualification') }}" placeholder="Qualification">

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_profession" value="{{ old('mother_profession') }}" placeholder="Occupation/profession">
                                                    <span class="text-danger">
                                                        @error('mother_profession')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="mother_designation" value="{{ old('mother_designation') }}" placeholder="Designation">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="mother_alt_contact" value="{{ old('mother_alt_contact') }}" placeholder="Alternate Contact Number">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="mother_office_address" value="{{ old('mother_office_address') }}" placeholder="Office Address (If applicable)">
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_adhaar_no" value="{{ old('mother_adhaar_no') }}" class="form-control" maxlength="12" placeholder="Mother's Adhaar Card No.">
                                                    <span class="text-danger">
                                                        @error('mother_adhaar_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="mother_pan_no" value="{{ old('mother_pan_no') }}" class="form-control" placeholder="PAN No.">

                                                </div>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Current Residential Address:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 3 - 4</h2>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Residential House Type<span class="required">*</span></label>
                                                <select class=" default-select form-control wide" name="residential_type" id="residential_type" aria-label="Default select example">
                                                    <option value="">Select Residential Type</option>
                                                    <option value="1" {{ old('residential_type') == '1' ? 'selected' : '' }}>Rental</option>
                                                    <option value="2" {{ old('residential_type') == '2' ? 'selected' : '' }}>Own</option>
                                                </select>
                                            </div>

                                            <div id="months_section" class="col-lg-4 mb-2" style="display: none">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">How many months in current address<span class="required">*</span></label>
                                                    <input type="text" name="no_of_months" value="{{ old('no_of_months') }}" class="form-control" placeholder="How many months in current address">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="house_no" value="{{ old('house_no') }}" class="form-control"  placeholder="Flat/House No./Building/Apartment">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="area" value="{{ old('area') }}" class="form-control" placeholder="Area/Street/Sector">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="landmark" value="{{ old('landmark') }}" class="form-control" placeholder="Land Mark">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="city_name" value="{{ old('city_name') }}" class="form-control" placeholder="City">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">District<span class="required">*</span></label>
                                                    <input type="text" name="district" value="{{ old('district') }}" class="form-control" placeholder="District">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">State<span class="required">*</span></label>
                                                    <input type="text" name="state"  value="Maharashtra" class="form-control" placeholder="State">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="pincode" value="{{ old('pincode') }}" class="form-control" placeholder="Pincode No.">
                                                </div>
                                            </div>



                                            <h2 class="fs-title">Permanent Address:</h2>

                                            <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="same_address" value="1">
                                                    <label class="text-label form-label">Same as above</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="p_house_no" value="{{ old('p_house_no') }}" class="form-control" placeholder="Flat/House No./Building/Apartment">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="p_area" value="{{ old('p_area') }}" class="form-control" placeholder="Area/Street/Sector">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="p_landmark" value="{{ old('p_landmark') }}" class="form-control" placeholder="Land Mark">
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="p_city_name" value="{{ old('p_city_name') }}" class="form-control" placeholder="City">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">District<span class="required">*</span></label>
                                                    <input type="text" name="p_district" value="{{ old('p_district') }}" class="form-control" placeholder="District">
                                                </div>
                                            </div>
                                        <div class="col-lg-4 mb-2">
                                            <div class="mb-3">
                                                    <label class="text-label form-label">State<span class="required">*</span></label>
                                                    <input type="text" name="p_state" value="Maharashtra" class="form-control" placeholder="State">
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="p_pincode" value="{{ old('p_pincode') }}" class="form-control" placeholder="Pincode No.">
                                            </div>
                                        </div>







                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">

                                                    <input type="hidden" name="school_id" value="{{ $school->id }}" class="form-control" readonly>

                                                </div>
                                            </div>






                                        </div>
                                    </div><input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>



                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Document Uploads:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 4 - 4</h2>
                                            </div>

                                            @foreach ($document as $documents)
                                                @php
                                                    $twins_id = [6, 7, 8];
                                                    $class = in_array($documents->id, $twins_id) ? 'twins' : '';
                                                @endphp

                                                <div class="col-lg-4 mb-2 {{ $class }}">
                                                    <div class="mb-3">
                                                        <label>{{ $documents->document }} @if ($documents->is_required == 1)
                                                                <span class="required">*</span>
                                                            @endif
                                                        </label>
                                                        <input type="hidden" name="document_id[]" class="form-control" value="{{ $documents->id }}">
                                                        <input type="file" name="document_file[]" class="form-control" multiple>
                                                        <span class="text-danger">
                                                            @error('document_file')
                                                                {{ $message }}
                                                            @enderror
                                                        </span>
                                                    </div>
                                                </div>
                                            @endforeach




                                        </div>
                                    </div><button class="btn btn-primary action-button" value="Submit">Save</button> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>

$('#religion').on('change', function () {
    var religions = $(this).val();

    $("#cast_id").html('');
    $.ajax({
        type: "POST",
        url: "{{ url('get-cast') }}",
        data: {
            "religions": [religions],
            "_token": '{{ csrf_token() }}'
        },
        dataType: 'json',
        success: function (res) {
            $('#cast_id').html('<option value="">Select Cast Category</option>');
            $.each(res, function (key, value) {
                $("#cast_id").append('<option value="' + value.id + '">' + value.cast_name + '</option>');
            });
        }
    });
});


        function toggleSecondSection() {
            var twinsCheckbox = document.getElementById('twins');
            var secondSection = document.getElementsByClassName('second_section');


            for (var i = 0; i < secondSection.length; i++) {
                if (twinsCheckbox.checked) {
                    secondSection[i].style.display = 'block';
                    $('.twins').show();
                } else {

                    $('.twins').hide();
                    secondSection[i].style.display = 'none';
                }
            }
        }


        document.getElementById('twins').addEventListener('change', toggleSecondSection);
        toggleSecondSection();
    </script>



    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var textInputs = document.querySelectorAll('input[type="text"]:not([name="email"]):not([name="mother_email"]), textarea');
            textInputs.forEach(function(input) {
                input.addEventListener('input', function() {
                    this.value = this.value.toUpperCase();
                });
            });
        });


        $(document).ready(function() {
            $('#inputGroupPrepend45').on('change', function() {
                var dob = $(this).val();
                if (dob) {
                    var age = calculateAge(new Date(dob));
                    $('#age').val(age);
                }
            });

            function calculateAge(dob) {
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }
        });


        $(document).ready(function() {
            $('#t_dob').on('change', function() {
                var dob = $(this).val();
                if (dob) {
                    var age = calculateTAge(new Date(dob));
                    $('#t_age').val(age);
                }
            });

            function calculateTAge(dob) {
                var today = new Date();
                var birthDate = new Date(dob);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }
        });
    </script>

    <script>
        document.getElementById('residential_type').addEventListener('change', function() {
            var monthsSection = document.getElementById('months_section');
            if (this.value === '1') {
                monthsSection.style.display = 'block';
            } else {
                monthsSection.style.display = 'none';
            }
        });
    </script>


<script>
    function copyAddress() {
        var sameAddressCheckbox = document.getElementsByName('same_address')[0];
        var permanentFields = ['p_house_no', 'p_area', 'p_landmark', 'p_pincode', 'p_city_name', 'p_district', 'p_state'];

        if (sameAddressCheckbox.checked) {

            permanentFields.forEach(function(field) {
                document.getElementsByName(field)[0].disabled = true;
            });


            var residentialFields = ['house_no', 'area', 'landmark', 'pincode', 'city_name','city_name','district','state'];
            residentialFields.forEach(function(field) {
                var residentialValue = document.getElementsByName(field)[0].value;
                var permanentField = document.getElementsByName('p_' + field)[0];
                permanentField.value = residentialValue;
            });
        } else {

            permanentFields.forEach(function(field) {
                document.getElementsByName(field)[0].disabled = false;
            });

            document.getElementsByName('p_house_no')[0].value = '';
            document.getElementsByName('p_area')[0].value = '';
            document.getElementsByName('p_landmark')[0].value = '';
            document.getElementsByName('p_pincode')[0].value = '';
            document.getElementsByName('p_city_name')[0].value = '';
            document.getElementsByName('p_district')[0].value = '';
            document.getElementsByName('p_state')[0].value = '';
        }
    }

    document.getElementsByName('same_address')[0].addEventListener('change', copyAddress);

    copyAddress();
    </script>
@endsection
