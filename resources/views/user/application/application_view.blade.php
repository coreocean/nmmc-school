@extends('layouts.master')

@section('content')
    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">View Student Application </h5>
                        </div>

                        <div class="card-body">
                            <form id="msform" enctype="multipart/form-data">

                                <!-- progressbar -->
                               <ul id="progressbar">
                                    <li class="active" id="account"><strong>Student Personal Info</strong></li>
                                    <li id="personal"><strong>Parent's Details</strong></li>
                                    <li id="confirm"><strong>Address</strong></li>
                                    <li id="upload"><strong>Document Upload</strong></li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> <br> <!-- fieldsets -->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Student Personal Info:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 1 - 4</h2>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Admission for Standard<span class="required">*</span></label>
                                                <input type="text" class="form-control" value="{{ $data->standard_name }}" readonly>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Academic Year<span class="required">*</span></label>
                                                <input type="text" class="form-control" value="{{ $data->year }}" readonly>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">First Name<span class="required">*</span></label>
                                                    <input type="text" name="firstname" class="form-control" placeholder="First Name" value="{{ $data->firstname }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="middlename" class="form-control" placeholder="Middle Name" value="{{ $data->middlename }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Last Name<span class="required">*</span></label>
                                                    <input type="text" name="lastname" value="{{ $data->lastname }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Date Of Birth<span class="required">*</span></label>
                                                    <input type="date" class="form-control" name="dob" id="inputGroupPrepend45" value="{{ $data->dob }}" readonly>

                                                </div>
                                            </div>

                                            <?php $dob = $data->dob;
                                            $dob = new DateTime($dob);
                                            $currentDate = new DateTime();
                                            $age = $currentDate->diff($dob)->y; ?>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Age<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="age" value="{{ $age }}" id="age"  readonly>
                                                </div>
                                            </div>

                                            <?php
                                            $gender='';
                                            if($data->gender==1){
                                                $gender = "Male";
                                            }else{
                                                $gender = "Female";
                                            }
                                            ?>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                <label class="text-label form-label">Gender<span class="required">*</span></label>
                                                <input type="text" class="form-control" name="age" value="{{ $gender }}" id="age"  readonly>
                                            </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Nationality<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="nationality" value="Indian"  readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Place Of Birth<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="birth_place" value="{{$data->birth_place }}" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother Tongue<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_tongue" value="{{$data->mother_tongue }}" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                <label class="text-label form-label">Religion<span class="required">*</span></label>
                                                <input type="text" class="form-control" name="mother_tongue" value="{{$data->religion_name }}" readonly>
                                            </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                <label class="text-label form-label">Cast Category<span class="required">*</span></label>
                                                <input type="text" class="form-control" name="mother_tongue" value="{{$data->cast_name }}" readonly>
                                            </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Student Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="adhaar_no" value="{{ $data->adhaar_no }}" readonly>
                                                </div>
                                            </div>
                                           <?php
                                           $blood_group ='';
                                           if($data->blood_group == 1){
                                            $blood_group="A+";
                                           }elseif($data->blood_group == 2){
                                            $blood_group="A-";
                                           }
                                           elseif($data->blood_group == 3){
                                            $blood_group="B+";
                                           }
                                           elseif($data->blood_group == 4){
                                            $blood_group="B-";
                                           }
                                           elseif($data->blood_group == 5){
                                            $blood_group="AB+";
                                           }
                                           elseif($data->blood_group == 6){
                                            $blood_group="AB-";
                                           }
                                           elseif($data->blood_group == 7){
                                            $blood_group="O+";
                                           }
                                           elseif($data->blood_group == 8){
                                            $blood_group="O-";
                                           }


                                           ?>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                <label class="text-label form-label">Blood Group</label>
                                                <input type="text" class="form-control" name="adhaar_no" value="{{ $blood_group }}" readonly>
                                            </div>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Father's Details:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 2 - 4</h2>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label"> Father's First Name<span class="required">*</span></label>
                                                    <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" placeholder="Father's First Name" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Middle Name <span class="required">*</span></label>
                                                    <input type="text" name="middle_name" value="{{ $user->middle_name }}" class="form-control" placeholder="Father's Middle Name" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Last Name <span class="required">*</span></label>
                                                    <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" placeholder="Father's Last Name" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mobile_no" class="form-control" value="{{ $user->mobile_no }}" readonly>

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="father_alt_contact" value="{{ $data->father_alt_contact }}" placeholder="Alternate Contact Number" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email Address" readonly>

                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_qualification" value="{{ $data->father_qualification }}" id="emial1" placeholder="Qualification" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="father_profession" value="{{ $data->father_profession }}" id="emial1" placeholder="Occupation/profession" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="father_designation" value="{{ $data->father_designation }}" placeholder="Designation" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="father_office_address" value="{{ $data->father_office_address }}"  readonly>
                                                </div>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Father's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="father_adhaar_no" value="{{ $data->father_adhaar_no }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="father_pan_no" value="{{ $data->father_pan_no }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>

                                            <h2 class="fs-title">Mother's Details:</h2>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's First Name <span class="required">*</span></label>
                                                    <input type="text" name="mother_first_name" class="form-control" value="{{ $data->mother_first_name }}" placeholder="Mother's First Name" readonly>
                                                    <span class="text-danger">
                                                        @error('mother_first_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Middle Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_middle_name" class="form-control" value="{{ $data->mother_middle_name }}" placeholder="Mother's Middle Name" readonly>
                                                    <span class="text-danger">
                                                        @error('mother_middle_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Last Name<span class="required">*</span></label>
                                                    <input type="text" name="mother_last_name" class="form-control" value="{{ $data->mother_last_name }}" placeholder="Mother's Last Name" readonly>
                                                    <span class="text-danger">
                                                        @error('mother_last_name')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Contact No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_mobile_no" value="{{ $data->mother_mobile_no }}" class="form-control" placeholder="(+1)408-657-9007" readonly>
                                                    <span class="text-danger">
                                                        @error('mother_mobile_no')
                                                            {{ $message }}
                                                        @enderror
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Email Address<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_email" value="{{ $data->mother_email }}" id="emial1" placeholder="Email Address" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Qualification</label>
                                                    <input type="text" class="form-control" name="mother_qualification" value="{{ $data->mother_qualification }}" placeholder="Qualification" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Occupation/profession<span class="required">*</span></label>
                                                    <input type="text" class="form-control" name="mother_profession" value="{{ $data->mother_profession }}" id="emial1" placeholder="Occupation/profession" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Designation</label>
                                                    <input type="text" class="form-control" name="mother_designation" value="{{$data->mother_designation }}" placeholder="Designation" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Alt Contact No.</label>
                                                    <input type="text" class="form-control" name="mother_alt_contact" value="{{ $data->mother_alt_contact }}" placeholder="Alternate Contact Number" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Work Address (If applicable)</label>
                                                    <input type="text" class="form-control" name="mother_office_address" value="{{ $data->mother_office_address }}" id="emial1" placeholder="Office Address (If applicable)" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Mother's Adhaar Card No.<span class="required">*</span></label>
                                                    <input type="text" name="mother_adhaar_no" value="{{ $data->mother_adhaar_no }}" class="form-control" maxlength="12" readonly>

                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">PAN No.</label>
                                                    <input type="text" name="mother_pan_no" value="{{ $data->mother_pan_no }}" class="form-control" readonly>

                                                </div>
                                            </div>

                                        </div>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Current Residential Address:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 3 - 4</h2>
                                            </div>


                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Residential House Type<span class="required">*</span></label>
                                                <input type="text" name="no_of_months" value="" class="form-control" readonly>
                                            </div>



                                            <div id="months_section" class="col-lg-4 mb-2" @if($data->residential_type == 1) style="display: block" @else style="display: none" @endif>
                                                <div class="mb-3">
                                                    <label class="text-label form-label">How many months in current address<span class="required">*</span></label>
                                                    <input type="text" name="no_of_months" value="" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="house_no" value="{{ $data->house_no}}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="area" value="{{ $data->area }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="landmark" value="{{ $data->landmark }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="pincode" value="{{ $data->pincode }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="city_name" value="{{ $data->city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <h2 class="fs-title">Permanent Address:</h2>

                                            <div class="col-lg-12 mb-2">
                                                <div class="mb-3">
                                                    <input type="checkbox" name="same_address" value="1" {{ $data->same_address == 1 ? 'checked' : '' }} disabled>
                                                    <label class="text-label form-label">Same as above</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Flat/House No./Building/Apartment<span class="required">*</span></label>
                                                    <input type="text" name="p_house_no" value="{{ $data->p_house_no}}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Area/Street/Sector<span class="required">*</span></label>
                                                    <input type="text" name="p_area" value="{{ $data->p_area }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Land Mark<span class="required">*</span></label>
                                                    <input type="text" name="p_landmark" value="{{ $data->p_landmark }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Pincode No.<span class="required">*</span></label>
                                                    <input type="text" name="p_pincode" value="{{ $data->p_pincode }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">City<span class="required">*</span></label>
                                                    <input type="text" name="p_city_name" value="{{ $data->p_city_name }}" class="form-control">
                                                </div>
                                            </div>



                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">


                                                </div>
                                            </div>
                                            <h2 class="fs-title">Distance:</h2>
                                            <div class="col-lg-4 mb-2">
                                                <label class="text-label form-label">Distance between School & Resident<span class="required">*</span></label>
                                                <input type="text" value="{{ $data->distance_name }}" class="form-control" readonly>
                                            </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                <label class="text-label form-label">School Name<span class="required">*</span></label>
                                                <input type="text" value="{{ $data->school_name }}" class="form-control" readonly>
                                            </div>
                                        </div>

                                            <div class="col-lg-4 mb-2">
                                                <div class="mb-3">
                                                    <label class="text-label form-label">Application Date<span class="required">*</span></label>

                                                    <input type="text" id="current_date" name="current_date" value="{{ now()->format('Y-m-d') }}" class="form-control" readonly>
                                                </div>
                                            </div>

                                        </div>
                                    </div><input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>


                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Document Uploads:</h2>
                                            </div>
                                            <div class="col-5">
                                                <h2 class="steps">Step 4 - 4</h2>
                                            </div>
                                            @foreach ($document as $documents)
                                                <div class="col-lg-4 mb-2">
                                                    <div class="mb-3">
                                                        <label class="form-label">{{$documents->document}}</label>

                                                        <a href="{{ asset('documents/'.$documents->document_file)}}" class="btn btn-primary shadow btn-xs sharp me-1" target="_blank"> <i class="fas fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div><input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function copyAddress() {
            var sameAddressCheckbox = document.getElementsByName('same_address')[0];
            var permanentFields = ['p_house_no', 'p_area', 'p_landmark', 'p_pincode', 'p_city_name'];

            if (sameAddressCheckbox.checked) {

                permanentFields.forEach(function(field) {
                    document.getElementsByName(field)[0].disabled = true;
                });


                var residentialFields = ['house_no', 'area', 'landmark', 'pincode', 'city_name'];
                residentialFields.forEach(function(field) {
                    var residentialValue = document.getElementsByName(field)[0].value;
                    var permanentField = document.getElementsByName('p_' + field)[0];
                    permanentField.value = residentialValue;
                });
            } else {

                permanentFields.forEach(function(field) {
                    document.getElementsByName(field)[0].disabled = false;
                });

                document.getElementsByName('p_house_no')[0].value = '';
                document.getElementsByName('p_area')[0].value = '';
                document.getElementsByName('p_landmark')[0].value = '';
                document.getElementsByName('p_pincode')[0].value = '';
                document.getElementsByName('p_city_name')[0].value = '';
            }
        }

        document.getElementsByName('same_address')[0].addEventListener('change', copyAddress);

        copyAddress();
    </script>

@endsection
